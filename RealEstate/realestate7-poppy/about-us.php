<?php include'header.php';?>
      <!-- Breadcrumbs -->
      <section class="breadcrumbs-custom-inset">
        <div class="breadcrumbs-custom context-dark bg-overlay-60">
          <div class="container">
            <h2 class="breadcrumbs-custom-title">About Us</h2>
            <ul class="breadcrumbs-custom-path">
              <li><a href="index.php">Home</a></li>
              <li class="active">About Us</li>
            </ul>
          </div>
          <div class="box-position" style="background-image: url(images/Banner-About-us.jpg);"></div>
        </div>
      </section>
      <!-- Why choose us-->
      <section class="section section-sm section-first bg-default text-md-left">
        <div class="container">
          <div class="row row-50 justify-content-center align-items-xl-center">
            <div class="col-md-10 col-lg-5 col-xl-6"><img src="images/proyecto04_04_1500px.jpg" alt="" width="519" height="564"/>
            </div>
            <div class="col-md-10 col-lg-7 col-xl-6">
              <h1 class="text-spacing-25 font-weight-normal title-opacity-9">Why choose us</h1>
              <!-- Bootstrap tabs-->
              <div class="tabs-custom tabs-horizontal tabs-line" id="tabs-4">
                <!-- Nav tabs-->
                <ul class="nav nav-tabs">
                  <li class="nav-item" role="presentation"><a class="nav-link active" href="#tabs-4-1" data-toggle="tab">Experience</a></li>
                  <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-4-3" data-toggle="tab">Mission</a></li>
                </ul>
                <!-- Tab panes-->
                <div class="tab-content">
                  <div class="tab-pane fade show active" id="tabs-4-1">
                    <p></p>
                    <!-- Linear progress bar-->
                    <article class="progress-linear progress-secondary">
                      <div class="progress-header">
                        <p>BUY</p>
                      </div>
                      <div class="progress-bar-linear-wrap">
                        <div class="progress-bar-linear"><span class="progress-value">79</span><span class="progress-marker"></span></div>
                      </div>
                    </article>
                    <!-- Linear progress bar-->
                    <article class="progress-linear progress-orange">
                      <div class="progress-header">
                        <p>REN</p>
                      </div>
                      <div class="progress-bar-linear-wrap">
                        <div class="progress-bar-linear"><span class="progress-value">72</span><span class="progress-marker"></span></div>
                      </div>
                    </article>
                    <!-- Linear progress bar-->
                    <article class="progress-linear">
                      <div class="progress-header">
                        <p>SELL</p>
                      </div>
                      <div class="progress-bar-linear-wrap">
                        <div class="progress-bar-linear"><span class="progress-value">88</span><span class="progress-marker"></span></div>
                      </div>
                    </article>
                  </div>
               
                  <div class="tab-pane fade" id="tabs-4-3">
                    <p>Our mission is be the most recommended real estate company and with the best service, as well as offer our clients a high quality treatment</p>
                    <p>service</p>
                    <div class="text-center text-sm-left offset-top-30">
                      <ul class="row-16 list-0 list-custom list-marked list-marked-sm list-marked-secondary">
                        <li>buy</li>
                        <li>rent</li>
                        <li>sell</li>
                        <li>advise</li></ul>
                    </div>
                    <div class="group-md group-middle"><a class="button button-width-xl-230 button-primary button-pipaluk" href="contact-us.php">Get in touch</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div class="container" style="width: 80%;" >
          <h3>about us</h3>
          <p>Since 2009, the date that our company started, we have made dreams come true through advice and support in the purchase of a home.</p>
          <p>From our beginning in 2009 as real estate we are present in page web whe we support  a more clients in buy a the house. we work a the big  because  have more propieties of sale and search your need.</p>
          <p>we a have adviser calificate for you .</p>
        </div>
      </section>

      <!-- Latest Projects-->

      <!-- What people Say-->
      <section class="section section-sm section-last bg-default">
        <div class="container">
          <h3>What people Say</h3>
          <!-- Owl Carousel-->
          <div class="owl-carousel owl-modern" data-items="1" data-stage-padding="15" data-margin="30" data-dots="true" data-animation-in="fadeIn" data-animation-out="fadeOut" data-autoplay="true">
            <!-- Quote Lisa-->
            <article class="quote-lisa">
              <div class="quote-lisa-body"><a class="quote-lisa-figure" href="#"><img class="img-circles" src="images/P4.jpg" alt="" width="100" height="100"/></a>
                <div class="quote-lisa-text">
                  <p class="q">They are the ones indicated to advise and support in the process of acquiring a property, I worked with them at the recommendation of a brother-in-law and there was no problem</p>
                </div>
                <h5 class="quote-lisa-cite"><a href="#">Catherine Williams</a></h5>
                <p class="quote-lisa-status">Local shop owner</p>
              </div>
            </article>
            <!-- Quote Lisa-->
            <article class="quote-lisa">
              <div class="quote-lisa-body"><a class="quote-lisa-figure" href="#"><img class="img-circles" src="images/P5.jpg" alt="" width="100" height="100"/></a>
                <div class="quote-lisa-text">
                  <p class="q">They supported me from the beginning to the end of the process of buying my home and have highly trained consultants who support you in finding the place of your dreams.They supported me from the beginning to the end of the process of buying my home and have highly trained consultants who support you in finding the place of your dreams.</p>
                </div>
                <h5 class="quote-lisa-cite"><a href="#">Rupert Wood</a></h5>
                <p class="quote-lisa-status">House owner</p>
              </div>
            </article>
            <!-- Quote Lisa-->
            <article class="quote-lisa">
              <div class="quote-lisa-body"><a class="quote-lisa-figure" href="#"><img class="img-circles" src="images/P7.jpg" alt="" width="100" height="100"/></a>
                <div class="quote-lisa-text">
                  <p class="q">I highly recommend them since it worked for me, I used their services for the acquisition of my house and I had no problem, Thank you!</p>
                </div>
                <h5 class="quote-lisa-cite"><a href="#">Samantha Brown</a></h5>
                <p class="quote-lisa-status">Freelancer</p>
              </div>
            </article>
          </div>
        </div>
      </section>

      <!-- Counter Classic-->
      <section class="section section-fluid bg-default">
        <div class="parallax-container" data-parallax-img="images/Parque-Nacional-Yoho.jpg">
          <div class="parallax-content section-xl context-dark bg-overlay-26">
            <div class="container">
              <div class="row row-50 justify-content-center border-classic">
                <div class="col-sm-6 col-md-5 col-lg-3">
                  <div class="counter-classic">
                    <div class="counter-classic-number"><span class="counter">40</span>
                    </div>
                    <h5 class="counter-classic-title">Monthly sell</h5>
                  </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-3">
                  <div class="counter-classic">
                    <div class="counter-classic-number"><span class="counter">12</span>
                    </div>
                    <h5 class="counter-classic-title">Awards</h5>
                  </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-3">
                  <div class="counter-classic">
                    <div class="counter-classic-number"><span class="counter">2</span><span class="symbol">k+</span>
                    </div>
                    <h5 class="counter-classic-title">total sales</h5>
                  </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-3">
                  <div class="counter-classic">
                    <div class="counter-classic-number"><span class="counter">26</span>
                    </div>
                    <h5 class="counter-classic-title">Team members</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Our clients-->
     

    <?php include'footer.php';?>