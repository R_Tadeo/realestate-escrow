<?php include'header.php';?>
      <!-- Swiper-->
      <section class="section swiper-container swiper-slider swiper-slider-classic" data-loop="true" data-autoplay="5000" data-simulate-touch="true" data-direction="vertical" data-nav="false">
        <div class="swiper-wrapper text-center">
          <div class="swiper-slide context-dark" data-slide-bg="images/images/bn1.jpg">
            <div class="swiper-slide-caption section-md">
              <div class="container">
                <h1 data-caption-animate="fadeInLeft" data-caption-delay="0">APPROVE OFFERS</h1>
                <h5 class="text-width-large" data-caption-animate="fadeInRight" data-caption-delay="100">We have a large number of properties on offer.</h5><a class="button button-primary button-ujarak" href="contact-us.php" data-caption-animate="fadeInUp" data-caption-delay="200">Get in touch</a>
              </div>
            </div>
          </div>
          <div class="swiper-slide context-dark" data-slide-bg="images/images/gallery-original-5.jpg">
            <div class="swiper-slide-caption section-md">
              <div class="container">
                <h1 data-caption-animate="fadeInLeft" data-caption-delay="0">We adjust to your needs</h1>
                <h5 class="text-width-large" data-caption-animate="fadeInRight" data-caption-delay="100">We study your needs to offer you the best option</h5 >
              </div>
            </div>
          </div>
          <div class="swiper-slide context-dark" data-slide-bg="images/images/gallery-original-7.jpg">
            <div class="swiper-slide-caption section-md">
              <div class="container">
                <h1 data-caption-animate="fadeInLeft" data-caption-delay="0">
we advise you</h1>
                <h5 class="text-width-large" data-caption-animate="fadeInRight" data-caption-delay="100">We advise you during the process and help you get the best price</h5><a class="button button-primary button-ujarak" href="contact-us.php" data-caption-animate="fadeInUp" data-caption-delay="200">Get in touch</a>
              </div>
            </div>
          </div>
        </div>
        <!-- Swiper Pagination-->
        <div class="swiper-pagination__module">
          <div class="swiper-pagination__fraction"><span class="swiper-pagination__fraction-index">00</span><span class="swiper-pagination__fraction-divider">/</span><span class="swiper-pagination__fraction-count">00</span></div>
          <div class="swiper-pagination__divider"></div>
          <div class="swiper-pagination"></div>
        </div>
      </section>

      <!-- See all services-->
      <section class="section section-sm section-first bg-default text-center" style="background:  #e5f4fa ;">
        <div class="container">
          <div class="row row-30 justify-content-center">
            <div class="col-md-7 col-lg-5 col-xl-6 text-lg-left wow fadeInUp">
              <div class="figure-classic figure-classic-left"><img src="images/images/ofiice.jpg" alt="" width="513" height="561"/>
              </div>
            </div>
            <div class="col-lg-7 col-xl-6">
              <div class="row row-30">
               
                <div class="col-sm-6 wow fadeInRight" data-wow-delay=".1s">
                  <article class="box-icon-modern box-icon-modern-2 ">
                    <div class="box-icon-modern-icon  fl-bigmug-line-shopping199"></div>
                    <h5 class="box-icon-modern-title"><a href="#">BUY</a></h5>
                    <div class="box-icon-modern-decor"></div>
                    <p class="box-icon-modern-text">
On our website you can find a large number of offers so finding the home of your dreams is possible</p>
                  </article>
                </div>
                <div class="col-sm-6 wow fadeInRight" data-wow-delay=".2s">
                  <article class="box-icon-modern box-icon-modern-2">
                    <div class="box-icon-modern-icon fl-bigmug-line-weekly14 "></div>
                    <h5 class="box-icon-modern-title"><a href="#">RENT</a></h5>
                    <div class="box-icon-modern-decor"></div>
                    <p class="box-icon-modern-text">
On our new website you can do searches, comparisons, explore photos and find what you need</p>
                  </article>
                </div>
                <div class="col-sm-6 wow fadeInRight" data-wow-delay=".3s">
                  <article class="box-icon-modern box-icon-modern-2">
                    <div class="box-icon-modern-icon fl-bigmug-line-shuffle17"></div>
                    <h5 class="box-icon-modern-title"><a href="#">SELL</a></h5>
                    <div class="box-icon-modern-decor"></div>
                    <p class="box-icon-modern-text">We have the best sales consultants, who will help you get more for your property </p>
                  </article>
                </div>
                 <div class="col-sm-6 wow fadeInRight" data-wow-delay=".4s">
                  <article class="box-icon-modern box-icon-modern-2">
                    <div class="box-icon-modern-icon fl-bigmug-line-chat55  "></div>
                    <h5 class="box-icon-modern-title"><a href="#">
ASESORIA</a></h5>
                    <div class="box-icon-modern-decor"></div>
                    <p class="box-icon-modern-text">Our specialists are ready to consult you on any   buy and sell propietes related topic./p>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    <hr>
      <!-- Latest Projects-->
      <section class="section section-sm section-fluid bg-default text-center">
        <div style="width: 80%;" class="container">
          <h3 class="">SERVICES</h3>
          <p class="" data-wow-delay=".1s">IOur real estate agency offers its own services in the management of real estate, significantly serving the emerging market and the social need in the rental of homes, also presenting all the diversity that the real estate market offers us.</p><p>
All actions of commercial priority will be given by the situation that the present demands, until we achieve the full integration and consolidation of our company.</p>
          <div class="isotope-filters isotope-filters-horizontal">
            <button class="isotope-filters-toggle button button-md button-icon button-icon-right button-default-outline button-wapasha" data-custom-toggle="#isotope-3" data-custom-toggle-hide-on-blur="true"><span class="icon fa fa-caret-down"></span>Filter</button>
            <ul class="isotope-filters-list" id="isotope-3">
              <li><a class="active" href="#" data-isotope-filter="*" data-isotope-group="gallery">All</a></li>
              <li><a href="#" data-isotope-filter="Type 1" data-isotope-group="gallery">HOUSE</a></li>
              <li><a href="#" data-isotope-filter="Type 2" data-isotope-group="gallery">CONDO</a></li>
              <li><a href="#" data-isotope-filter="Type 3" data-isotope-group="gallery">DUPLEX</a></li>
            </ul>
          </div>
          <div class="row row-30 isotope" data-isotope-layout="fitRows" data-isotope-group="gallery" data-lightgallery="group">
            <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInLeft" data-filter="Type 3">
              <!-- Thumbnail Classic-->
              <article class="thumbnail thumbnail-classic thumbnail-md">
                <div class="thumbnail-classic-figure"><img src="images/propi/s1.jpg" alt="" width="420" height="350"/>
                </div>
                <div class="thumbnail-classic-caption">
                  <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="images/propi/1.1.jpg" data-lightgallery="item"><img src="images/propi/1.1.jpg" alt="" width="420" height="350"/></a>
                    <h5 class="thumbnail-classic-title"><a href="#">San Francisco, CA, 94118</a></h5>
                  </div>
                  <p class="thumbnail-classic-text">Ucked away from the hustle and bustle of San Francisco is this charming, secluded cottage in the Inner Richmond</p>
                  <p><a href="vts.php">more info</a></p>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInLeft" data-filter="Type 2" data-wow-delay=".1s">
              <!-- Thumbnail Classic-->
              <article class="thumbnail thumbnail-classic thumbnail-md">
                <div class="thumbnail-classic-figure"><img src="images/propi/2.jpg" alt="" width="420" height="350"/>
                </div>
                <div class="thumbnail-classic-caption">
                  <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="images/propi/2.1.jpg" data-lightgallery="item"><img src="images/propi/2.1.jpg" alt="" width="420" height="350"/></a>
                    <h5 class="thumbnail-classic-title"><a href="#">San Francisco, CA, 94110</a></h5>
                  </div>
                  <p class="thumbnail-classic-text"> This lovely split level home, has 2 large, light and airy, bedrooms and one bath on the upper level</p>
                    <p><a href="vts.php">more info</a></p>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInLeft" data-filter="Type 1" data-wow-delay=".2s">
              <!-- Thumbnail Classic-->
              <article class="thumbnail thumbnail-classic thumbnail-md">
                <div class="thumbnail-classic-figure"><img src="images/propi/3.jpg" alt="" width="420" height="350"/>
                </div>
                <div class="thumbnail-classic-caption">
                  <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="images/propi/3.1.jpg" data-lightgallery="item"><img src="images/propi/3.1.jpg" alt="" width="420" height="350"/></a>
                    <h5 class="thumbnail-classic-title"><a href="#">Antioch, CA 94509</a>
                  <p class="thumbnail-classic-text">Home to this cozy sought after three bedroom two bathroom single story. Adorable living room with beautifully updated kitchen/dining area to include granite</p>
  <p><a href="vts.php">more info</a></p>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInLeft" data-filter="Type 3" data-wow-delay=".3s">
              <!-- Thumbnail Classic-->
              <article class="thumbnail thumbnail-classic thumbnail-md">
                <div class="thumbnail-classic-figure"><img src="images/propi/4.jpg" alt="" width="420" height="350"/>
                </div>
                <div class="thumbnail-classic-caption">
                  <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="images/propi/4. 1.jpg" data-lightgallery="item"><img src="images/propi/4.1.jpg" alt="" width="420" height="350"/></a>
                    <h5 class="thumbnail-classic-title"><a href="#">San Francisco, CA 94134</a></h5>
                  </div>
                  <p class="thumbnail-classic-text">Come on over to this great block located in Upper Visitacion Valley. Built around the turn of the 20th Century this fully detached home shows lots of love & attention to detail.</p>
  <p><a href="vts.php">more info</a></p>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInRight" data-filter="Type 2">
              <!-- Thumbnail Classic-->
              <article class="thumbnail thumbnail-classic thumbnail-md">
                <div class="thumbnail-classic-figure"><img src="images/propi/5.jpg" alt="" width="420" height="350"/>
                </div>
                <div class="thumbnail-classic-caption">
                  <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="images/propi/5.1.jpg" data-lightgallery="item"><img src="images/propi/5.1.jpg" alt="" width="420" height="350"/></a>
                    <h5 class="thumbnail-classic-title"><a href="#">Danville, CA 94526</a></h5>
                  </div>
                  <p class="thumbnail-classic-text">Enjoy this fantastic property's pleasures, from the park-like setting to the refreshing pool and spa. Well maintained throughout the years,</p>
                   <p><a href="vts.php">more info</a></p>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInRight" data-filter="Type 1" data-wow-delay=".1s">
              <!-- Thumbnail Classic-->
              <article class="thumbnail thumbnail-classic thumbnail-md">
                <div class="thumbnail-classic-figure"><img src="images/propi/6.jpg" alt="" width="420" height="350"/>
                </div>
                <div class="thumbnail-classic-caption">
                  <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="images/propi/6.1.jpg" data-lightgallery="item"><img src="images/propi/6.1.jpg" alt="" width="420" height="350"/></a>
                    <h5 class="thumbnail-classic-title"><a href="#">Oakland, CA 94606</a></h5>
                  </div>
                  <p class="thumbnail-classic-text">This updated, airy 2BR/1.5BA bungalow with an enormous backyard is perfect for indoor-outdoor living. </p>
                
                 <p><a href="vts.php">more info</a></p></div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInRight" data-filter="Type 3" data-wow-delay=".2s">
              <!-- Thumbnail Classic-->
              <article class="thumbnail thumbnail-classic thumbnail-md">
                <div class="thumbnail-classic-figure"><img src="images/propi/7.jpg" alt="" width="420" height="350"/>
                </div>
                <div class="thumbnail-classic-caption">
                  <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="images/propi/7.1.jpg" data-lightgallery="item"><img src="images/propi/7.jpg" alt="" width="420" height="350"/></a>
                    <h5 class="thumbnail-classic-title"><a href="#">Berkeley, CA 94705</a></h5>
                  </div>
                  <p class="thumbnail-classic-text">Located in prestigious Claremont, this lovely 3 bedroom 2 bath home features vaulted ceilings, unique architectural details, hardwood floors, a </p>
                </div>
                 <p><a href="vts.php">more info</a></p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInRight" data-filter="Type 2" data-wow-delay=".3s">
              <!-- Thumbnail Classic-->
              <article class="thumbnail thumbnail-classic thumbnail-md">
                <div class="thumbnail-classic-figure"><img src="images/propi/8.jpg" alt="" width="420" height="350"/>
                </div>
                <div class="thumbnail-classic-caption">
                  <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="images/propi/8.1.jpg" data-lightgallery="item"><img src="images/propi/8.jpg" alt="" width="420" height="350"/></a>
                    <h5 class="thumbnail-classic-title"><a href="#">Dublin, CA 94568</a></h5>
                  </div>
                  <p class="thumbnail-classic-text"> stunning home on a huge lot on the 12th hole of the Dublin Ranch golf course! Dramatic rotunda entry with two staircases. </p>
                   <p><a href="vts.php">more info</a></p>
                </div>
              </article>
            </div>
          </div>
        </div>
      </section>

      <!-- Years of experience-->
      <section class="section section-sm bg-default">
        <div class="container">
          <div class="row row-50 row-xl-24 justify-content-center align-items-center align-items-lg-start text-left">
            <div class="col-md-6 col-lg-5 col-xl-4 text-center"><a class="text-img" href="about-us.php"><span class="counter">10</span></a></div>
            <div class="col-sm-8 col-md-6 col-lg-5 col-xl-4">
              <div class="text-width-extra-small offset-top-lg-24 wow fadeInUp">
                <h3 class="title-decoration-lines-left">Years of experience</h3>
                <p class="text-gray-500">we are a real estate agency with great international impact we have highly trained agents</p><a class="button button-secondary button-pipaluk" href="contact-us.php">Get in touch</a>
              </div>
            </div>
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-4 wow fadeInRight" data-wow-delay=".1s">
              <div class="row justify-content-center border-2-column offset-top-xl-26">
                <div class="col-9 col-sm-6">
                  <div class="counter-amy">
                    <div class="counter-amy-number"><span class="counter">2</span><span class="symbol">k+</span>
                    </div>
                    <h6 class="counter-amy-title">total sales</h6>
                  </div>
                </div>
                <div class="col-9 col-sm-6">
                  <div class="counter-amy">
                    <div class="counter-amy-number"><span class="counter">40</span>
                    </div>
                    <h6 class="counter-amy-title">Monthly sell</h6>
                  </div>
                </div>
                <div class="col-9 col-sm-6">
                  <div class="counter-amy">
                    <div class="counter-amy-number"><span class="counter">12</span>
                    </div>
                    <h6 class="counter-amy-title">Awards</h6>
                  </div>
                </div>
                <div class="col-9 col-sm-6">
                  <div class="counter-amy">
                    <div class="counter-amy-number"><span class="counter">26</span>
                    </div>
                    <h6 class="counter-amy-title">Employees</h6>
                  </div>
                </div>
              </div>
            </div>
         
          </div>
        </div>
      </section>

      <!-- Years of experience-->
      <section class="section section-sm section-fluid bg-default">
        <div class="container-fluid">
          <h3>our team</h3>
          <div class="row row-sm row-30 justify-content-center">
            <div class="col-md-6 col-lg-5 col-xl-3 wow fadeInRight" data-wow-delay=".2s">
              <!-- Team Classic-->
              <article class="team-classic team-classic-lg"><a class="team-classic-figure" href="#"><img src="images/P1.jpg" alt="" width="420" height="424"/></a>
                <div class="team-classic-caption">
                  <h5 class="team-classic-name"><a href="#">Sam Robinson</a></h5>
                  <p class="team-classic-status">adviser</p>
                </div>
              </article>
            </div>
            <div class="col-md-6 col-lg-5 col-xl-3 wow fadeInRight" data-wow-delay=".3s">
              <!-- Team Classic-->
              <article class="team-classic team-classic-lg"><a class="team-classic-figure" href="#"><img src="images/P3.jpg" alt="" width="420" height="424"/></a>
                <div class="team-classic-caption">
                  <h5 class="team-classic-name"><a href="#">J.J. FRAY</a></h5>
                  <p class="team-classic-status">asviser</p>
                </div>
              </article>
            </div>
          </div>
        </div>
      </section>

      <!-- You dream — we embody-->


      <!-- What people Say-->
      <section class="section section-sm section-bottom-70 section-fluid bg-default">
        <div class="container-fluid">
          <h3>What people Say</h3>
          <div class="row row-50 row-sm">
            <div class="col-md-6 col-xl-4 wow fadeInRight">
              <!-- Quote Modern-->
              <article class="quote-modern quote-modern-custom">
                <div class="unit unit-spacing-md align-items-center">
                  <div class="unit-left"><a class="quote-modern-figure" href="#"><img class="img-circles" src="images/P4.jpg
                    " alt="" width="75" height="75"/></a></div>
                  <div class="unit-body">
                    <h5 class="quote-modern-cite"><a href="#">Catherine Williams</a></h5>
                    <p class="quote-modern-status">Local owner</p>
                  </div>
                </div>
                <div class="quote-modern-text">
                  <p class="q">They are the ones indicated to advise and support in the process of acquiring a property, I worked with them at the recommendation of a brother-in-law and there was no problem</p>
                </div>
              </article>
            </div>
            <div class="col-md-6 col-xl-4 wow fadeInRight" data-wow-delay=".1s">
              <!-- Quote Modern-->
              <article class="quote-modern quote-modern-custom">
                <div class="unit unit-spacing-md align-items-center">
                  <div class="unit-left"><a class="quote-modern-figure" href="#"><img class="img-circles" src="images/P5.jpg" alt="" width="75" height="75"/></a></div>
                  <div class="unit-body">
                    <h5 class="quote-modern-cite"><a href="#">Rupert Wood</a></h5>
                    <p class="quote-modern-status">House owner</p>
                  </div>
                </div>
                <div class="quote-modern-text">
                  <p class="q"> They supported me from the beginning to the end of the process of buying my home and have highly trained consultants who support you in finding the place of your dreams.</p>
                </div>
              </article>
            </div>
            <div class="col-md-6 col-xl-4 wow fadeInRight" data-wow-delay=".2s">
              <!-- Quote Modern-->
              <article class="quote-modern quote-modern-custom">
                <div class="unit unit-spacing-md align-items-center">
                  <div class="unit-left"><a class="quote-modern-figure" href="#"><img class="img-circles" src="images/P7.jpg" alt="" width="75" height="75"/></a></div>
                  <div class="unit-body">
                    <h5 class="quote-modern-cite"><a href="#">Samantha Brown</a></h5>
                    <p class="quote-modern-status">Freelancer</p>
                  </div>
                </div>
                <div class="quote-modern-text">
                  <p class="q">I highly recommend them since it worked for me, I used their services for the acquisition of my house and I had no problem, Thank you!</p>
                </div>
              </article>
            </div>
          </div>
        </div>
      </section>

      <!-- Latest blog posts-->
<?php include'footer.php';?>