<?php include'header.php';?>

	<!-- Home -->
	<br>
<br>
<br>
<br><br><br>

	<div class="suy">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/about.jpg" data-speed="0.8" style="height: 50PX;"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content text-center">
						<div class="home_title">About us</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Intro -->

	<div class="intro">
		<div class="container">
			<div class="row row-eq-height">
				
				<!-- Intro Content -->
				<div class="col-lg-6">
					<div class="intro_content">
						<div class="section_title_container">
							<div class="section_subtitle">the best deals</div>
							<div class="section_title"><h1>Who we are</h1></div>
						</div>
						<div class="intro_text">
							<p>The origin of us goes back to 1993 when Joseft James, a real estate agent with a lot of experience but disappointed with the way the sector works, decided to create an alternative way of working that would allow him to practice his profession with full autonomy. Like many other good agents, he was frustrated by his limited income and the limited professional development offered by the industry at the time. Thus, it was created
J.J. REAL ESTATE: a company that promotes a new way of working that has become the ideal profession for thousands of tenacious and ambitious people around the world.
</p>
<br>
<p>
The system created by him is designed to attract and retain the best salespeople in the market, offering them an enriching professional environment in which they can work with full satisfaction. This system has a particularly revealing aspect: the income of each professional is a true reflection of the results achieved with their work. provide the best service in exchange for obtaining the maximum reward.</p>
<br>
<p>

The system was widely accepted by Agents and clients, managing to open six offices in the first six months of development. Since then, we have grown to be the most successful real estate company in the world. Currently, it has more than 10,000 Associates working in more than 1,000 offices located in more than 60 countries.
.</p>
						</div>

					</div>
				</div>

				<!-- Intro Image -->
				<div class="col-lg-6 intro_col">
					<div class="intro_image">
						<div class="background_image" style="background-image:url(images/intro.jpg)"></div>
						<img src="images/intro.jpg" alt="">
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Services -->

	<div class="services">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						
						<div class="section_title"><h1>The Best Real Estate Option</h1></div>
					</div>
				</div>
			</div>
			<div class="row services_row">
				
				<!-- Service -->
				<div class="col-xl-4 col-md-6">
					<div class="service">
						<div class="service_title_container d-flex flex-row align-items-center justify-content-start">
							<div class="service_icon d-flex flex-column align-items-start justify-content-center">
								<img src="images/service_1.png" alt="">
							</div>
							
						</div>
						<div class="service_text">
							<p>The best developments in the destination carefully selected to guarantee your satisfaction.</p>
						</div>
					</div>
				</div>

				<!-- Service -->
				<div class="col-xl-4 col-md-6">
					<div class="service">
						<div class="service_title_container d-flex flex-row align-items-center justify-content-start">
							<div class="service_icon d-flex flex-column align-items-start justify-content-center">
								<img src="images/service_2.png" alt="">
							</div>
							
						</div>
						<div class="service_text">
							<p>Sale, rental and administration of real estate.</p>
						</div>
					</div>
				</div>

				<!-- Service -->
				<div class="col-xl-4 col-md-6">
					<div class="service">
						<div class="service_title_container d-flex flex-row align-items-center justify-content-start">
							<div class="service_icon d-flex flex-column align-items-start justify-content-center">
								<img src="images/service_3.png" alt="">
							</div>
							
						</div>
						<div class="service_text">
							<p>Management for mortgage loans.</p>
						</div>
					</div>
				</div>

				<!-- Service -->
				<div class="col-xl-4 col-md-6">
					<div class="service">
						<div class="service_title_container d-flex flex-row align-items-center justify-content-start">
							<div class="service_icon d-flex flex-column align-items-start justify-content-center">
								<img src="images/service_4.png" alt="">
							</div>
							
						</div>
						<div class="service_text">
							<p>Management with municipal, state and federal authorities</p>
						</div>
					</div>
				</div>

				<!-- Service -->
				<div class="col-xl-4 col-md-6">
					<div class="service">
						<div class="service_title_container d-flex flex-row align-items-center justify-content-start">
							<div class="service_icon d-flex flex-column align-items-start justify-content-center">
								<img src="images/service_5.png" alt="">
							</div>
				
						</div>
						<div class="service_text">
							<p> Legal representation and search service.
TESTIMONIAL</p>
						</div>
					</div>
				</div>

				

			</div>
		</div>
	</div>

	<!-- Milestones -->

	<div class="milestones">
		<div class="container">
			<div class="row">
				
				<!-- Milestone -->
				<div class="col-xl-3 col-md-6 milestone_col">
					<div class="milestone d-flex flex-row align-items-start justify-content-md-center justify-content-start">
						<div class="milestone_content">
							<div class="milestone_icon d-flex flex-column align-items-start justify-content-center"><img src="images/duplex.svg" alt=""></div>
							<div class="milestone_counter" data-end-value="425">4000</div>
							<div class="milestone_text">homes sold</div>
						</div>
					</div>
				</div>

				<!-- Milestone -->
				<div class="col-xl-3 col-md-6 milestone_col">
					<div class="milestone d-flex flex-row align-items-start justify-content-md-center justify-content-start">
						<div class="milestone_content">
							<div class="milestone_icon d-flex flex-column align-items-start justify-content-center"><img src="images/prize.svg" alt=""></div>
							<div class="milestone_counter" data-end-value="18">15</div>
							<div class="milestone_text">awards</div>
						</div>
					</div>
				</div>

				<!-- Milestone -->
				<div class="col-xl-3 col-md-6 milestone_col">
					<div class="milestone d-flex flex-row align-items-start justify-content-md-center justify-content-start">
						<div class="milestone_content">
							<div class="milestone_icon d-flex flex-column align-items-start justify-content-center"><img src="images/home.svg" alt=""></div>
							<div class="milestone_counter" data-end-value="25" data-sign-after="k">35</div>
							<div class="milestone_text">followers</div>
						</div>
					</div>
				</div>

				<!-- Milestone -->
				<div class="col-xl-3 col-md-6 milestone_col">
					<div class="milestone d-flex flex-row align-items-start justify-content-md-center justify-content-start">
						<div class="milestone_content">
							<div class="milestone_icon d-flex flex-column align-items-start justify-content-center"><img src="images/rent.svg" alt=""></div>
							<div class="milestone_counter" data-end-value="1265">2000</div>
							<div class="milestone_text">rentals</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Agents -->

	<div class="agents">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<div class="section_subtitle">the best deals</div>
						<div class="section_title"><h1>Our Realtors</h1></div>
					</div>
				</div>
			</div>
			<div class="row agents_row">
				
				<!-- Agent -->
				<div class="col-lg-4 agent_col">
					<div class="agent">
						<div class="agent_image"><img src="images/realtor_1.jpg" alt=""></div>
						<div class="agent_content">
							<div class="agent_name"><a href="#">Michael Smith</a></div>
							<div class="agent_title">Buying Agent</div>
							<div class="agent_list">
								<ul>
									<li>michael@myhometemp.com</li>
									<li>+45 27774 5653 267</li>
								</ul>
							</div>
							<div class="social">
								<ul class="d-flex flex-row align-items-center justify-content-start">
									<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<!-- Agent -->
				<div class="col-lg-4 agent_col">
					<div class="agent">
						<div class="agent_image"><img src="images/realtor_2.jpg" alt=""></div>
						<div class="agent_content">
							<div class="agent_name"><a href="#">Jane Williams</a></div>
							<div class="agent_title">Buying Agent</div>
							<div class="agent_list">
								<ul>
									<li>michael@myhometemp.com</li>
									<li>+45 27774 5653 267</li>
								</ul>
							</div>
							<div class="social">
								<ul class="d-flex flex-row align-items-center justify-content-start">
									<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<!-- Agent -->
				<div class="col-lg-4 agent_col">
					<div class="agent">
						<div class="agent_image"><img src="images/realtor_3.jpg" alt=""></div>
						<div class="agent_content">
							<div class="agent_name"><a href="#">Carla Brown</a></div>
							<div class="agent_title">Buying Agent</div>
							<div class="agent_list">
								<ul>
									<li>michael@myhometemp.com</li>
									<li>+45 27774 5653 267</li>
								</ul>
							</div>
							<div class="social">
								<ul class="d-flex flex-row align-items-center justify-content-start">
									<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="row contact_row">
				<div class="col">
					<div class="button ml-auto mr-auto"><a href="#">contact us</a></div>
				</div>
			</div>
		</div>
	</div>

	<?php include'footer.php';?>