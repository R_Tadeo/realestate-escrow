<!DOCTYPE html>
<html lang="en">
<head>
<title>j.j. realestate</title>
<meta charset="utf-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="styles/listings.css">
<link rel="stylesheet" type="text/css" href="styles/listings_responsive.css">

<link rel="stylesheet" type="text/css" href="styles/about.css">
<link rel="stylesheet" type="text/css" href="styles/about_responsive.css">
<meta name="description" content="myHOME - real estate template project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap-4.1.2/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/contact.css">
>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="myHOME - real estate template project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap-4.1.2/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.3.4/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="styles/contact.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.3.4/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.3.4/animate.css">
<link rel="stylesheet" type="text/css" href="styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="styles/responsive.css">
<link rel="stylesheet" type="text/css" href="styles/contact_responsive.css"
</head>
<body>

  <div class="super_container">
  <div class="super_overlay"></div>
  <!-- Header -->
  <header class="header">
    
    <!-- Header Bar -->
    <div class="header_bar d-flex flex-row align-items-center justify-content-start">
      <div class="header_list">
        <ul class="d-flex flex-row align-items-center justify-content-start">
          <!-- Phone -->
          <li class="d-flex flex-row align-items-center justify-content-start">
            <div><img src="images/phone-call-2.svg" alt=""></div>
            <span>+546s 990221 123</span>
          </li>
          <!-- Address -->
          <li class="d-flex flex-row align-items-center justify-content-start">
            <div><img src="images/placeholder_2.svg" alt=""></div>
            <span>Main Str, no 23, New York</span>
          </li>
          <!-- Email -->
          <li class="d-flex flex-row align-items-center justify-content-start">
            <div><img src="images/envelope-2.svg" alt=""></div>
            <span>info@J.J.REALESTATE.com</span>
          </li>
        </ul>
      </div>
      <div class="ml-auto d-flex flex-row align-items-center justify-content-start">
        <div class="social">
          <ul class="d-flex flex-row align-items-center justify-content-start">
            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
          </ul>
        </div>
        
      </div>
    </div>

    <!-- Header Content -->
    <div class="header_content d-flex flex-row align-items-center justify-content-start">
      <div class="logo"><a href="index.php"><img style="height: 50PX; " src="images/logo.png" alt="Realestate"></a></div>
      <nav class="main_nav">
        <ul class="d-flex flex-row align-items-start justify-content-start">
          <li><a href="index.php">Home</a></li>
          <li><a href="about.php">About us</a></li>
          <li><a href="listings.php">propities</a></li>
          
          <li><a href="contact.php">Contact</a></li>
        </ul>
      </nav>
      <div class="submit ml-auto"><a href="#">submit listing</a></div>
      <div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
    </div>

  </header>