<?php include'header.php';?>

<br>
<br>
<br>
<br><br><br>
<div class="suy">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/blog.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content text-center">
						<div class="home_title">Contact</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Menu -->


	<!-- Home -->



	<!-- Contact -->

	<div class="contact">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<div class="section_subtitle">the best deals</div>
						<div class="section_title"><h1>Get in touch</h1></div>
					</div>
				</div>
			</div>
			<div class="row contact_row">

				<!-- Contact - About -->
				<div class="col-lg-4 contact_col">
					<div class="logo"><a href="index.php"><img style="height: 150PX; " src="images/logo.png" alt="Realestate"></a></div>
					<div class="contact_text">
						<p>The origin of us goes back to 1993 when Joseft James, a real estate agent with a lot of experience but disappointed with the way the sector works, decided to create an alternative way of working that would allow him to practice his profession with full autonomy.</p>
					</div>
				</div>

				<!-- Contact - Info -->
				<div class="col-lg-4 contact_col">
					<div class="contact_info">
						<ul>
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div class="d-flex flex-column align-items-center justify-content-center">
									<div><img src="images/placeholder_2.svg" alt=""></div>
								</div>
								<span>Main Str, no 23, New York</span>
							</li>
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div class="d-flex flex-column align-items-center justify-content-center">
									<div><img src="images/phone-call-2.svg" alt=""></div>
								</div>
								<span>+546 990221 123</span>
							</li>
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div class="d-flex flex-column align-items-center justify-content-center">
									<div><img src="images/envelope-2.svg" alt=""></div>
								</div>
								<span>hosting@contact.com</span>
							</li>
						</ul>
					</div>
				</div>

				<!-- Contact - Image -->
				<div class="col-lg-4 contact_col">
					<div class="contact_image d-flex flex-column align-items-center justify-content-center">
						<img src="images/contact_image.jpg" alt="">
					</div>
				</div>

			</div>
			<div class="row contact_form_row">
				<div class="col">
					<div class="contact_form_container">
						<form action="#" class="contact_form text-center" id="contact_form">
							<div class="row">
								<div class="col-lg-4">
									<input type="text" class="contact_input" placeholder="Your name" required="required">
								</div>
								<div class="col-lg-4">
									<input type="email" class="contact_input" placeholder="Your e-mail" required="required">
								</div>
								<div class="col-lg-4">
									<input type="text" class="contact_input" placeholder="Subject" required="required">
								</div>
							</div>
							<textarea class="contact_textarea contact_input" placeholder="Message" required="required"></textarea>
							<button class="contact_button">send message</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Google Map -->

	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8864.22473069926!2d-73.98994481038801!3d40.743234213858386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c259aff2c10fcb%3A0xe592ece2bd3ac215!2s28%20Street!5e0!3m2!1ses-419!2smx!4v1600787238452!5m2!1ses-419!2smx" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

	<!-- Footer -->

	<?php include'footer.php';?>