<?php include 'header.php' ?>

<div class="site-block-wrap" style="height: 500px;">
      <div class="site-blocks-cover overlay overlay-2" style="background-image: url(images/hero_1.jpg); height: 500px;" data-aos="fade" id="home-section">
        <div class="container">
          <div class="row align-items-center justify-content-center pt-5" style="height: 0;">
            <div class="col-md-6 mt-lg-5 text-center">
              <h1 class="text-shadow">ABOUT US</h1>
              <p class="mb-5 text-shadow">Feel free to learn more about our team and company on this page. We are always happy to help you!</p>
            </div>
          </div>
        </div>
      </div>  
</div> 


<section class="site-section" id="about-section">
      <div class="container">
        
        <div class="row large-gutters">
          <div class="col-lg-6 mb-5">

              <div class="owl-carousel slide-one-item with-dots">
                  <div><img src="images/img_1.jpg" alt="Image" class="img-fluid"></div>
                  <div><img src="images/img_2.jpg" alt="Image" class="img-fluid"></div>
                  <div><img src="images/img_3.jpg" alt="Image" class="img-fluid"></div>
                </div>

          </div>
          <div class="col-lg-6 ml-auto">
            
            <h2 class="section-title mb-3">Warehouse Real Estate</h2>
                <p class="lead">Hello! Welcome to Warehouse Real Estate.</p>
                <p>Integrity does not bend to the wind of convenience and neither do we. We will always tell you what the facts are, even if it is not what you want to hear, but what are the realities of the situation. We are here to to help you reach your goals in Real Estate, whatever those may be!</p>

                <ul class="list-unstyled ul-check success">
                  <li>Regularly audited by government authorities.</li>
                  <li>Armed with an in-depth knowledge of the escrow process.</li>
                  <li>A licensed and regulated escrow company compliant with Realestate Law.</li>
                  <li>Online escrow that is simple and safe for Buyers and Sellers.</li>
                  <li>The safest service to trust with your money.</li>
                </ul>
          </div>
        </div>
      </div>
    </section>

    <section class="py-5 bg-primary site-section how-it-works" id="howitworks-section">
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-7 text-center">
            <h2 class="section-title mb-3 text-black">How It Works</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 text-center">
            <div class="pr-5 first-step">
              <span class="text-black">01.</span>
              <span class="custom-icon flaticon-house text-black"></span>
              <h3 class="text-black">Find Property.</h3>
              <p class="text-black">We help you find a new home by offering a smart real estate.</p>
            </div>
          </div>

          <div class="col-md-4 text-center">
            <div class="pr-5 second-step">
              <span class="text-black">02.</span>
              <span class="custom-icon flaticon-coin text-black"></span>
              <h3 class="text-dark">Buy Property.</h3>
              <p class="text-black">Millions of houses and apartments in your favourite cities.</p>
            </div>
          </div>

          <div class="col-md-4 text-center">
            <div class="pr-5">
              <span class="text-black">03.</span>
              <span class="custom-icon flaticon-home text-black"></span>
              <h3 class="text-dark">Outstanding Houses.</h3>
              <p class="text-black">Sign up now and sell or rent your own properties.</p>
            </div>
          </div>
        </div>
      </div>  
    </section>

    <section class="site-section" id="agents-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-7 text-left">
            <h2 class="section-title mb-3">Real Estate Agents</h2>
            <p class="lead">Feel free to learn more about our team and company on this page. We are always happy to help you!</p>
          </div>
        </div>
        <div class="row">
          

          <div class="col-md-6 col-lg-4 mb-4">
            <div class="team-member">
              <figure>
                <ul class="social">
                  <li><a href="#"><span class="icon-facebook"></span></a></li>
                  <li><a href="#"><span class="icon-twitter"></span></a></li>
                  <li><a href="#"><span class="icon-linkedin"></span></a></li>
                  <li><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
                <img src="images/person_1.jpg" alt="Image" class="img-fluid">
              </figure>
              <div class="p-3 bg-primary">
                <h3 class="mb-2">Allison Holmes</h3>
                <span class="position">Real Estate Agent</span>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-4">
            <div class="team-member">
              <figure>
                <ul class="social">
                  <li><a href="#"><span class="icon-facebook"></span></a></li>
                  <li><a href="#"><span class="icon-twitter"></span></a></li>
                  <li><a href="#"><span class="icon-linkedin"></span></a></li>
                  <li><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
                <img src="images/person_2.jpg" alt="Image" class="img-fluid">
              </figure>
              <div class="p-3 bg-primary">
                <h3 class="mb-2">Dave Simpson</h3>
                <span class="position">Real Estate Agent</span>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-4">
            <div class="team-member">
              <figure>
                <ul class="social">
                  <li><a href="#"><span class="icon-facebook"></span></a></li>
                  <li><a href="#"><span class="icon-twitter"></span></a></li>
                  <li><a href="#"><span class="icon-linkedin"></span></a></li>
                  <li><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
                <img src="images/person_3.jpg" alt="Image" class="img-fluid">
              </figure>
              <div class="p-3 bg-primary">
                <h3 class="mb-2">Ben Thompson</h3>
                <span class="position">Real Estate Agent</span>
              </div>
            </div>
          </div>

          
        </div>
      </div>
    </section>

<?php include 'footer.php' ?>