<?php include 'header.php' ?>

<div class="site-block-wrap" style="height: 500px;">
      <div class="site-blocks-cover overlay overlay-2" style="background-image: url(images/hero_1.jpg); height: 500px;" data-aos="fade" id="home-section">
        <div class="container">
          <div class="row align-items-center justify-content-center pt-5" style="height: 0;">
            <div class="col-md-6 mt-lg-5 text-center">
              <h1 class="text-shadow">CONTCT US</h1>
              <p class="mb-5 text-shadow">Ready to look forward to Quality Contracts, Quality Servicing, and Fewer Defaults? Contact Warehouse now and put a professional team to work for you!</p>
            </div>
          </div>
        </div>
      </div>  
</div> 

<section class="site-section bg-light bg-image" id="contact-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Contct Us</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-7 mb-5">

            

            <form action="#" class="p-5 bg-white">
              
              <h2 class="h4 text-black mb-5">Get In Touch</h2> 

              <div class="row form-group">
                <div class="col-md-6 mb-3 mb-md-0">
                  <label class="text-black" for="fname">First Name</label>
                  <input type="text" id="fname" class="form-control">
                </div>
                <div class="col-md-6">
                  <label class="text-black" for="lname">Last Name</label>
                  <input type="text" id="lname" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="email">Email</label> 
                  <input type="email" id="email" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="subject">Subject</label> 
                  <input type="subject" id="subject" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label class="text-black" for="message">Message</label> 
                  <textarea name="message" id="message" cols="30" rows="7" class="form-control" placeholder="Write your notes or questions here..."></textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <input type="submit" value="Send Message" class="btn btn-primary btn-md text-white">
                </div>
              </div>

  
            </form>
          </div>
          <div class="col-md-5">
            
            <div class="p-4 mb-3 bg-white">
              <p class="mb-0 font-weight-bold">Address</p>
              <p class="mb-4">203 Fake St. Mountain View, San Francisco, California, USA</p>

              <p class="mb-0 font-weight-bold">Phone</p>
              <p class="mb-4"><a href="#">+1 232 3235 324</a></p>

              <p class="mb-0 font-weight-bold">Email Address</p>
              <p class="mb-0"><a href="#">youremail@domain.com</a></p>

            </div>
            <div class="p-4 mb-3 bg-white">
            <style>
      .map-container {
        overflow: hidden;
        position: relative;
        height: 0;
      }

      .map-container iframe {
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        position: absolute;
      }
    </style>

    <!-- Google Maps -->
    <div id="full-width-map" class="z-depth-1-half map-container" style="height: 400px">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d50704.05197778026!2d-122.11628639647361!3d37.413398109434866!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fb7495bec0189%3A0x7c17d44a466baf9b!2sMountain%20View%2C%20California%2C%20EE.%20UU.!5e0!3m2!1ses-419!2smx!4v1600794825308!5m2!1ses-419!2smx" frameborder="0"
        style="border:0" allowfullscreen></iframe>
    </div>
            </div> 
            
          </div>
        </div>
      </div>
    </section>

<?php include 'footer.php' ?>