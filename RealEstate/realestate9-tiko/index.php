<?php include 'header.php' ?>

    
<div class="site-block-wrap">
    <div class="owl-carousel with-dots">
      <div class="site-blocks-cover overlay overlay-2" style="background-image: url(images/hero_1.jpg);" data-aos="fade" id="home-section">


        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-6 mt-lg-5 text-center">
              <h1 class="text-shadow">Buy &amp; Sell Property Here</h1>
              <p class="mb-5 text-shadow">Find an agent who knows your market best. </p>
              <p><a href="contact.php" class="btn btn-primary px-5 py-3">Contact Us</a></p>
              
            </div>
          </div>
        </div>
  
        
      </div>  
  
      <div class="site-blocks-cover overlay overlay-2" style="background-image: url(images/hero_2.jpg);" data-aos="fade" id="home-section">
  
  
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-6 mt-lg-5 text-center">
              <h1 class="text-shadow">Find Your Perfect Property For Your Home</h1>
              <p class="mb-5 text-shadow">Millions of houses and apartments in your favourite cities.</p>
              <p><a href="properties.php" class="btn btn-primary px-5 py-3">Get Started</a></p>
              
            </div>
          </div>
        </div>
  
        
      </div>  
    </div>   
    
  </div>      



  <section class="site-section" id="about-section">
      <div class="container">
        
        <div class="row large-gutters">
          <div class="col-lg-6 mb-5">

              <div class="owl-carousel slide-one-item with-dots">
                  <div><img src="images/img_1.jpg" alt="Image" class="img-fluid"></div>
                  <div><img src="images/img_2.jpg" alt="Image" class="img-fluid"></div>
                  <div><img src="images/img_3.jpg" alt="Image" class="img-fluid"></div>
                </div>

          </div>
          <div class="col-lg-6 ml-auto">
            
            <h2 class="section-title mb-3">Warehouse Real Estate</h2>
                <p class="lead">Hello! Welcome to Warehouse Real Estate.</p>
                <p>Integrity does not bend to the wind of convenience and neither do we. We will always tell you what the facts are, even if it is not what you want to hear, but what are the realities of the situation. We are here to to help you reach your goals in Real Estate, whatever those may be!</p>

                <ul class="list-unstyled ul-check success">
                  <li>Regularly audited by government authorities.</li>
                  <li>Armed with an in-depth knowledge of the escrow process.</li>
                  <li>A licensed and regulated escrow company compliant with Realestate Law.</li>
                  <li>Online escrow that is simple and safe for Buyers and Sellers.</li>
                  <li>The safest service to trust with your money.</li>
                </ul>
                <p><a href="about.php" class="btn btn-primary mr-2 mb-2">Learn More</a></p>
          </div>
        </div>
      </div>
    </section>


    <section class="site-section bg-light" id="services-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Services</h2>
          </div>
        </div>
        <div class="row align-items-stretch">
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-house"></span></div>
              <div>
                <h3>Find Property</h3>
                <p>We help you find a new home by offering a smart real estate.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-coin"></span></div>
              <div>
                <h3>Buy Property</h3>
                <p>Find an agent who knows your market best.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-home"></span></div>
              <div>
                <h3>Beautiful Home</h3>
                <p>The ideal house is waiting for you.</p>
              </div>
            </div>
          </div>


          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="300">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-flat"></span></div>
              <div>
                <h3>Buildings &amp; Lands</h3>
                <p>Sign up now and sell or rent your own properties.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="400">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-location"></span></div>
              <div>
                <h3>Property Locator</h3>
                <p>Millions of houses and apartments in your favourite cities.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="500">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-mobile-phone"></span></div>
              <div>
                <h3>We provide qualified support</h3>
                <p>Our support team is online 24/7, and is ready to help you with any design - related issue.</p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>


  <div class="site-section" id="properties-section">
      <div class="container">
      <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Recent Properties<h2>
          </div>
        </div>
        <div class="row large-gutters">
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
            <div class="ftco-media-1">
              <div class="ftco-media-1-inner">
                <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_1.jpg" alt="FImageo" class="img-fluid"></a>
                <div class="ftco-media-details">
                  <h3>HD17 19 Utica Ave.</h3>
                  <p>New York - USA</p>
                  <strong>$20,000,000</strong>
                </div>
  
              </div> 
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
              <div class="ftco-media-1">
                  <div class="ftco-media-1-inner">
                    <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_2.jpg" alt="Image" class="img-fluid"></a>
                    <div class="ftco-media-details">
                      <h3>HD17 19 Utica Ave.</h3>
                      <p>New York - USA</p>
                      <strong>$20,000,000</strong>
                    </div>
      
                  </div> 
                </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
              <div class="ftco-media-1">
                  <div class="ftco-media-1-inner">
                    <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_3.jpg" alt="Image" class="img-fluid"></a>
                    <div class="ftco-media-details">
                      <h3>HD17 19 Utica Ave.</h3>
                      <p>New York - USA</p>
                      <strong>$20,000,000</strong>
                    </div>
      
                  </div> 
                </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
              <div class="ftco-media-1">
                <div class="ftco-media-1-inner">
                  <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_1.jpg" alt="Image" class="img-fluid"></a>
                  <div class="ftco-media-details">
                    <h3>HD17 19 Utica Ave.</h3>
                    <p>New York - USA</p>
                    <strong>$20,000,000</strong>
                  </div>
    
                </div> 
              </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
                <div class="ftco-media-1">
                    <div class="ftco-media-1-inner">
                      <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_2.jpg" alt="Image" class="img-fluid"></a>
                      <div class="ftco-media-details">
                        <h3>HD17 19 Utica Ave.</h3>
                        <p>New York - USA</p>
                        <strong>$20,000,000</strong>
                      </div>
        
                    </div> 
                  </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
                <div class="ftco-media-1">
                    <div class="ftco-media-1-inner">
                      <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_3.jpg" alt="Image" class="img-fluid"></a>
                      <div class="ftco-media-details">
                        <h3>HD17 19 Utica Ave.</h3>
                        <p>New York - USA</p>
                        <strong>$20,000,000</strong>
                      </div>
        
                    </div> 
                  </div>
            </div>

        </div>
      </div>
    </div>

    <section class="py-5 bg-primary site-section how-it-works" id="howitworks-section">
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-7 text-center">
            <h2 class="section-title mb-3 text-black">How It Works</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 text-center">
            <div class="pr-5 first-step">
              <span class="text-black">01.</span>
              <span class="custom-icon flaticon-house text-black"></span>
              <h3 class="text-black">Find Property.</h3>
              <p class="text-black">We help you find a new home by offering a smart real estate.</p>
            </div>
          </div>

          <div class="col-md-4 text-center">
            <div class="pr-5 second-step">
              <span class="text-black">02.</span>
              <span class="custom-icon flaticon-coin text-black"></span>
              <h3 class="text-dark">Buy Property.</h3>
              <p class="text-black">Millions of houses and apartments in your favourite cities.</p>
            </div>
          </div>

          <div class="col-md-4 text-center">
            <div class="pr-5">
              <span class="text-black">03.</span>
              <span class="custom-icon flaticon-home text-black"></span>
              <h3 class="text-dark">Outstanding Houses.</h3>
              <p class="text-black">Sign up now and sell or rent your own properties.</p>
            </div>
          </div>
        </div>
      </div>  
    </section>

    <section class="site-section" id="agents-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-7 text-left">
            <h2 class="section-title mb-3">Real Estate Agents</h2>
            <p class="lead">Feel free to learn more about our team and company on this page. We are always happy to help you!</p>
          </div>
        </div>
        <div class="row">
          

          <div class="col-md-6 col-lg-4 mb-4">
            <div class="team-member">
              <figure>
                <ul class="social">
                  <li><a href="#"><span class="icon-facebook"></span></a></li>
                  <li><a href="#"><span class="icon-twitter"></span></a></li>
                  <li><a href="#"><span class="icon-linkedin"></span></a></li>
                  <li><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
                <img src="images/person_1.jpg" alt="Image" class="img-fluid">
              </figure>
              <div class="p-3 bg-primary">
                <h3 class="mb-2">Allison Holmes</h3>
                <span class="position">Real Estate Agent</span>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-4">
            <div class="team-member">
              <figure>
                <ul class="social">
                  <li><a href="#"><span class="icon-facebook"></span></a></li>
                  <li><a href="#"><span class="icon-twitter"></span></a></li>
                  <li><a href="#"><span class="icon-linkedin"></span></a></li>
                  <li><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
                <img src="images/person_2.jpg" alt="Image" class="img-fluid">
              </figure>
              <div class="p-3 bg-primary">
                <h3 class="mb-2">Dave Simpson</h3>
                <span class="position">Real Estate Agent</span>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-4">
            <div class="team-member">
              <figure>
                <ul class="social">
                  <li><a href="#"><span class="icon-facebook"></span></a></li>
                  <li><a href="#"><span class="icon-twitter"></span></a></li>
                  <li><a href="#"><span class="icon-linkedin"></span></a></li>
                  <li><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
                <img src="images/person_3.jpg" alt="Image" class="img-fluid">
              </figure>
              <div class="p-3 bg-primary">
                <h3 class="mb-2">Ben Thompson</h3>
                <span class="position">Real Estate Agent</span>
              </div>
            </div>
          </div>

          
        </div>
      </div>
    </section>


    <section class="site-section testimonial-wrap" id="testimonials-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">Testimonials<h2>
          </div>
        </div>

        <div class="row">
            <div class="col-md-6 mb-4">
                <div class="ftco-testimonial-1">
                  <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
                    <img src="images/testimonio2.jpg" alt="Image" class="img-fluid mr-3">
                    <div>
                      <h3>Allison Holmes</h3>
                      <span>Customer</span>
                    </div>
                  </div>
                  <div>
                    <p>I have known Warehouse Real Estate for several years. My wife and I have had the pleasure of working with 
                    Warehouse Real Estate to sell one house and purchase two. Warehouse Real Estate is a true professional. He is bright, thorough, 
                            and honest as the day is long. Through the years, Warehouse Real Estate has become a close and trusted friend. 
                            I would not hesitate to recommend him.</p>
                  </div>
                </div>
              </div>
                <div class="col-md-6 mb-4">
                    <div class="ftco-testimonial-1">
                        <div class="ftco-testimonial-vcard d-flex align-items-center mb-4">
                          <img src="images/testimonio3.jpg" alt="Image" class="img-fluid mr-3">
                          <div>
                            <h3>Allison Holmes</h3>
                            <span>Customer</span>
                          </div>
                        </div>
                        <div>
                          <p>Warehouse Real Estate has represented us on numerous real estate deals both buying and selling. He has 
                            shown right from the start that he is extremely competent in real estate transactions. 
                            We loved how he quickly responded to all of our questions as well as his knowledge of the 
                            whole process from start to finish. Finally, Warehouse Real Estate is very well respected in the real estate 
                            community and I think that goes a long way when it comes to negotiations.</p>
                        </div>
                      </div>
                </div> 
        </div>
      </div>
    </section>

<?php include 'footer.php' ?>