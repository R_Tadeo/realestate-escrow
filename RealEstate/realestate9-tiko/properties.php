<?php include 'header.php' ?>

<div class="site-block-wrap" style="height: 500px;">
      <div class="site-blocks-cover overlay overlay-2" style="background-image: url(images/hero_1.jpg); height: 500px;" data-aos="fade" id="home-section">
        <div class="container">
          <div class="row align-items-center justify-content-center pt-5" style="height: 0;">
            <div class="col-md-6 mt-lg-5 text-center">
              <h1 class="text-shadow">Property Listings</h1>
            </div>
          </div>
        </div>
      </div>  
</div> 

    <div class="site-section" id="listings-section">
      <div class="container">
        
        <div class="row">
          <div class="col-md-3 order-1 order-md-2">
          <div class="mb-5">
            <h3 class="text-black mb-4 h5 font-family-2">Filter</h3>
            <form action="#">
              <div class="form-group">
                <div class="select-wrap">
                  <span class="icon icon-keyboard_arrow_down"></span>
                  <select class="form-control px-3">
                    <option value="">By Date</option>
                    <option value="">Trending</option>
                    <option value="">Best</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="select-wrap">
                  <span class="icon icon-keyboard_arrow_down"></span>
                  <select class="form-control px-3">
                    <option value="">1 Bath, 1 Bedroom</option>
                    <option value="">2 Bath, 2 Bedroom</option>
                    <option value="">3+ Bath, 3+ Bedroom</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="select-wrap">
                  <span class="icon icon-keyboard_arrow_down"></span>
                  <select class="form-control px-3">
                    <option value="">For Sale</option>
                    <option value="">For Rent</option>
                    <option value="">For Lease</option>
                  </select>
                </div>
              </div>
            </form>
            </div>

            <div class="mb-5">
              <h3 class="text-black mb-4 h5 font-family-2">Filter by Price</h3>
              <div id="slider-range" class="border-primary"></div>
              <input type="text" name="text" id="amount" class="form-control border-0 pl-0 bg-white" disabled="" />
            </div>
          </div>
          <div class="col-md-9 order-2 order-md-1">


              <div class="row large-gutters">
                  <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
                    <div class="ftco-media-1">
                      <div class="ftco-media-1-inner">
                        <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_1.jpg" alt="Image" class="img-fluid"></a>
                        <div class="ftco-media-details">
                          <h3>HD17 19 Utica Ave.</h3>
                          <p>New York - USA</p>
                          <strong>$20,000,000</strong>
                        </div>
          
                      </div> 
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
                      <div class="ftco-media-1">
                          <div class="ftco-media-1-inner">
                            <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_2.jpg" alt="Image" class="img-fluid"></a>
                            <div class="ftco-media-details">
                              <h3>HD17 19 Utica Ave.</h3>
                              <p>New York - USA</p>
                              <strong>$20,000,000</strong>
                            </div>
              
                          </div> 
                        </div>
                  </div>
                  <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
                      <div class="ftco-media-1">
                          <div class="ftco-media-1-inner">
                            <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_3.jpg" alt="Image" class="img-fluid"></a>
                            <div class="ftco-media-details">
                              <h3>HD17 19 Utica Ave.</h3>
                              <p>New York - USA</p>
                              <strong>$20,000,000</strong>
                            </div>
              
                          </div> 
                        </div>
                  </div>
        
                  <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
                      <div class="ftco-media-1">
                        <div class="ftco-media-1-inner">
                          <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_1.jpg" alt="Image" class="img-fluid"></a>
                          <div class="ftco-media-details">
                            <h3>HD17 19 Utica Ave.</h3>
                            <p>New York - USA</p>
                            <strong>$20,000,000</strong>
                          </div>
            
                        </div> 
                      </div>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
                        <div class="ftco-media-1">
                            <div class="ftco-media-1-inner">
                              <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_2.jpg" alt="Image" class="img-fluid"></a>
                              <div class="ftco-media-details">
                                <h3>HD17 19 Utica Ave.</h3>
                                <p>New York - USA</p>
                                <strong>$20,000,000</strong>
                              </div>
                
                            </div> 
                          </div>
                    </div>
                    <div class="col-md-6 col-lg-4 mb-5 mb-lg-5 ">
                        <div class="ftco-media-1">
                            <div class="ftco-media-1-inner">
                              <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_3.jpg" alt="Image" class="img-fluid"></a>
                              <div class="ftco-media-details">
                                <h3>HD17 19 Utica Ave.</h3>
                                <p>New York - USA</p>
                                <strong>$20,000,000</strong>
                              </div>
                
                            </div> 
                          </div>
                    </div>
        
                </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section" id="properties-section">
      <div class="container">
        <div class="row mb-5 align-items-center">
          <div class="col-md-7 text-left">
            <h2 class="section-title mb-3">More Properties</h2>
          </div>
          <div class="col-md-5 text-left text-md-right">
            <div class="custom-nav1">
              <a href="#" class="custom-prev1">Previous</a><span class="mx-3">/</span><a href="#" class="custom-next1">Next</a>
            </div>
          </div>
        </div>

        <div class="owl-carousel nonloop-block-13 mb-5">

            <div class="ftco-media-1">
                <div class="ftco-media-1-inner">
                  <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_1.jpg" alt="Image" class="img-fluid"></a>
                  <div class="ftco-media-details">
                    <h3>HD17 19 Utica Ave.</h3>
                    <p>New York - USA</p>
                    <strong>$20,000,000</strong>
                  </div>
    
                </div> 
              </div>


              <div class="ftco-media-1">
                  <div class="ftco-media-1-inner">
                    <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_2.jpg" alt="Image" class="img-fluid"></a>
                    <div class="ftco-media-details">
                      <h3>HD17 19 Utica Ave.</h3>
                      <p>New York - USA</p>
                      <strong>$20,000,000</strong>
                    </div>
      
                  </div> 
                </div>

                <div class="ftco-media-1">
                    <div class="ftco-media-1-inner">
                      <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_3.jpg" alt="Image" class="img-fluid"></a>
                      <div class="ftco-media-details">
                        <h3>HD17 19 Utica Ave.</h3>
                        <p>New York - USA</p>
                        <strong>$20,000,000</strong>
                      </div>
        
                    </div> 
                  </div>


                  <div class="ftco-media-1">
                      <div class="ftco-media-1-inner">
                        <a href="property-single.php" class="d-inline-block mb-4"><img src="images/property_4.jpg" alt="Image" class="img-fluid"></a>
                        <div class="ftco-media-details">
                          <h3>HD17 19 Utica Ave.</h3>
                          <p>New York - USA</p>
                          <strong>$20,000,000</strong>
                        </div>
          
                      </div> 
                    </div>

        </div>
        
      </div>
    </div>


<?php include 'footer.php' ?>