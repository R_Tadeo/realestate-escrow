<?php include 'header.php' ?>

<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/hero_1.jpg);" data-aos="fade">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-5 mx-auto mt-lg-5 text-center">
            <h1>HD17 19 Utica Ave, New York, USA</h1>
            <p class="mb-5"><strong class="text-white">$2,000,000</strong></p>
            
          </div>
        </div>
      </div>

      <a href="#property-details" class="smoothscroll arrow-down"><span class="icon-arrow_downward"></span></a>
    </div>  

    
    
    <div class="site-section" id="property-details">
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <div class="owl-carousel slide-one-item with-dots">
              <div><img src="images/property_1.jpg" alt="Image" class="img-fluid"></div>
              <div><img src="images/property_2.jpg" alt="Image" class="img-fluid"></div>
              <div><img src="images/property_3.jpg" alt="Image" class="img-fluid"></div>
            </div>
          </div>
          <div class="col-lg-5 pl-lg-5 ml-auto">
            <div class="mb-5">
              <h3 class="text-black mb-4">Property Details</h3>
              <p>6 Beds, 4 Baths, 4250 sqft.</p>
              <p>A special offering! This modern East End designer condo lives like a single family home. 4 bedroom/ 2 bath custom home 
                  built by Redfern Properties & Wright-Ryan. The heart of the home features an open great room with floor-to-ceiling windows. 
                  Large custom kitchen w/ Wolf gas range and elegant herringbone tiled backsplash. Living room features gas fireplace, and dining 
                  area flows to front deck. Southwesterly exposure with city views bathes the space in natural light.</p>
              <p>Collaboration with a local designer enhanced the owner's vision & added luxurious details and cohesive style. 
                  Two outdoor spaces, including private rooftop deck & custom bar area. 2-car garage on lower level with generous storage. 
                  If you've been searching for a highly efficient, low maintenance & well designed home - 193 Sheridan Street is at the top of your list!</p>
              <p><a href="contact.php" class="btn btn-primary">Buy Property</a></p>
            </div>

            <div class="mb-5">
              
              <div class="mt-5">
                <img src="images/person_1.jpg" alt="Image" class="w-25 mb-3 rounded-circle">
                <h4 class="text-black">ALLISON HOLMES</h4>
                <p class="text-muted mb-4">Real Estate Agent</p>
                <p>Allison specializes in residential, condominium and multi-family properties in the Greater Portland area.</p>
                <p><a href="contact.php" class="btn btn-primary btn-sm">Contact Agent</a></p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

<?php include 'footer.php' ?>