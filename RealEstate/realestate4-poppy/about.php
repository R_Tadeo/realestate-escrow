<?php include'header.php';?>
<!-- banner -->
<div class="inside-banner">
  <div class="container"> 
    <span class="pull-right"><a href="#">Home</a> / About Us</span>
    <h2>About Us</h2>
</div>
</div>
<!-- banner -->


<div class="container">
<div class="spacer">
<div class="row">
  <div class="col-lg-8  col-lg-offset-2">
  <img src="images/ofiice.jpg" class="img-responsive thumbnail"  alt="realestate">
  <h3>ABOUT</h3>
  <p>With over 15 years experience, who you list or buy real estate with matters. We are dedicated to providing the most up-to-date market data . Our team is made up of caring, knowledgeable professionals that work tirelessly to help you with the home buying and selling process. </p><p>
Contact us today to find out how we can be of assistance to you!.</p>
<P>
we are a company with experience in high-level real estate developments. Our goal is to offer families the home they have always dreamed of, within an atmosphere of modernity and elegance.</P>
<h3>MISSION</h3>
<P>Advise our clients so that they live in the house of their dreams, selling the best existing properties in the market and that they meet their economic, family and personal needs.</P>
<h3>VISION</h3>
<p>To become a reference in the international real estate market, both for the excellence of our executives, and for the transparency, honesty and efficiency of our processes, which allow us to sell, buy or rent a property with the greatest certainty and security. possible.</p>
  </div>


 
</div>
</div>
<center>
<h2>our team</h2></center>
</div>
<div class="container">
<div class="spacer agents">

<div class="row">
  <div class="col-lg-8  col-lg-offset-2 col-sm-12">
      <!-- agents -->
      <div class="row">
        <div class="col-lg-2 col-sm-2 "><a href="#"><img src="images/agents/2.jpg" class="img-responsive"  alt="agent name"></a></div>
        <div class="col-lg-7 col-sm-7 "><h4>John Smith</h4><p>My goal is make you happy and help you to find the right property. I have an extensive network of contacts in the Real Estate including: legal support, tax advise, mortgage consulting, immigration assistance, rental (long term and short term) and im also are a prestige and well known property manager in the last 16 years.</p></div>
        <div class="col-lg-3 col-sm-3 "><span class="glyphicon glyphicon-envelope"></span> <a href="mailto:abc@realestate.com">jsmith@houseservices.com</a><br>
        <span class="glyphicon glyphicon-earphone"></span> (9009) 899 889</div>
      </div>
      <!-- agents -->
      
       <!-- agents -->
      <div class="row">
        <div class="col-lg-2 col-sm-2 "><a href="#"><img src="images/agents/1.jpg" class="img-responsive" alt="agent name"></a></div>
        <div class="col-lg-7 col-sm-7 "><h4>J.K. Jones</h4><p>I'm a 30 year veteran in the hospitality and hotel business in the United States. He spent the vast majority of that time in hotel development, management and construction, have a keen understanding of real estate and investing. </p></div>
        <div class="col-lg-3 col-sm-3 "><span class="glyphicon glyphicon-envelope"></span> <a href="mailto:abc@realestate.com">jkj@houseservices.com</a><br>
        <span class="glyphicon glyphicon-earphone"></span> (9009) 85679 2349</div>
      </div>
    </div>
  </div>
</div>
</div>

<?php include'footer.php';?>