<?php include'header.php';?>
	
	<ul class='slideshow'>
		<li>
			<span>Summer</span>
		</li>
		<li>		
			<span>Fall</span>
		</li>
		<li>		
			<span>Winter</span>
		</li>
		<li>
			<span>Spring</span>
		</li>
	</ul>
	
    <div class="parallax first-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 wow slideInLeft hidden-xs hidden-sm">
                    <div class="contact_form">
                        <h3><a href="http://www.sendanonymousemail.net/" ><i class="fa fa-envelope-o grd1 global-radius"></i> </a>QUICK APPOINTMENT</h3>
                        <form id="contactform1" class="row" name="contactform" method="post">
                            <fieldset class="row-fluid">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="first_name1" id="first_name1" class="form-control" placeholder="First Name">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="last_name1" id="last_name1" class="form-control" placeholder="Last Name">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" name="email1" id="email1" class="form-control" placeholder="Your Email">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="phone1" id="phone1" class="form-control" placeholder="Your Phone">
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label class="sr-only">Select Time</label>
                                    <select name="select_service1" id="select_service1" class="selectpicker form-control" data-style="btn-white">
                                        <option value="selecttime">Select Time</option>
                                        <option value="Weekdays">Weekdays</option>
                                        <option value="Weekend">Weekend</option>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label class="sr-only">What is max price?</label>
                                    <select name="select_price1" id="select_price1" class="selectpicker form-control" data-style="btn-white">
                                        <option value="$100 - $2000">$100 - $2000</option>
                                        <option value="$2000 - $4000">$2000 - $4000</option>
                                        <option value="$4000 - $10000">$4000 - $10000</option>
                                    </select>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                    <button type="submit" value="SEND" id="submit1" class="btn btn-light btn-radius btn-brd grd1 btn-block">Get an Appointment</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
				<div class="col-md-6 col-sm-12">
                    <div class="big-tagline clearfix">
                        <h2>Sell Your Property with we</h2>
                        <p class="lead">With us, you can promote all your real estate and property projects </p>
                        <a data-scroll href="gallery.php" class="btn btn-light btn-radius grd1 btn-brd">View Gallery</a>
                    </div>
                </div>
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end section -->
	
	<div class="about-box">
		<div class="container">
			<div class="row"><center><h1>SERVICES</h1></center>
				<div class="top-feature owl-carousel owl-theme">

					<div class="item">
						<div class="single-feature">
							<div class="icon"><img src="uploads/1.png" class="img-responsive" alt=""></div>
							<h4><a href="#">BUY</a></h4>
							<p>Making a purchase through our services offers a guarantee and security.</p>
						</div> 
					</div>
					<div class="item">
						<div class="single-feature">
							<div class="icon"><img src="uploads/2.png" class="img-responsive" alt=""></div>
							<h4><a href="#">RENT</a></h4>
							<p>We have endless properties for rent in different parts of the world.</p>
						</div> 
					</div>
					<div class="item">
						<div class="single-feature">
							<div class="icon"><img src="uploads/3.png" class="img-responsive" alt=""></div>
							<h4><a href="#">SELL</a></h4>
							<p>selling through our services is relatively easy. </p>
						</div> 
					</div>
					<div class="item">
						<div class="single-feature">
							<div class="icon"><img src="uploads/4.png" class="img-responsive" alt=""></div>
							<h4><a href="#">ADVISORY</a></h4>
							<p>We have specialized agents that will support you at any time,</p>
						</div> 
					</div>
				</div>
			</div>
			
			<hr class="hr1">
			
			<div class="row">
				<div class="col-md-6">
                    <div class="post-media wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                        <img src="uploads/dff.jpeg" alt="" class="img-responsive">                        
                    </div><!-- end media -->
                </div>
				<div class="col-md-6" >
					<div class="message-box right-ab container " style="width: 80%;" >
                        <h4>Awards Winning Real Estate Company</h4>
                        <h2>Welcome to us</h2>
                        <p>We are a company dedicated to real estate. since our inception we have been recognized internationally.</p>
                        <p></p>We have more than 2 decades of experience that support us,we have qualified service agents to support you at any time</p><p>We have a wide portfolio of satisfied clients who have used our services obtaining favorable results</p>
							<a href="about.php" class="btn btn-light btn-radius grd1 btn-brd"> Read More </a>
                    </div>
				</div>
			</div>
			
		</div>
	</div>


    <div id="features" class="section wb">
    <div class="container">
        <hr class="invis"> 

        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="service-widget">
                    <div class="property-main">
                        <div class="property-wrap">
                            <figure class="post-media wow fadeIn">
                                <a href="uploads/estate_01.jpg" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                                <img src="uploads/estate_01.jpg" alt="" class="img-responsive">
                                <div class="label-inner">
                                    <span class="label-status label">Popular</span>
                                </div>
                                <div class="price">
                                    <span class="item-sub-price">$749,900</span> 
                                </div>
                            </figure>
                            <div class="item-body">
                                <h3>3,778 SF Specialty Building Offered</h3>
                                <div class="info">
                                    <p><span>Bedroom: 4</span> <span>Bathroom: 2</span> <span>Land Size: <span class="estate-x-size">5000             </span> <span class="estate-x-unit">square</span></span> <span>Building Size: <span class="estate-x-size">2400</span> <span class="estate-x-unit">square</span></span> </p>
                                     
                                </div>
                              
                            </div>
                        </div>
                       
                    </div>
                </div><!-- end service -->
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="service-widget">
                    <div class="property-main">
                        <div class="property-wrap">
                            <figure class="post-media wow fadeIn">
                                <a href="uploads/estate_03.jpg" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                                <img src="uploads/estate_03.jpg" alt="" class="img-responsive">
                                <div class="label-inner">
                                    <span class="label-status label">Popular</span>
                                </div>
                                <div class="price">
                                    <span class="item-sub-price">$2,695,000 </span> 
                                </div>
                            </figure>
                            <div class="item-body">
                                <h3>859 Diablo Ave</h3>
                                <div class="info">
                                    <p><span>Bedroom: 2</span> <span>Bathroom: 2</span> <span>Land Size: <span class="estate-x-size">3000</span> <span class="estate-x-unit">square</span></span> <span>Building Size: <span class="estate-x-size">1400</span> <span class="estate-x-unit">square</span></span> </p>
                                    <p>House</p> 
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div><!-- end service -->
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="service-widget">
                    <div class="property-main">
                        <div class="property-wrap">
                            <figure class="post-media wow fadeIn">
                                <a href="uploads/estate_02.jpg" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                                <img src="uploads/estate_02.jpg" alt="" class="img-responsive">
                                <div class="label-inner">
                                    <span class="label-status label">Popular</span>
                                </div>
                                <div class="price">
                                    <span class="item-sub-price">$1,125,000 </span> 
                                </div>
                            </figure>
                            <div class="item-body">
                                <h3>2011 W F St -- 2011 W F</h3>
                                <div class="info">
                                    <p><span>Bedroom: 3</span> <span>Bathroom: 2</span> <span>Land Size: <span class="estate-x-size">4000</span> <span class="estate-x-unit">square</span></span> <span>Building Size: <span class="estate-x-size">2000</span> <span class="estate-x-unit">square</span></span> </p>
                                    <p>House</p>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div><!-- end service -->
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</div>
<div id="testimonials" class="section lb">
        <div class="container">
            <div class="section-title row text-center">
                <div class="col-md-8 col-md-offset-2">
                    <h3>Happy Customers</h3>
                    <p class="lead">what our clients think are right when they choose us</p>
                </div><!-- end col -->
            </div><!-- end title -->

            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="testi-carousel owl-carousel owl-theme">
                        <div class="testimonial clearfix">
                            <div class="desc">
                                <h3><i class="fa fa-quote-left"></i> excellent experience<i class="fa fa-quote-right"></i></h3>
                                <p class="lead">they  was a joy to work with!  along with a thorough real estate expertise was invaluable to us  novices.they took time to introduce us to the market and then showed us lots of houses experience was great from start to finish, </p>
								
                            </div>
                            <div class="testi-meta">
                                <img src="images/desc/h30.jpg" alt="" class="img-responsive alignleft">
                                <h4>James Fernando <small>client</small></h4>
                            </div>
                            <!-- end testi-meta -->
                        </div>
                        <!-- end testimonial -->

                        <div class="testimonial clearfix">
                            <div class="desc">
                                <h3><i class="fa fa-quote-left"></i>It was amazing <i class="fa fa-quote-right"></i></h3>
                                <p class="lead">We had a very positive experience with they- both in the sale of our previous home, and the search for & purchase of our new home. they remained calm and confident through all of the problems we encountered along the way, and in turn we stayed positive through the entire process </p>
                            </div>
                            <div class="testi-meta">
                                <img src="images/desc/m35.jpg" alt="" class="img-responsive alignleft">
                                <h4>Jacques Philips <small>-user</small></h4>
                            </div>
                            <!-- end testi-meta -->
                        </div>
                        <!-- end testimonial -->

                        <div class="testimonial clearfix">
                            <div class="desc">
                                <h3><i class="fa fa-quote-left"></i> Extremely competent in real estate transactions! <i class="fa fa-quote-right"></i></h3>
                                <p class="lead">They have represented us in numerous real estate agreements both for purchase and sale. They have proven from the beginning that they are extremely competent in real estate transactions. We loved the speed with which they answered all of our questions, as well as their knowledge of the entire process from start to finish.</p>
                            </div>
                            <div class="testi-meta">
                                <img src="images/desc/m23.jpg" alt="" class="img-responsive alignleft">
                                <h4>Venanda Mercy <small>- user</small></h4>
                            </div>
                            <!-- end testi-meta -->
                        </div>
                        <!-- end testimonial -->
                        <div class="testimonial clearfix">
                            <div class="desc">
                                <h3><i class="fa fa-quote-left"></i> Wonderful Support! <i class="fa fa-quote-right"></i></h3>
                                <p class="lead">They have got my project on time with the competition with a sed highly skilled, and experienced & professional team.</p>
                            </div>
                            <div class="testi-meta">
                                <img src="images/desc/h23.jpg" alt="" class="img-responsive alignleft">
                                <h4>James Fernando <small>- Manager of Racer</small></h4>
                            </div>
                            <!-- end testi-meta -->
                        </div>
                        <!-- end testimonial -->

                       
                        <!-- end testimonial -->

                       <!-- end testimonial -->
                    </div><!-- end carousel -->
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end section -->

    <?php include'footer.php';?>