<?php include'header.php';?>
	
	<div class="all-title-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Properties</h2>
					<!-- Breadcrumbs -->
					<nav id="breadcrumbs">
						<ul>
							<li><a href="index.php">Home</a></li>
							<li>Properties</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
<center>




<div class="row container" style="width: 80%" >

     <!-- properties -->
      <div class="col-lg-4 col-sm-6">
      <div class="properties">
        <div class="image-holder"><img src="images/properties/H6.jpg" class="img-responsive tama" alt="properties">
          <div class="status sold">Sold</div>
        </div>
        <h4><a href="property-detail.php">Royal Inn</a></h4>
        <p class="price">Price: $234,900</p>
        <div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">5</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> </div>
        <a class="btn btn-primary" href="">View Details</a>
      </div>
      </div>
      <!-- properties -->


      <!-- properties -->
      <div class="col-lg-4 col-sm-6">
      <div class="properties">
        <div class="image-holder"><img src="images/properties/H7.jpg" class="img-responsive tama" alt="properties">
          <div class="status sold">Sold</div>
        </div>
        <h4><a href="property-detail.php">Valley Village, CA 91607 </a></h4>
        <p class="price">Contact For Price </p>
        <div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">5</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> </div>
        <a class="btn btn-primary" href="">View Details</a>
      </div>
      </div>
      <!-- properties -->

      <!-- properties -->
      <div class="col-lg-4 col-sm-6">
      <div class="properties">
        <div class="image-holder"><img src="images/properties/H7.jpg" class="img-responsive tama" alt="properties">
          <div class="status sold">available</div>
        </div>
        <h4><a href="property-detail.php"> 33 Horizon Ave 33 Horizon Ave, Venice, CA 90291 </a></h4>
        <p class="price">$2,290 /mont</p>
        <div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">1</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">1</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">1</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> </div>
        <a class="btn btn-primary" href="">View Details</a>
      </div>
      </div>
      <!-- properties -->

      <!-- properties -->
      <div class="col-lg-4 col-sm-6">
      <div class="properties">
        <div class="image-holder"><img src="images/properties/H8.jpg" class="img-responsive tama" alt="properties">
          <div class="status sold">available</div>
        </div>
        <h4><a href="property-detail.php">Fullerton Ave, Chicago, IL 60614</a></h4>
        <p class="price"> $2,550 /mont</p>
        <div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">1</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">1</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">1</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> </div>
        <a class="btn btn-primary" href="">View Details</a>
      </div>
      </div>
      <!-- properties -->

      <!-- properties -->
      <div class="col-lg-4 col-sm-6">
      <div class="properties">
        <div class="image-holder"><img src="images/properties/H9.jpg" class="img-responsive tama" alt="properties">
          <div class="status sold">available</div>
        </div>
        <h4><a href="property-detail.php">Commonwealth Ave, Chicago, IL 60614</a></h4>
        <p class="price">$1,420 /mo</p>
        <div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">1</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">1</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> </div>
        <a class="btn btn-primary" href="">View Details</a>
      </div>
      </div>
      <!-- properties -->

      <!-- properties -->
      <div class="col-lg-4 col-sm-6">
      <div class="properties">
        <div class="image-holder"><img src="images/properties/H10.jpg" class="img-responsive tama" alt="properties">
          <div class="status sold">aviable</div>
        </div>
        <h4><a href="property-detail.php"> 1032 W Standley St, Ukiah, CA 95482 </a></h4>
        <p class="price">$1,700 /mo</p>
        <div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">3</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> </div>
        <a class="btn btn-primary" href="">View Details</a>
      </div>
      </div>
      <!-- properties -->

      <!-- properties -->


      <!-- properties -->
      

      <!-- properties -->
      <div class="center">
<ul class="pagination">
          <li><a href="#">«</a></li>
          <li><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#">»</a></li>
        </ul>
</div>
     
    </div><!-- end section -->
  <?php include'footer.php';?>