<?php include'header.php';?>
	
	<div class="all-title-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>About Us</h2>
					<!-- Breadcrumbs -->
					<nav id="breadcrumbs">
						<ul>
							<li><a href="index.php">Home</a></li>
							<li>About Us</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	
	<div class="about-box">
		<div class="container">
			
			
			<hr class="hr1">
			
			 <div class="row">
                <div class="col-md-6">
                    <div class="post-media wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
                        <img src="uploads/dff.jpeg" alt="" class="img-responsive">                        
                    </div><!-- end media -->
                </div>
                <div class="col-md-6" >
                    <div class="message-box right-ab container " style="width: 80%;" >
                        <h4>Awards Winning Real Estate Company</h4>
                        <h2>Welcome to us</h2>
                        <p>We are a company dedicated to real estate. since our inception we have been recognized internationally.</p>
                        <p></p>We have more than 2 decades of experience that support us,we have qualified service agents to support you at any time</p><p>We have a wide portfolio of satisfied clients who have used our services obtaining favorable results</p>
                            
                    </div>
                </div>

            </div>
			<p>We are a company with decades of experience in the real estate area, in our beginnings we began with a single advisor who took the task of creating the company with its main office in Victoria 8007 Australia from where we have had a large number of branches in various parts of the world we currently have more than 150 branches</p>
		</div>
	</div>

   <div id="testimonials" class="section lb">
        <div class="container">
            <div class="section-title row text-center">
                <div class="col-md-8 col-md-offset-2">
                    <h3>Happy Customers</h3>
                    <p class="lead">what our clients think are right when they choose us</p>
                </div><!-- end col -->
            </div><!-- end title -->

            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="testi-carousel owl-carousel owl-theme">
                        <div class="testimonial clearfix">
                            <div class="desc">
                                <h3><i class="fa fa-quote-left"></i> excellent experience<i class="fa fa-quote-right"></i></h3>
                                <p class="lead">they  was a joy to work with!  along with a thorough real estate expertise was invaluable to us  novices.they took time to introduce us to the market and then showed us lots of houses experience was great from start to finish, </p>
                                
                            </div>
                            <div class="testi-meta">
                                <img src="images/desc/h30.jpg" alt="" class="img-responsive alignleft">
                                <h4>James Fernando <small>client</small></h4>
                            </div>
                            <!-- end testi-meta -->
                        </div>
                        <!-- end testimonial -->

                        <div class="testimonial clearfix">
                            <div class="desc">
                                <h3><i class="fa fa-quote-left"></i>It was amazing <i class="fa fa-quote-right"></i></h3>
                                <p class="lead">We had a very positive experience with they- both in the sale of our previous home, and the search for & purchase of our new home. they remained calm and confident through all of the problems we encountered along the way, and in turn we stayed positive through the entire process </p>
                            </div>
                            <div class="testi-meta">
                                <img src="images/desc/m35.jpg" alt="" class="img-responsive alignleft">
                                <h4>Jacques Philips <small>-user</small></h4>
                            </div>
                            <!-- end testi-meta -->
                        </div>
                        <!-- end testimonial -->

                        <div class="testimonial clearfix">
                            <div class="desc">
                                <h3><i class="fa fa-quote-left"></i> Extremely competent in real estate transactions! <i class="fa fa-quote-right"></i></h3>
                                <p class="lead">They have represented us in numerous real estate agreements both for purchase and sale. They have proven from the beginning that they are extremely competent in real estate transactions. We loved the speed with which they answered all of our questions, as well as their knowledge of the entire process from start to finish.</p>
                            </div>
                            <div class="testi-meta">
                                <img src="images/desc/m23.jpg" alt="" class="img-responsive alignleft">
                                <h4>Venanda Mercy <small>- user</small></h4>
                            </div>
                            <!-- end testi-meta -->
                        </div>
                        <!-- end testimonial -->
                        <div class="testimonial clearfix">
                            <div class="desc">
                                <h3><i class="fa fa-quote-left"></i> Wonderful Support! <i class="fa fa-quote-right"></i></h3>
                                <p class="lead">They have got my project on time with the competition with a sed highly skilled, and experienced & professional team.</p>
                            </div>
                            <div class="testi-meta">
                                <img src="images/desc/h23.jpg" alt="" class="img-responsive alignleft">
                                <h4>James Fernando <small>- Manager of Racer</small></h4>
                            </div>
                            <!-- end testi-meta -->
                        </div>
                        <!-- end testimonial -->

                       
                        <!-- end testimonial -->

                       <!-- end testimonial -->
                    </div><!-- end carousel -->
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end section -->
    </div><!-- end section -->

    <center>><h1>AGENTS</h1></center
    <div class="container">
<div class="spacer agents">

<div class="row">
  <div class="col-lg-8  col-lg-offset-2 col-sm-12">
      <!-- agents -->
      <div class="row">
        <div class="col-lg-2 col-sm-2 "><a href="#"><img src="images/p1.jpg" class="img-responsive"  alt="agent name"></a></div>
        <div class="col-lg-7 col-sm-7 "><h4>Shirley Hisgen</h4><p> I’ve worked in real estate for over 15 years,  motivated by my deep interest in architecture and people. My background is with people and places in which most of my buyers are moving to somewhere unfamiliar in search of a new dream, a new community – a perfect prelude for what I offer .</p></div>
        <div class="col-lg-3 col-sm-3 "><span class="glyphicon glyphicon-envelope"></span> <a href="mailto:abc@realestate.com">asesor@home&ranch.com</a><br>
        <span class="glyphicon glyphicon-earphone"></span> +52 7873487987</div>
      </div>
      <!-- agents -->
      
       <!-- agents -->
      <div class="row">
        <div class="col-lg-2 col-sm-2 "><a href="#"><img src="images/p2.jpg" class="img-responsive" alt="agent name"></a></div>
        <div class="col-lg-7 col-sm-7 "><h4>Melissa Adlerh</h4><p>In the 13 years that I have sold real estate, whether as a licensed agent in New York State where I began my career, or here in Merida, there is one thing that my clients appreciate. What is honesty.</p></div>
        <div class="col-lg-3 col-sm-3 "><span class="glyphicon glyphicon-envelope"></span> <a href="mailto:abc@realestate.com">asesor1@home&ranch.com</a><br>
        <span class="glyphicon glyphicon-earphone"></span>+52 7873487987</div>
      </div>
      <!-- agents -->
      

      <!-- agents -->
      

      <!-- agents -->
      
     
  </div>
</div>


</div>
</div>
<?php include'footer.php';?>
