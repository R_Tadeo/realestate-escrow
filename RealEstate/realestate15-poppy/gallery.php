<?php include'header.php';?>
	
	<div class="all-title-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Gallery</h2>
					<!-- Breadcrumbs -->
					<nav id="breadcrumbs">
						<ul>
							<li><a href="index.php">Home</a></li>
							<li>Gallery</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	
	<div id="gallery" class="section wb">        
        <div class="container">
            <div class="section-title row text-center">
                <div class="col-md-8 col-md-offset-2">
                    <h3>Property Gallery</h3>
                    
                </div><!-- end col -->
            </div><!-- end title -->

            <div id="da-thumbs" class="da-thumbs portfolio">
                <div class="post-media_g pitem item-w1 item-h1 cat1">
                    <a href="uploads/home_01.jpg" data-rel="prettyPhoto[gal]">
                        <img src="uploads/home_01.jpg" alt="" class="img-responsive">
                        <div>
                            <i class="flaticon-unlink"></i>
                        </div>
                    </a>
                </div>
                <div class="post-media_g pitem item-w1 item-h1 cat2">
                    <a href="uploads/home_02.jpg" data-rel="prettyPhoto[gal]">
                        <img src="uploads/home_02.jpg" alt="" class="img-responsive">
                        <div>
                            <i class="flaticon-unlink"></i>
                        </div>
                    </a>
                </div>
                <div class="post-media_g pitem item-w1 item-h1 cat1">
                    <a href="uploads/home_03.jpg" data-rel="prettyPhoto[gal]">
                        <img src="uploads/home_03.jpg" alt="" class="img-responsive">
                        <div>
                            <i class="flaticon-unlink"></i>
                        </div>
                    </a>
                </div>

                <div class="post-media_g pitem item-w1 item-h1 cat3">
                    <a href="uploads/home_04.jpg" data-rel="prettyPhoto[gal]">
                        <img src="uploads/home_04.jpg" alt="" class="img-responsive">
                        <div>
                            <i class="flaticon-unlink"></i>
                        </div>
                    </a>
                </div>
                <div class="post-media_g pitem item-w1 item-h1 cat2">
                    <a href="uploads/home_05.jpg" data-rel="prettyPhoto[gal]">
                        <img src="uploads/home_05.jpg" alt="" class="img-responsive">
                        <div>
                            <i class="flaticon-unlink"></i>
                        </div>
                    </a>
                </div>
                <div class="post-media_g pitem item-w1 item-h1 cat1">
                    <a href="uploads/home_06.jpg" data-rel="prettyPhoto[gal]">
                        <img src="uploads/home_06.jpg" alt="" class="img-responsive">
                        <div>
                            <i class="flaticon-unlink"></i>
                        </div>
                    </a>
                </div>
            </div><!-- end portfolio -->
        </div><!-- end container -->
    </div><!-- end section -->

    

   <?php include'footer.php';?>