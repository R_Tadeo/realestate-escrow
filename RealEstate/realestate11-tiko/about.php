<?php include 'header.php' ?>

<section class="ftco-intro img" id="about-section" style="background-image: url(images/bg_1.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-9 text-center">
						<h2>Choose Your Dream House</h2>
						<p>Millions of houses and apartments in your favourite cities</p>
						<p class="mb-0"><a href="properties.php" class="btn btn-white px-4 py-3">Search Places</a></p>
					</div>
				</div>
			</div>
		</section>

		<section class="ftco-counter img ftco-section ftco-no-pt ftco-no-pb">
    	<div class="container">
    		<div class="row no-gutters d-flex">
    			<div class="col-md-6 col-lg-5 d-flex">
    				<div class="img d-flex align-self-stretch align-items-center" style="background-image:url(images/about.jpg);">
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-7 px-lg-5 py-md-5">
    				<div class="py-md-5">
	    				<div class="row justify-content-start pb-3">
			          <div class="col-md-12 heading-section ftco-animate p-4 p-lg-5">
			            <h2 class="mb-4">Stayhome Real Estate Agency</h2>
			            <p>We are real people, with real goals and dreams, just like our clients. We are more than familiar with all of the questions, concerns and emotions that can go into real estate transactions. We’ll guide you through it with the least amount of stress and negotiate the best deal possible, on your behalf.</p>
			            <p>The Stayhome Team was founded in 2002, by Matthew Robbins, to create a different experience for clients in the Real Estate industry. We aren't here to talk you into anything, but we are here to talk you through the process, so you can make the best and most informed decisions concerning your Real Estate Goals.</p>
			            <p><a href="contact.php" class="btn btn-secondary py-3 px-4">Contact us</a></p>
			          </div>
			        </div>
		        </div>
	        </div>
        </div>
    	</div>
    </section>
    
    <section class="ftco-section ftco-agent" id="agent-section">
    	<div class="container">
    		<div class="row justify-content-center pb-5">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Our Agents</h2>
          </div>
        </div>
    		<div class="row">
        	<div class="col-md-12">
            <div class="carousel-agent owl-carousel">
            	<div class="item">
            		<div class="agent">
		    					<div class="img">
				    				<img src="images/team-1.jpg" class="img-fluid" alt="Colorlib Template">
				    				<div>
				    					<h3>I'm an agent</h3>
				    				</div>
			    				</div>
			    				<div class="desc pt-3">
			    					<div>
				    					<h3><a href="properties.html">James Stallon</a></h3>
											<p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
										</div>
			    				</div>
		    				</div>
            	</div>
            	<div class="item">
            		<div class="agent">
		    					<div class="img">
				    				<img src="images/team-2.jpg" class="img-fluid" alt="Colorlib Template">
				    				<div>
				    					<h3>I'm an agent</h3>
				    				</div>
			    				</div>
			    				<div class="desc pt-3">
			    					<div>
				    					<h3><a href="properties.html">James Stallon</a></h3>
											<p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
										</div>
			    				</div>
		    				</div>
            	</div>
            	<div class="item">
            		<div class="agent">
		    					<div class="img">
				    				<img src="images/team-3.jpg" class="img-fluid" alt="Colorlib Template">
				    				<div>
				    					<h3>I'm an agent</h3>
				    				</div>
			    				</div>
			    				<div class="desc pt-3">
			    					<div>
				    					<h3><a href="properties.html">James Stallon</a></h3>
											<p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
										</div>
			    				</div>
		    				</div>
            	</div>
            	<div class="item">
            		<div class="agent">
		    					<div class="img">
				    				<img src="images/team-4.jpg" class="img-fluid" alt="Colorlib Template">
				    				<div>
				    					<h3>I'm an agent</h3>
				    				</div>
			    				</div>
			    				<div class="desc pt-3">
			    					<div>
				    					<h3><a href="properties.html">James Stallon</a></h3>
											<p class="h-info"><span class="position">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
										</div>
			    				</div>
		    				</div>
            	</div>
            </div>
          </div>
        </div>
    	</div>
    </section>
<?php include 'footer.php' ?>