<?php include 'header.php' ?>

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/image_4.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-start">
          <div class="col-md-9 ftco-animate pb-4">
            <h1 class="mb-3 bread text-white">Property Details</h1>
             <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Properties Single <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>


<div class="container">
<section class="ftco-section ftco-services-2">
      <div class="container">
        <div class="row">
        	<div class="col-md-12 property-wrap mb-5">
    				<div class="row">
    					<div class="col-md-6 d-flex ftco-animate">
    						<div class="img align-self-stretch" style="background-image: url(images/work-2.jpg);"></div>
    					</div>
    					<div class="col-md-6 ftco-animate py-5">
    						<div class="text py-5 pl-md-5">
    							<div class="d-flex">
			    					<div>
				    					<h3><a href="properties-single.html">Fatima Subdivision</a></h3>
										</div>
										<div class="pl-md-4">
											<h4 class="price">$120,000</h4>
										</div>
									</div>
    							<p>A special offering! This modern East End designer condo lives like a single family home. 4 bedroom/ 4 bath custom home 
                  built by Redfern Properties & Wright-Ryan. The heart of the home features an open great room with floor-to-ceiling windows. 
                  Large custom kitchen w/ Wolf gas range and elegant herringbone tiled backsplash. Living room features gas fireplace, and dining 
                  area flows to front deck. Southwesterly exposure with city views bathes the space in natural light.</p>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="col-md-12 tour-wrap">
    				<table class="table">
					    <tbody>
					      <tr>
				      	  <th scope="row">Lot area</th>
					        <td>
					        	<p>1,250 SQ FT</p>
					        </td>
					        <td></td>
					      </tr><!-- END TR-->

					      <tr>
				      	  <th scope="row">Floor Area</th>
					        <td>
					        	<p>1,300 SQ FT</p>
					        </td>
					        <td></td>
					      </tr><!-- END TR-->

					      <tr>
				      	  <th scope="row">Bedroom</th>
					        <td>
					        	<p>4 Bedrooms</p>
					        </td>
					        <td></td>
					      </tr><!-- END TR-->

					      <tr>
				      	  <th scope="row">Bathroom</th>
					        <td>
					        	<p>4 Bathrooms</p>
					        </td>
					        <td></td>
					      </tr><!-- END TR-->

					      <tr>
				      	  <th scope="row">Garage</th>
					        <td>
					        	<p>2 Garage</p>
					        </td>
					        <td></td>
					      </tr><!-- END TR-->


					      <tr>
				      	  <th scope="row">Included</th>
					        <td class="d-flex">
					        	<ul>
					        		<li>Year Built: 2019</li>
					        		<li>Roofing New</li>
					        	</ul>
					        	<ul>
					        		<li>Free Taxes</li>
					        		<li>Agent</li>
					        	</ul>
					        </td>
					        <td></td>
					      </tr><!-- END TR-->

					      <tr>
				      	  <th scope="row">Not Included</th>
					        <td class="d-flex">
					        	<ul>
					        		<li>Taxes</li>
					        	</ul>
					        	<ul>
					        		<li>Entry Fees</li>
					        	</ul>
					        </td>
					        <td></td>
					      </tr><!-- END TR-->
					      <tr>
				      	  <th scope="row">Maps</th>
					        <td>
										<div id="map"></div>
					        </td>
					      </tr><!-- END TR-->
					    </tbody>
					  </table>
    			</div>
    			<div class="col-md-12 tour-wrap">
    				<div class="pt-5 mt-5">
              <h3 class="mb-5">Reviews</h3>
              <ul class="comment-list">
                <li class="comment">
                  <div class="vcard bio">
                    <img src="images/person_1.jpg" alt="Image placeholder">
                  </div>
                  <div class="comment-body">
                    <h3>John Doe</h3>
                    <div class="meta">October 03, 2018 at 2:21pm</div>
                    <p>I worked with Clara to find an apartment last minute. Clara went above and beyond to help. When I ran into issues with the application, he worked tirelessly to help resolve it. He is also very responsive and communicative. I would definitely recommend working with him. I love my New apartment!</p>
                    <p><a href="#" class="reply">Reply</a></p>
                  </div>
                </li>
              <!-- END comment-list -->
              
              <div class="comment-form-wrap pt-5">
                <h3 class="mb-5">Leave a comment</h3>
                <form action="#" class="p-5 bg-light">
                  <div class="form-group">
                    <label for="name">Name *</label>
                    <input type="text" class="form-control" id="name">
                  </div>
                  <div class="form-group">
                    <label for="email">Email *</label>
                    <input type="email" class="form-control" id="email">
                  </div>
                  <div class="form-group">
                    <label for="website">Website</label>
                    <input type="url" class="form-control" id="website">
                  </div>

                  <div class="form-group">
                    <label for="message">Message</label>
                    <textarea name="" id="message" cols="30" rows="10" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                    <input type="submit" value="Post Comment" class="btn py-3 px-4 btn-primary">
                  </div>

                </form>
              </div>
            </div>
    			</div>
        </div>
      </div>
    </section> <!-- .section -->
</div>


<?php include 'footer.php' ?>