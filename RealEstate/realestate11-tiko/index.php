<?php include 'header.php' ?>

<section class="hero-wrap js-fullheight" style="background-image: url('images/bg_2.jpg');" data-section="home" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-md-5 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
            <h1 class="mb-5" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Get your way home worldwide</h1>
            <p class="mb-5" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Millions of houses and apartments in your favourite cities</p>
            <form action="#" class="search-location">
	        		<div class="row">
	        			<div class="col-lg align-items-end">
	        				<div class="form-group">
	          				<div class="form-field">
						<p class="mb-0"><a href="properties.php" class="btn btn-white px-4 py-3">Search Places</a></p>
			              </div>
		              </div>
	        			</div>
	        		</div>
	        	</form>
          </div>
        </div>
      </div>
    </section>

		
		<section class="ftco-section ftco-services-2 bg-light" id="services-section">
			<div class="container">
				<div class="row justify-content-center pb-5">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Our Services</h2>
            <p>Contact us now and sell or rent your own properties.</p>
          </div>
        </div>
        <div class="row">
        	<div class="col-md d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services text-center d-block">
              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-purse"></span></div>
              <div class="media-body">
                <h3 class="heading mb-3">Making Money.</h3>
                <p>We have properties to satisfy the needs of our clients from low cost to mansions, we adjust to your needs, we help you fulfill your dreams.</p>
              </div>
            </div>      
          </div>
          <div class="col-md d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services text-center d-block mt-lg-5 pt-lg-4">
              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-detective"></span></div>
              <div class="media-body">
                <h3 class="heading mb-3">We have the best agents.</h3>
                <p>The most experienced and best-prepared team to help you find the best home for you.</p>
              </div>
            </div>      
          </div>
          <div class="col-md d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services text-center d-block">
              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-house"></span></div>
              <div class="media-body">
                <h3 class="heading mb-3">Buy &amp; Rent Modern Properties.</h3>
                <p>We have various proposals for you both for sale and for rent we are the best solution for you.</p>
              </div>
            </div>      
          </div>
          <div class="col-md d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services text-center d-block mt-lg-5 pt-lg-4">
              <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-pin"></span></div>
              <div class="media-body">
                <h3 class="heading mb-3">Find Places Anywhere in the World.</h3>
                <p>We are an international company so we have properties around the world. </p>
              </div>
            </div>      
          </div>
        </div>
			</div>
		</section>

		<section class="ftco-section ftco-properties" id="properties-section">
    	<div class="container">
    		<div class="row justify-content-center pb-5">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Our Property</h2>
          </div>
        </div>
    		<div class="row">
        	<div class="col-md-12">
            <div class="carousel-properties owl-carousel">
            	<div class="item">
            		<div class="properties ftco-animate">
		    					<div class="img">
				    				<img src="images/work-1.jpg" class="img-fluid" alt="Colorlib Template">
			    				</div>
			    				<div class="desc">
			    					<div class="text bg-primary d-flex text-center align-items-center justify-content-center">
				    					<span>Sale</span>
				    				</div>
			    					<div class="d-flex pt-5">
				    					<div>
					    					<h3><a href="property.php">Fatima Subdivision</a></h3>
											</div>
											<div class="pl-md-4">
												<h4 class="price">$120,000</h4>
											</div>
										</div>
										<p class="h-info"><span class="location">New York</span> <span class="details">&mdash; 3bds, 2bath</span></p>
			    				</div>
		    				</div>
            	</div>
            	<div class="item">
            		<div class="properties ftco-animate">
		    					<div class="img">
				    				<img src="images/work-2.jpg" class="img-fluid" alt="Colorlib Template">
			    				</div>
			    				<div class="desc">
			    					<div class="text bg-secondary d-flex text-center align-items-center justify-content-center">
				    					<span>Rent</span>
				    				</div>
			    					<div class="d-flex pt-5">
				    					<div>
					    					<h3><a href="property.php">Fatima Subdivision</a></h3>
											</div>
											<div class="pl-md-4">
												<h4 class="price">$120<span>/mo</span></h4>
											</div>
										</div>
										<p class="h-info"><span class="location">New York</span> <span class="details">&mdash; 3bds, 2bath</span></p>
			    				</div>
		    				</div>
            	</div>
            	<div class="item">
            		<div class="properties ftco-animate">
		    					<div class="img">
				    				<img src="images/work-3.jpg" class="img-fluid" alt="Colorlib Template">
			    				</div>
			    				<div class="desc">
			    					<div class="text bg-primary d-flex text-center align-items-center justify-content-center">
				    					<span>Sale</span>
				    				</div>
			    					<div class="d-flex pt-5">
				    					<div>
					    					<h3><a href="property.php">Fatima Subdivision</a></h3>
											</div>
											<div class="pl-md-4">
												<h4 class="price">$230,000</h4>
											</div>
										</div>
										<p class="h-info"><span class="location">New York</span> <span class="details">&mdash; 3bds, 2bath</span></p>
			    				</div>
		    				</div>
            	</div>
            	<div class="item">
            		<div class="properties ftco-animate">
		    					<div class="img">
				    				<img src="images/work-4.jpg" class="img-fluid" alt="Colorlib Template">
			    				</div>
			    				<div class="desc">
			    					<div class="text bg-primary d-flex text-center align-items-center justify-content-center">
				    					<span>Sale</span>
				    				</div>
			    					<div class="d-flex pt-5">
				    					<div>
					    					<h3><a href="property.php">Fatima Subdivision</a></h3>
											</div>
											<div class="pl-md-4">
												<h4 class="price">$120,000</h4>
											</div>
										</div>
										<p class="h-info"><span class="location">New York</span> <span class="details">&mdash; 3bds, 2bath</span></p>
			    				</div>
		    				</div>
            	</div>
            	<div class="item">
            		<div class="properties ftco-animate">
		    					<div class="img">
				    				<img src="images/work-5.jpg" class="img-fluid" alt="Colorlib Template">
			    				</div>
			    				<div class="desc">
			    					<div class="text bg-secondary d-flex text-center align-items-center justify-content-center">
				    					<span>Rent</span>
				    				</div>
			    					<div class="d-flex pt-5">
				    					<div>
					    					<h3><a href="property.php">Fatima Subdivision</a></h3>
											</div>
											<div class="pl-md-4">
												<h4 class="price">$50<span>/mo</span></h4>
											</div>
										</div>
										<p class="h-info"><span class="location">New York</span> <span class="details">&mdash; 3bds, 2bath</span></p>
			    				</div>
		    				</div>
            	</div>
            </div>
          </div>
        </div>
    	</div>
    </section>

    <section class="ftco-intro img" id="about-section" style="background-image: url(images/bg_1.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-9 text-center">
						<h2>Choose Your Dream House</h2>
						<p>Millions of houses and apartments in your favourite cities</p>
						<p class="mb-0"><a href="properties.php" class="btn btn-white px-4 py-3">Search Places</a></p>
					</div>
				</div>
			</div>
		</section>

		<section class="ftco-counter img ftco-section ftco-no-pt ftco-no-pb">
    	<div class="container">
    		<div class="row no-gutters d-flex">
    			<div class="col-md-6 col-lg-5 d-flex">
    				<div class="img d-flex align-self-stretch align-items-center" style="background-image:url(images/about.jpg);">
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-7 px-lg-5 py-md-5">
    				<div class="py-md-5">
	    				<div class="row justify-content-start pb-3">
			          <div class="col-md-12 heading-section ftco-animate p-4 p-lg-5">
			            <h2 class="mb-4">Stayhome Real Estate Agency</h2>
			            <p>We are real people, with real goals and dreams, just like our clients. We are more than familiar with all of the questions, concerns and emotions that can go into real estate transactions. We’ll guide you through it with the least amount of stress and negotiate the best deal possible, on your behalf.</p>
			            <p>The Stayhome Team was founded in 2002, by Matthew Robbins, to create a different experience for clients in the Real Estate industry. We aren't here to talk you into anything, but we are here to talk you through the process, so you can make the best and most informed decisions concerning your Real Estate Goals.</p>
			            <p><a href="contact.php" class="btn btn-secondary py-3 px-4">Contact us</a></p>
			          </div>
			        </div>
		        </div>
	        </div>
        </div>
    	</div>
    </section>

    <section class="ftco-section ftco-services-2 bg-light" id="workflow-section">
			<div class="container">
				<div class="row">
          <div class="col-md-4 heading-section ftco-animate">
            <h2 class="mb-4">How it works</h2>
            <p>FIND YOUR DREAM HOUSE</p>
            <div class="media block-6 services text-center d-block pt-md-5 mt-md-5">
              <div class="icon justify-content-center align-items-center d-flex"><span>1</span></div>
              <div class="media-body p-md-3">
                <h3 class="heading mb-3">Largest Database</h3>
                <p class="mb-5">Nestpick is a free to use search engine: we work with more than 100+ partners and provide you with the biggest and best selection of mid-to-long term furnished apartments for rent on the internet, in over 200 cities around the world.</p>
                <hr>
              </div>
            </div>
          </div>
          <div class="col-md-4 d-flex align-self-stretch ftco-animate mt-lg-5">
            <div class="media block-6 services text-center d-block mt-lg-5 pt-md-5 pt-lg-4">
              <div class="icon justify-content-center align-items-center d-flex"><span>2</span></div>
              <div class="media-body p-md-3">
                <h3 class="heading mb-3">Relevant Results Delivered to You</h3>
                <p class="mb-5">We compare millions of listings of furnished apartments for rent and provide you with the most relevant results. Filter by neighborhood; choose between luxury apartments and rooms; and search using pinned locations on the city map.</p>
                <hr>
              </div>
            </div>      
          </div>
          <div class="col-md-4 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services text-center d-block">
              <div class="icon justify-content-center align-items-center d-flex"><span>3</span></div>
              <div class="media-body p-md-3">
                <h3 class="heading mb-3">Top Vetted Providers in One Place</h3>
                <p class="mb-5">We only work with trustworthy partners and deliver to you the most reliable and updated listings. From Berlin to San Francisco, select your next destination and view the available properties.</p>
                <hr>
              </div>
            </div>      
          </div>
        </div>
			</div>
		</section>

		<section class="ftco-intro img" id="hotel-section" style="background-image: url(images/bg_4.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row justify-content-end">
					<div class="col-md-7">
						<h2 class="mb-4">Choose Your House for Only <span>$120,000</span></h2>
                        <p>Find the perfect Real Estate Agent Near You.</p>
						<p class="mb-0"><a href="properties.php" class="btn btn-white px-4 py-3">Advance Search</a></p>
					</div>
				</div>
			</div>
		</section>

		<section class="ftco-section ftco-agent" id="agent-section">
    	<div class="container">
    		<div class="row justify-content-center pb-5">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h2 class="mb-4">Our Agents</h2>
          </div>
        </div>
    		<div class="row">
        	<div class="col-md-12">
            <div class="carousel-agent owl-carousel">
            	<div class="item">
            		<div class="agent">
		    					<div class="img">
				    				<img src="images/team-1.jpg" class="img-fluid" alt="Colorlib Template">
				    				<div>
				    					<h3>I'm an agent</h3>
				    				</div>
			    				</div>
			    				<div class="desc pt-3">
			    					<div>
				    					<h3><a href="properties.html">James Stallon</a></h3>
											<p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
										</div>
			    				</div>
		    				</div>
            	</div>
            	<div class="item">
            		<div class="agent">
		    					<div class="img">
				    				<img src="images/team-2.jpg" class="img-fluid" alt="Colorlib Template">
				    				<div>
				    					<h3>I'm an agent</h3>
				    				</div>
			    				</div>
			    				<div class="desc pt-3">
			    					<div>
				    					<h3><a href="properties.html">James Stallon</a></h3>
											<p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
										</div>
			    				</div>
		    				</div>
            	</div>
            	<div class="item">
            		<div class="agent">
		    					<div class="img">
				    				<img src="images/team-3.jpg" class="img-fluid" alt="Colorlib Template">
				    				<div>
				    					<h3>I'm an agent</h3>
				    				</div>
			    				</div>
			    				<div class="desc pt-3">
			    					<div>
				    					<h3><a href="properties.html">James Stallon</a></h3>
											<p class="h-info"><span class="location">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
										</div>
			    				</div>
		    				</div>
            	</div>
            	<div class="item">
            		<div class="agent">
		    					<div class="img">
				    				<img src="images/team-4.jpg" class="img-fluid" alt="Colorlib Template">
				    				<div>
				    					<h3>I'm an agent</h3>
				    				</div>
			    				</div>
			    				<div class="desc pt-3">
			    					<div>
				    					<h3><a href="properties.html">James Stallon</a></h3>
											<p class="h-info"><span class="position">Listing</span> <span class="details">&mdash; 10 Properties</span></p>
										</div>
			    				</div>
		    				</div>
            	</div>
            </div>
          </div>
        </div>
    	</div>
    </section>

    <section class="ftco-section testimony-section">
      <div class="container">
        <div class="row justify-content-center pb-3">
          <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
          	<span class="subheading">Read testimonials</span>
            <h2 class="mb-4">What Client Says</h2>
          </div>
        </div>
        <div class="row ftco-animate justify-content-center">
          <div class="col-md-12">
            <div class="carousel-testimony owl-carousel ftco-owl">
              <div class="item">
                <div class="testimony-wrap text-center py-4 pb-5">
                  <div class="user-img" style="background-image: url(images/person_1.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                  </div>
                  <div class="text px-4 pb-5">
                    <p class="mb-4">This is the second home I have purchased, and they has shown a high degree of professionalism. He devoted a lot of time, they are very thorough, and very patient. I have recommended to many friends over the years.</p>
                    <p class="name">Jeff F.</p>
                    <span class="position">Client</span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap text-center py-4 pb-5">
                  <div class="user-img" style="background-image: url(images/person_2.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                  </div>
                  <div class="text px-4 pb-5">
                    <p class="mb-4">they did an excellent job. We had a limited amount of time to look for housing during a trip to Fargo. they sent e-mails or called in order to narrow our search, which made the process so efficient.</p>
                    <p class="name">Peter S.</p>
                    <span class="position">Client</span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap text-center py-4 pb-5">
                  <div class="user-img" style="background-image: url(images/person_3.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                  </div>
                  <div class="text px-4 pb-5">
                    <p class="mb-4">Excellent service. they have  a wealth of knowledge. they  goes the extra mile to ensure that his customers are happy and that things happen in a timely fashion.</p>
                    <p class="name">George A.</p>
                    <span class="position">Client</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


<?php include 'footer.php' ?>