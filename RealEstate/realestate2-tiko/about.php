<?php include 'header.php' ?>

<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section spad set-bg wow fadeInRight" data-setbg="img/breadcrumb-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 wow fadeInLeft">
                    <div class="breadcrumb-text">
                        <h4>About us</h4>
                        <div class="bt-option">
                            <a href="./index.php"><i class="fa fa-home"></i> Home</a>
                            <span>About</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- About Section Begin -->
    <section class="about-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 wow fadeInLeft">
                    <div class="about-text">
                        <div class="at-title">
                            <h3>Welcom to RealEstate</h3>
                            <p>RealEstate remains proudly committed to serving our original markets — New York City, the Hamptons, and South Florida. But we’re growing, too, bringing our name and values to big cities and small towns across the country and around the world.</p>
                        </div>
                        <div class="at-feature">
                            <div class="af-item wow fadeInUp">
                                <div class="af-icon">
                                    <img src="img/chooseus/chooseus-icon-1.png" alt="">
                                </div>
                                <div class="af-text">
                                    <h6>Find your future home</h6>
                                    <p>We help you find a new home by offering a smart real estate.</p>
                                </div>
                            </div>
                            <div class="af-item wow fadeInUp">
                                <div class="af-icon">
                                    <img src="img/chooseus/chooseus-icon-2.png" alt="">
                                </div>
                                <div class="af-text">
                                    <h6>Experienced agents</h6>
                                    <p>Find an agent who knows your market best</p>
                                </div>
                            </div>
                            <div class="af-item wow fadeInUp">
                                <div class="af-icon">
                                    <img src="img/chooseus/chooseus-icon-3.png" alt="">
                                </div>
                                <div class="af-text">
                                    <h6>Buy or rent homes</h6>
                                    <p>Millions of houses and apartments in your favourite cities</p>
                                </div>
                            </div>
                            <div class="af-item wow fadeInUp">
                                <div class="af-icon">
                                    <img src="img/chooseus/chooseus-icon-4.png" alt="">
                                </div>
                                <div class="af-text">
                                    <h6>List your own property</h6>
                                    <p>Sign up now and sell or rent your own properties</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInRight">
                    <div class="about-pic set-bg" data-setbg="img/about-us.jpg">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Section End -->

    <div class="container mb-5">


  <!--Section: Content-->
  <section class="dark-grey-text">
    <!-- Section heading -->
    <h2 class="text-center font-weight-bold mb-4 pb-2 wow fadeInLeft">Our Mission</h2>
    <!-- Section description -->
    <p class="text-justify lead grey-text mx-auto mb-5 wow fadeInLeft">
    Since 1998, RealEstate agents have been earning―and keeping―our clients' trust with an unwavering commitment to white-glove service, 
    expertise, and integrity. In every market we serve, we are a growing family of world-class professionals, combining real estate expertise 
    with unrivaled market knowledge and neighborhood fluency.
    </p>
    <!-- Grid row -->
    <div class="row">
      <!-- Grid column -->
      <div class="col-lg-5 text-center text-lg-left wow fadeInLeft">
        <img class="img-fluid" src="img/about1.jpg" alt="Sample image">
      </div>
      <!-- Grid column -->
      <!-- Grid column -->
      <div class="col-lg-7">
        <!-- Grid row -->
        <div class="row mb-3">
          <!-- Grid column -->
          <div class="col-1 wow fadeInRight">
          </div>
          <!-- Grid column -->
          <!-- Grid column -->
          <div class="col-xl-10 col-md-11 col-10">
            <h5 class="font-weight-bold mb-3 wow fadeInRight"><i class="icon_briefcase"></i> Experience</h5>
            <p class="grey-text wow fadeInUp">For close to 20 years, Corcoran has been the first name in residential real estate. In every market we serve, 
            coast to coast and around the world, our agents are the acknowledged experts.
            </p>
          </div>
          <!-- Grid column -->
        </div>
        <!-- Grid row -->
        <!-- Grid row -->
        <div class="row mb-3">
          <!-- Grid column -->
          <div class="col-1 wow fadeInRight">
          </div>
          <!-- Grid column -->
          <!-- Grid column -->
          <div class="col-xl-10 col-md-11 col-10">
            <h5 class="font-weight-bold mb-3 wow fadeInRight"><i class="icon_easel"></i> Innovation</h5>
            <p class="grey-text wow fadeInUp">Staying ahead of the curve matters, and RealEstate supports its agents with the real estate industry’s most experienced and 
            effective marketing and technology teams.
            </p>
          </div>
          <!-- Grid column -->
        </div>
        <!-- Grid row -->
        <!--Grid row-->
        <div class="row">
          <!-- Grid column -->
          <div class="col-1 wow fadeInRight">
          </div>
          <!-- Grid column -->
          <!-- Grid column -->
          <div class="col-xl-10 col-md-11 col-10">
            <h5 class="font-weight-bold mb-3 wow fadeInRight"><i class="icon_group"></i> Integrity</h5>
            <p class="grey-text mb-0 wow fadeInUp">We’re part of the trusted Realogy family of real estate companies, named to the Ethisphere Institute’s 
            annual list of the World’s Most Ethical Companies for eight straight years.
            </p>
          </div>
          <!-- Grid column -->
        </div>
        <!--Grid row-->
      </div>
      <!--Grid column-->
    </div>
    <!-- Grid row -->
  </section>
  <!--Section: Content-->
</div>

   <!-- Team Section Begin -->
   <section class="team-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 wow fadeInLeft">
                    <div class="section-title">
                        <h4>Best Sgents</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 wow fadeInLeft">
                    <div class="ts-item">
                        <div class="ts-text">
                            <img src="img/team/team-2.jpg" alt="">
                            <h5>Clara Valderrama</h5>
                            <span>123-455-688</span>
                            <p>Clara Valderrama offers her clients 28 years of real estate sales experience in New York. She is a recipient of the prestigious Legend Award for being among the top sales agents with the Douglas Elliman Network for five years in a row.</p>
                            <div class="ts-social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 wow fadeInRight">
                    <div class="ts-item">
                        <div class="ts-text">
                            <img src="img/team/team-3.jpg" alt="">
                            <h5>Raja Tayeb</h5>
                            <span>123-455-688</span>
                            <p>Let us help you find everything you need to know about buying or selling a home! As one of the preeminent, professional real estate teams in our community since 2004, we are dedicated to providing the finest service available while breaking new ground.</p>
                            <div class="ts-social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Team Section End -->

    <!-- Testimonial Section Begin -->
    <section class="testimonial-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 wow fadeInUp">
                    <div class="section-title">
                        <h4>What our client says?</h4>
                    </div>
                </div>
            </div>
            <div class="row testimonial-slider owl-carousel">
                <div class="col-lg-6 wow fadeInLeft">
                    <div class="testimonial-item">
                        <div class="ti-text">
                            <p>
                            Clara & Raja went above and beyond to both help me sell and buy simultaneously; they deserve two 5-star 
                            reviews from me! RealEstate provides a personally tailored experience with a warm, caring touch.
                            </p>
                        </div>
                        <div class="ti-author">
                            <div class="ta-pic">
                                <img src="img/testimonial-author/ta-1.jpg" alt="">
                            </div>
                            <div class="ta-text">
                                <h5>BRANDON KELLEY</h5>
                                <span>Designer</span>
                                <div class="ta-rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInRight">
                    <div class="testimonial-item">
                        <div class="ti-text">
                            <p>
                                I worked with Clara to find an apartment last minute. Clara went above and beyond to help. When I ran into issues 
                                with the application, he worked tirelessly to help resolve it. He is also very responsive and communicative. I would 
                                definitely recommend working with him. I love my New apartment!
                            </p>
                        </div>
                        <div class="ti-author">
                            <div class="ta-pic">
                                <img src="img/testimonial-author/ta-2.jpg" alt="">
                            </div>
                            <div class="ta-text">
                                <h5>MATTHEW NELSON</h5>
                                <span>Designer</span>
                                <div class="ta-rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->

    <!-- Contact Section Begin -->
    <section class="contact-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact-info">
                        <div class="ci-item wow fadeInUp">
                            <div class="ci-icon">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="ci-text">
                                <h5>Address</h5>
                                <p>160 Pennsylvania Ave NW, Washington, Castle, PA 16101-5161</p>
                            </div>
                        </div>
                        <div class="ci-item wow fadeInUp">
                            <div class="ci-icon">
                                <i class="fa fa-mobile"></i>
                            </div>
                            <div class="ci-text">
                                <h5>Phone</h5>
                                <ul>
                                    <li>125-711-811</li>
                                    <li>125-668-886</li>
                                </ul>
                            </div>
                        </div>
                        <div class="ci-item wow fadeInUp">
                            <div class="ci-icon">
                                <i class="fa fa-headphones"></i>
                            </div>
                            <div class="ci-text">
                                <h5>Support</h5>
                                <p>Support.aler@gmail.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cs-map wow fadeInRight">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d735515.5813275519!2d-80.41163541934742!3d43.93644386501528!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882a55bbf3de23d7%3A0x3ada5af229b47375!2sMono%2C%20ON%2C%20Canada!5e0!3m2!1sen!2sbd!4v1583262687289!5m2!1sen!2sbd"
                height="450" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </section>
    <!-- Contact Section End -->

<?php include 'footer.php' ?>