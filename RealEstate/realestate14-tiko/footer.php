	<!-- Footer section -->
	<footer class="footer-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-2 col-sm-12">
					<div class="footer-widget">
						<img src="img/log-color.png" alt="">
					</div>
					<p>Find the Perfect Real Estate Agent Near You</p>
				</div>
				<div class="col-lg-2 col-md-3 col-sm-4">
					<div class="footer-widget">
						<h5>Links</h5>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li><a href="about.php">About Us</a></li>
							<li><a href="contact.php">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-2 col-md-3 col-sm-4">
					<div class="footer-widget">
						<h5>Properties</h5>
						<ul>
							<li><a href="properties.php">On sale</a></li>
							<li><a href="properties.php">On rent</a></li>
							<li><a href="properties.php">Coming soon</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-4">
					<div class="footer-widget">
						<h5>SOCIAL</h5>
						<div class="footer-social">
							<a href=""><i class="fa fa-facebook"></i></a>
							<a href=""><i class="fa fa-linkedin"></i></a>
							<a href=""><i class="fa fa-twitter"></i></a>
							<a href=""><i class="fa fa-youtube-play"></i></a>
							<a href=""><i class="fa fa-instagram"></i></a>
						</div>
					</div>
				</div>
			</div>
			<div class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Estate
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
		</div>
	</footer>
	<!-- Footer section end-->
	
	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/main.js"></script>

	</body>
</html>
