<?php include 'header.php' ?>

	<!-- Hero section -->
	<section class="hero-section">
		<div class="hero-social-warp">
			<div class="hero-social">
				<a href=""><i class="fa fa-facebook"></i></a>
				<a href=""><i class="fa fa-linkedin"></i></a>
				<a href=""><i class="fa fa-twitter"></i></a>
				<a href=""><i class="fa fa-youtube-play"></i></a>
				<a href=""><i class="fa fa-instagram"></i></a>
			</div>
		</div>
		<div class="hero-slider owl-carousel owl-theme">
			<div class="hs-item set-bg" data-setbg="img/slider/1.jpg">
				<div class="container">
					<div class="row">
						<div class="col-xl-6 col-lg-7">
							<h2>3 Modern homes for modern thinking people. </h2>
						</div>
					</div>
				</div>
			</div>
			<div class="hs-item set-bg" data-setbg="img/slider/2.jpg">
				<div class="container">
					<div class="row">
						<div class="col-xl-6 col-lg-7">
							<h2>4 Modern homes for modern thinking people. </h2>
						</div>
					</div>
				</div>
			</div>
			<div class="hs-item set-bg" data-setbg="img/slider/3.jpg">
				<div class="container">
					<div class="row">
						<div class="col-xl-6 col-lg-7">
							<h2>6 Modern homes for modern thinking people. </h2>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Hero details slider -->
		<div class="hero-nav-slider-warp">
			<div class="container">
				<div class="hero-nav-slider owl-carousel">
					<div class="hns-item">
						<h5>3 bedrooms version</h5>
						<p>Availble from March 2019</p>
						<span>$145,000</span>
					</div>
					<div class="hns-item">
						<h5>4 bedrooms version + PRIVATE POOL </h5>
						<p>Availble from March 2019</p>
						<span>$145,000</span>
					</div>
					<div class="hns-item">
						<h5>6 bedrooms version + pRIVATE POOL  </h5>
						<p>Availble from March 2019</p>
						<span>$145,000</span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Hero section end -->

	<!-- features icons section -->
	<div class="features-icons-section">
		<div class="features-icon-item">
			<i class="flaticon-151-banker"></i>
			<p>Easy Buying</p>
		</div>
		<div class="features-icon-item">
			<i class="flaticon-151-closing"></i>
			<p>Ready to Move</p>
		</div>
		<div class="features-icon-item">
			<i class="flaticon-151-maps-and-flags-3"></i>
			<p>Great Location</p>
		</div>
		<div class="features-icon-item">
			<i class="flaticon-151-step-ladder"></i>
			<p>Comunity Pool</p>
		</div>
		<div class="features-icon-item">
				<i class="flaticon-151-trees"></i>
			<p>30% Park</p>
		</div>
		<div class="features-icon-item">
			<i class="flaticon-151-fader"></i>
			<p>Sunny Location</p>
		</div>
		<div class="features-icon-item">
			<i class="flaticon-151-indoor"></i>
			<p>Modern Design</p>
		</div>
		<div class="features-icon-item">
			<i class="flaticon-151-maps-and-flags-1"></i>
			<p>Parking Spaces</p>
		</div>
		<div class="features-icon-item">
			<i class="flaticon-151-transportation"></i>
			<p>Garage Included</p>
		</div>
	</div>
	<!-- features icons section end-->

	<!-- Intro section -->
	<section class="intro-section">
		<div class="container">
			<div class="section-title">
				<h2>our houses</h2>
			</div>
			<div class="row">
				<div class="col-lg-8">
					<div class="intro-img-box">
						<h4>MODERN VILLA 1</h4>
						<img src="img/intro/1.jpg" alt="">
					</div>
				</div>
				<div class="col-lg-4 align-items-end d-flex">
					<div class="intro-text-box">
						<p>Collaboration with a local designer enhanced the owner's vision & added luxurious details and cohesive style. Two outdoor spaces, including private rooftop deck & custom bar area. 2-car garage on lower level with generous storage. If you've been searching for a highly efficient, low maintenance & well designed home - 193 Sheridan Street is at the top of your list!</p>
						<a href="property-single.php" class="site-btn">MORE INFO</a>
					</div>
				</div>
				<div class="col-lg-4 align-items-end d-flex order-2 order-lg-1">
					<div class="intro-text-box">
						<p>A special offering! This modern East End designer condo lives like a single family home. 4 bedroom/ 2 bath custom home built by Redfern Properties & Wright-Ryan. The heart of the home features an open great room with floor-to-ceiling windows. Large custom kitchen w/ Wolf gas range and elegant herringbone tiled backsplash. Living room features gas fireplace, and dining area flows to front deck. Southwesterly exposure with city views bathes the space in natural light.</p>
						<a href="property-single.php" class="site-btn">MORE INFO</a>
					</div>
				</div>
				<div class="col-lg-8 order-1 order-lg-2">
					<div class="intro-img-box">
						<h4>MODERN VILLA 1</h4>
						<img src="img/intro/2.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Intro section end-->
	
	<!-- Design section end-->
	<section class="design-section">
		<div class="container">
			<div class="section-title st-light">
				<h2>About Us</h2>
			</div>
		</div>
		<div class="design-slider owl-carousel">
			<a href="img/design/1.jpg" class="img-popup-gallery">
				<img src="img/design/1.jpg" alt="">
				<i class="flaticon-151-reading-glasses"></i>
			</a>
			<a href="img/design/2.jpg" class="img-popup-gallery">
				<img src="img/design/2.jpg" alt="">
				<i class="flaticon-151-reading-glasses"></i>
			</a>
			<a href="img/design/3.jpg" class="img-popup-gallery">
				<img src="img/design/3.jpg" alt="">
				<i class="flaticon-151-reading-glasses"></i>
			</a>
			<a href="img/design/2.jpg" class="img-popup-gallery">
				<img src="img/design/2.jpg" alt="">
				<i class="flaticon-151-reading-glasses"></i>
			</a>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="design-text text-white">
						<h4>Our Story </h4>
						<p>The Estate Team was founded in 1995, by Margaret Sotillo, to create a different experience for clients in the Real Estate industry. We aren't here to talk you into anything, but we are here to talk you through the process, so you can make the best and most informed decisions concerning your Real Estate Goals. </p>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="design-text text-white">
						<h4>Our Vision</h4>
						<p>Integrity does not bend to the wind of convenience and neither do we. We will always tell you what the facts are, even if it is not what you want to hear, but what are the realities of the situation. We are here to to help you reach your goals in Real Estate, whatever those may be!</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Design section end-->
	
	<!-- Location section-->
	<section class="location-section spad">
		<div class="container">
			<div class="section-title">
				<h2>Our Team</h2>
			</div>
			<div class="row">
            <div class="col-lg-6">
					<div class="team-member">
						<div class="member-pic">
							<img src="img/team/1.jpg" alt="">
						</div>
						<div class="member-info">
							<h4>Margaret Sotillo</h4>
							<h5>General Manager</h5>
							<p>Margaret has been a landlord for 25 years and has been buying and selling real estate in Greater Portland since 1990.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="team-member">
						<div class="member-pic">
							<img src="img/team/3.jpg" alt="">
						</div>
						<div class="member-info">
							<h4>Stiven Spilver</h4>
							<h5>General Manager</h5>
							<p>Stiven specializes in residential, condominium and multi-family properties in the Greater Portland area.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Location section end-->

	<!-- Features section -->
	<section class="features-section">
		<div class="container">
			<div class="section-title">
				<h2>features</h2>
			</div>
		</div>
		<div class="container">
        <div class="features-slider owl-carousel">
			<div class="feature-box" style="min-height: 100px; height: 225px;">
				<i class="flaticon-151-banker"></i>
				<h5>ECO-FRIENDLY HOMES</h5>
			</div>		
			<div class="feature-box" style="min-height: 100px; height: 225px;">
				<i class="flaticon-151-security-system"></i>
				<h5>SECURE AREA</h5>
			</div>		
			<div class="feature-box" style="min-height: 100px; height: 225px;">
				<i class="flaticon-151-maps-and-flags-1"></i>
				<h5>FREE PARKING</h5>
			</div>		
			<div class="feature-box" style="min-height: 100px; height: 225px;">
				<i class="flaticon-151-step-ladder"></i>
				<h5>COMUNITY POOL</h5>
			</div>		
			<div class="feature-box" style="min-height: 100px; height: 225px;">
				<i class="flaticon-151-real-estate-6"></i>
				<h5>BEST DEALS</h5>
			</div>		
		</div>
        </div>
	</section>
	<!-- Features section end-->

	<!-- Subscribe section -->
	<section class="subscribe-section spad">
		<div class="container">
			<div class="subscribe-text text-center">
				<h2>Still Have Some Questions Left?</h2>
				<p>Feel free to contact our team to learn more about the services provided by us and multiple offers for Your business! </p>
			</div>
			<form class="subscribe-form">
                <a href="contact.php" class="site-btn">Contact Us</a>
			</form>
		</div>
	</section>
	<!-- Subscribe section end-->

<?php include 'footer.php' ?>