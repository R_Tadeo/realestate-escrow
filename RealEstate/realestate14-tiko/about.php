<?php include 'header.php' ?>


	<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg/1.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-5">
					<div class="page-top-text text-white">
						<h2>About Us</h2>
						<p>Feel free to learn more about our team and company on this page. We are always happy to help you!</p>
					</div>
				</div>
			</div>
		</div>
		<div class="hero-social-warp">
				<div class="hero-social">
					<a href=""><i class="fa fa-facebook"></i></a>
					<a href=""><i class="fa fa-linkedin"></i></a>
					<a href=""><i class="fa fa-twitter"></i></a>
					<a href=""><i class="fa fa-youtube-play"></i></a>
					<a href=""><i class="fa fa-instagram"></i></a>
				</div>
			</div>
	</section>
	<!-- Page top section end -->

	<!-- About section -->
	<section class="about-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6">
					<div class="about-text">
						<h4>Estate's real difference is simple. Most real estate firms are about transactions. Estate is about people and their individual needs.</h4>
						<p>We are real people, with real goals and dreams, just like our clients. We are more than familiar with all of the questions, concerns and emotions that can go into real estate transactions. We’ll guide you through it with the least amount of stress and negotiate the best deal possible, on your behalf.</p>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="about-slider owl-carousel">
						<img src="img/about/1.jpg" alt="">
						<img src="img/about/1.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- About section end -->
	
	<!-- Milestone section end -->
	<section class="milestone-section">
		<div class="milestone-warp">
			<div class="milestone">
				<h2>2</h2>
				<h4>Residential  Developed</h4>
			</div>
			<div class="milestone">
				<h2>167</h2>
				<h4>Homes Builded</h4>
			</div>
			<div class="milestone">
				<h2>25</h2>
				<h4>Years of Experience</h4>
			</div>
			<div class="milestone">
				<h2>0</h2>
				<h4>Complains</h4>
			</div>
			<div class="milestone">
				<h2>5</h2>
				<h4>Offices</h4>
			</div>
		</div>
	</section>
	<!-- Milestone section end -->


	<!-- Team section -->
	<section class="team-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="team-member">
						<div class="member-pic">
							<img src="img/team/1.jpg" alt="">
						</div>
						<div class="member-info">
							<h4>Margaret Sotillo</h4>
							<h5>General Manager</h5>
							<p>Margaret has been a landlord for 25 years and has been buying and selling real estate in Greater Portland since 1990.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="team-member">
						<div class="member-pic">
							<img src="img/team/3.jpg" alt="">
						</div>
						<div class="member-info">
							<h4>Stiven Spilver</h4>
							<h5>General Manager</h5>
							<p>Stiven specializes in residential, condominium and multi-family properties in the Greater Portland area.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Team section end -->

	<!-- About box text -->
	<div class="about-text-box-warp spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="design-text">
						<h4>Our Story</h4>
						<p>The Estate Team was founded in 1995, by Margaret Sotillo, to create a different experience for clients in the Real Estate industry. We aren't here to talk you into anything, but we are here to talk you through the process, so you can make the best and most informed decisions concerning your Real Estate Goals.</p>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="design-text">
						<h4>Our Vision</h4>
						<p>Integrity does not bend to the wind of convenience and neither do we. We will always tell you what the facts are, even if it is not what you want to hear, but what are the realities of the situation. We are here to to help you reach your goals in Real Estate, whatever those may be!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- About box text end -->

<?php include 'footer.php' ?>