<?php include 'header.php' ?>

<!-- Page top section -->
<section class="page-top-section set-bg" data-setbg="img/page-top-bg/2.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-5">
					<div class="page-top-text text-white">
						<h2>COLD VALLEY ESTATES</h2>
						<p>3465 Elk City Road.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="hero-social-warp">
			<div class="hero-social">
				<a href=""><i class="fa fa-facebook"></i></a>
				<a href=""><i class="fa fa-linkedin"></i></a>
				<a href=""><i class="fa fa-twitter"></i></a>
				<a href=""><i class="fa fa-youtube-play"></i></a>
				<a href=""><i class="fa fa-instagram"></i></a>
			</div>
		</div>
	</section>
	<!-- Page top section end -->

	<!-- Property details section -->
	<section class="property-details-section spad">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-7">
					<div class="property-details">
						<h2>MODERN VILLA 1</h2>
						<div class="property-info">
							<h6>Lot Size</h6>
							<div class="pi-icon">
								<i class="flaticon-151-plans"></i>
								<span>2561 sqft </span>
							</div>
						</div>
						<div class="property-info">
							<h6>Beds</h6>
							<div class="pi-icon">
								<i class="flaticon-151-beds"></i>
								<span>3</span>
							</div>
						</div>
						<div class="property-info">
							<h6>Baths</h6>
							<div class="pi-icon">
								<i class="flaticon-151-relax"></i>
								<span>2</span>
							</div>
						</div>
						<div class="property-info">
							<h6>Garage</h6>
							<div class="pi-icon">
								<i class="flaticon-151-transportation"></i>
								<span>1 </span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-5 text-lg-right text-left">
					<div class="property-price">
						<h2>$445,000</h2>
						<p>(taxes excluded)</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Property details section end -->

	<!-- Property features slider -->
	<div class="property-features-slider owl-carousel">
		<a href="img/property-gallery/1.jpg" class="img-popup-gallery">
			<img src="img/property-gallery/1.jpg" alt="">
			<i class="flaticon-151-reading-glasses"></i>
		</a>
		<a href="img/property-gallery/2.jpg" class="img-popup-gallery">
			<img src="img/property-gallery/2.jpg" alt="">
			<i class="flaticon-151-reading-glasses"></i>
		</a>
		<a href="img/property-gallery/3.jpg" class="img-popup-gallery">
			<img src="img/property-gallery/3.jpg" alt="">
			<i class="flaticon-151-reading-glasses"></i>
		</a>
		<a href="img/property-gallery/2.jpg" class="img-popup-gallery">
			<img src="img/property-gallery/2.jpg" alt="">
			<i class="flaticon-151-reading-glasses"></i>
		</a>
	</div>
	<!-- Property features slider end -->

	<!-- Property overview section-->
	<section class="property-overview-section spad pb-0">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="property-overview-text">
						<h4>GENERAL OVERVIEW</h4>
						<p>A special offering! This modern East End designer condo lives like a single family home. 4 bedroom/ 2 bath custom home built by Redfern Properties & Wright-Ryan. The heart of the home features an open great room with floor-to-ceiling windows. Large custom kitchen w/ Wolf gas range and elegant herringbone tiled backsplash. Living room features gas fireplace, and dining area flows to front deck. Southwesterly exposure with city views bathes the space in natural light.</p>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="property-overview-text">
						<h4>GENERAL OVERVIEW</h4>
						<ul>
							<li>Lot Area: 1,250 SQ FT.</li>
							<li>Bed Rooms: 4.</li>
							<li>Bath Rooms: 4.</li>
							<li>Luggage</li>
							<li>Garage: 2.</li>
							<li>Year Build:: 2019.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid pt-5">
			<div class="row">
				<div class="col-lg-6 p-0">
					<img src="img/property-features/1.jpg" alt="">
				</div>
				<div class="col-lg-6 p-0 d-flex align-items-center justify-content-center">
					<div class="property-text-warp">
						<div class="property-overview-text">
							<h4>GENERAL OVERVIEW</h4>
							<p>Collaboration with a local designer enhanced the owner's vision & added luxurious details and cohesive style. Two outdoor spaces, including private rooftop deck & custom bar area. 2-car garage on lower level with generous storage. If you've been searching for a highly efficient, low maintenance & well designed home - 193 Sheridan Street is at the top of your list!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Property overview section end-->

	<!-- Call to action section -->
	<section class="call-to-action-section set-bg" data-setbg="img/cta-bg.jpg">
		<div class="container text-white text-center">
			<h2>Ask our top consultants for an personalized offer today. </h2>
                <a href="contact.php" class="site-btn">Contact Us</a>
		</div>
	</section>
	<!-- Call to action section end-->

<?php include 'footer.php' ?>