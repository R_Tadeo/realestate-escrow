<?php include 'header.php' ?>

<main>
        <!--? Hero Start -->
        <div class="slider-area2">
            <div class="slider-height2  d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap hero-cap2 text-center">
                                <h2>Services</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--? Categories Area Start -->
        <div class="categories-area section-padding30" style="padding-top: 70px; padding-bottom: 50px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <!-- Section Tittle -->
                        <div class="section-tittle mb-70">
                            <span>Our Services area</span>
                            <h2>Our services can save you time, money, and the aggravation of managing installment payments on your real estate investment.</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cat mb-50">
                            <div class="cat-icon">
                            <span class="box-default-icon novi-icon icon-lg mercury-icon-house"></span>
                            </div>
                            <div class="cat-cap">
                                <h5><a>Rentals</a></h5>
                                <p>Our management service solves all tenant concerns and channels them effectively. Month by month, balances, receipts and account statements are generated that inform both Owners and Tenants of the situation of their rent.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cat mb-50">
                            <div class="cat-icon">
                                <span class="box-default-icon novi-icon icon-lg mercury-icon-partners"></span>
                            </div>
                            <div class="cat-cap">
                                <h5><a>Sales</a></h5>
                                <p>We make sure that the buyer of your property is determined to buy and has the resources to carry out the sale. The delivery of the sale opinion is one of the most important points of our service because at that moment is when we advise you in detail after having determined everything in relation to your sale.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cat mb-50">
                            <div class="cat-icon">
                                <span class="flaticon-life"></span>
                            </div>
                            <div class="cat-cap">
                                <h5><a>Advisory </a></h5>
                                <p>With the best data analysis technology and a team of experts, Escrow conducts candidate research so that you have the peace of mind of having the best client.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="services-area">
        <div class="container">
            <div class="row align-items-end mb-50">
                <div class="cl-xl-6 col-lg-6 col-md-10">
                    <!-- Section Tittle -->
                    <div class="section-tittle ">
                        <span>Why Choose Us</span>
                        <h2>Advantages for Buyers and Sellers</h2>
                    </div> 
                </div>
            </div>
            <!-- Nav Card -->
            <div class="tab-content" id="nav-tabContent">
                <!-- card one -->
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-10">
                            <div class="single-services mb-100">
                                <div class="services-img">
                                    <img src="assets/img/gallery/services2.png" alt="">
                                    </div>
                                    <div class="services-caption">
                                    <span>FAIR AND TRANSPARENT</span>
                                    <p><a>The step-by-step process ensures fair and transparent handling. Deliberate fraud is excluded as no merchandise is delivered or money is paid without proper consideration.</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-10">
                            <div class="single-services mb-100">
                                <div class="services-img">
                                    <img src="assets/img/gallery/services3.png" alt="">
                                    </div>
                                    <div class="services-caption">
                                    <span>SAFE AND SECURE AND WITHOUT THE RISK OF PREPAYMENT</span>
                                    <p><a>The buyer can be convinced of the quality before the seller receives the money. The seller does not pay until the buyer has irrevocably deposited the money into the escrow account.</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Nav Card -->
        </div>
    </div>


    </main>

<?php include 'footer.php' ?>