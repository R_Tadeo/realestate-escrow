<?php include 'header.php' ?>

<main>
        <!--? Hero Start -->
        <div class="slider-area2">
            <div class="slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap hero-cap2 text-center">
                                <h2>About</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!--? About Area start -->
        <section class="about-details-area about1 section-padding30">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-10">
                    <div class="about-caption2 mb-50">
                        <h3>How we deal Our Cases</h3>
                        <div class="send-cv">
                            <a href="#">contact@gmail.com</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-6 col-sm-10">
                    <div class="about-caption mb-50">
                        <h3>You can’t use up creativity. The more you use, the
                            more you have in your signifant mind.</h3>
                        <div class="experience">
                            <div class="year">
                                <span>22</span>
                            </div>
                            <div class="year-details"><p>YEARS OF<br> DIGITAL EXPERIENCE</p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
         <!--? About Area Start-->
        <div class="about-area fix">
            <!--Right Contents  -->
            <div class="about-img"></div>
            <!-- left Contents -->
            <div class="about-details">
                <div class="right-caption">
                    <!-- Section Tittle -->
                    <div class="section-tittle section-tittle2 mb-50">
                        <span>About Our Escrow agency</span>
                        <h2>We are commited for<br> better service</h2>
                    </div>
                    <div class="about-more">
                        <p class="pera-top">Our mission is to provide accounting and tax services of a superior quality. The responsive, personal attention we provide for each client, regardless how large or small, is the key to our approach</p>
                        <p class="mb-65 pera-bottom">Providing high-quality accounting & tax services. Sharing professional experience in tax management. Offering full support to all our clients.</p>
                    </div>
                </div>
            </div>

        </div>
        <!-- About Area End-->
        <!-- Team Start -->
        <div class="team-area section-padding30">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 col-md-10">
                        <!-- Section Tittle -->
                        <div class="section-tittle mb-70">
                            <span>Our advisors </span>
                            <h2>Meet Our Dedicated Team Members.</h2>
                        </div> 
                    </div>
                </div>
                <div class="row">
                    <!-- single Tem -->
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="assets/img/gallery/team2.png" alt="">
                            </div>
                            <div class="team-caption">
                                <h3><a href="#">Ethan Welch</a></h3>
                                <span>Chir advisors</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="assets/img/gallery/team3.png" alt="">
                            </div>
                            <div class="team-caption">
                                <h3><a href="#">Trevor Stanley</a></h3>
                                <span>Junior advisors</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="assets/img/gallery/team1.png" alt="">
                            </div>
                            <div class="team-caption">
                                <h3><a href="#">Allen Guzman</a></h3>
                                <span>Senior Lawadvisorsyer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Team End -->
        <!--? Testimonial Start -->
        <div class="testimonial-area testimonial-padding" data-background="assets/img/gallery/section_bg04.png">
            <div class="container ">
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-10 col-lg-10 col-md-9">
                        <div class="h1-testimonial-active">
                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                <!-- Testimonial Content -->
                                <div class="testimonial-caption ">
                                    <div class="testimonial-top-cap">
                                        <svg 
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="67px" height="49px">
                                       <path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
                                        d="M57.053,48.208 L42.790,48.208 L52.299,29.240 L38.036,29.240 L38.036,0.790 L66.562,0.790 L66.562,29.240 L57.053,48.208 ZM4.755,48.208 L14.263,29.240 L0.000,29.240 L0.000,0.790 L28.527,0.790 L28.527,29.240 L19.018,48.208 L4.755,48.208 Z"/>
                                       </svg>
                                        <p>Thank you everyone. Escrow Options has earned my business forever. Thank you so much, Faith and Karen, for your stellar service!</p>
                                    </div>
                                    <!-- founder -->
                                    <div class="testimonial-founder d-flex align-items-center justify-content-center">
                                        <div class="founder-img">
                                            <img src="assets/img/comment/comment_3.png" alt="">
                                        </div>
                                        <div class="founder-text">
                                            <span>Oliver jems</span>
                                            <p>Client</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                <!-- Testimonial Content -->
                                <div class="testimonial-caption ">
                                    <div class="testimonial-top-cap">
                                        <svg 
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="67px" height="49px">
                                       <path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
                                        d="M57.053,48.208 L42.790,48.208 L52.299,29.240 L38.036,29.240 L38.036,0.790 L66.562,0.790 L66.562,29.240 L57.053,48.208 ZM4.755,48.208 L14.263,29.240 L0.000,29.240 L0.000,0.790 L28.527,0.790 L28.527,29.240 L19.018,48.208 L4.755,48.208 Z"/>
                                       </svg>
                                        <p>I want to thank you for doing an awesome job once again with our escrow. It's so comforting to know my clients will be well taken care of through the diligence and professionalism you put forth.</p>
                                    </div>
                                    <!-- founder -->
                                    <div class="testimonial-founder d-flex align-items-center justify-content-center">
                                        <div class="founder-img">
                                            <img src="assets/img/gallery/Homepage_testi.png" alt="">
                                        </div>
                                        <div class="founder-text">
                                            <span>Antonio Banderas</span>
                                            <p>Client</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonial End -->
       
        
    </main>

<?php include 'footer.php' ?>