<?php include 'header.php' ?>

<main>
    <!-- slider Area Start-->
    <div class="slider-area ">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-7 col-lg-8 col-md-9 col-sm-10">
                            <div class="hero__caption">
                                <span data-animation="fadeInLeft" data-delay=".1s">Committed to success</span>
                                <h1 data-animation="fadeInLeft" data-delay=".5s" >The Best Solutions. Fresh Ideas for Your Business</h1>
                                <p data-animation="fadeInLeft" data-delay=".9s">Providing every client with the attention they deserve.</p>
                                <!-- Hero-btn -->
                                <div class="hero__btn" data-animation="fadeInLeft" data-delay="1.1s">
                                    <a href="about.php" class="btn hero-btn">Learn About Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </div>
    <!-- slider Area End-->
   <!--? Categories Area Start -->
   <div class="categories-area section-padding30" style="padding-top: 70px; padding-bottom: 50px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <!-- Section Tittle -->
                        <div class="section-tittle mb-70">
                            <span>Our Services area</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cat mb-50">
                            <div class="cat-icon">
                            <span class="box-default-icon novi-icon icon-lg mercury-icon-house"></span>
                            </div>
                            <div class="cat-cap">
                                <h5><a href="services.php">Rentals</a></h5>
                                <p>Our management service solves all tenant concerns and channels them effectively.</p>
                                <a href="services.php" class="read-more1">Read More ></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cat mb-50">
                            <div class="cat-icon">
                            <span class="box-default-icon novi-icon icon-lg mercury-icon-partners"></span>
                            </div>
                            <div class="cat-cap">
                                <h5><a href="services.php">Sales</a></h5>
                                <p>We make sure that the buyer of your property is determined to buy and has the resources to carry out the sale.</p>
                                <a href="services.php" class="read-more1">Read More ></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-cat mb-50">
                            <div class="cat-icon">
                                <span class="flaticon-life"></span>
                            </div>
                            <div class="cat-cap">
                                <h5><a href="services.php">Advisory </a></h5>
                                <p>With the best data analysis technology and a team of experts.</p>
                                <a href="services.php" class="read-more1">Read More ></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

  <!--? About Area Start-->
  <div class="about-area fix">
            <!--Right Contents  -->
            <div class="about-img"></div>
            <!-- left Contents -->
            <div class="about-details">
                <div class="right-caption">
                    <!-- Section Tittle -->
                    <div class="section-tittle section-tittle2 mb-50">
                        <span>About Our Escrow agency</span>
                        <h2>We are commited for<br> better service</h2>
                    </div>
                    <div class="about-more">
                        <p class="pera-top">Our mission is to provide accounting and tax services of a superior quality. The responsive, personal attention we provide for each client, regardless how large or small, is the key to our approach</p>
                        <p class="mb-65 pera-bottom">Providing high-quality accounting & tax services. Sharing professional experience in tax management. Offering full support to all our clients.</p>
                    </div>
                </div>
            </div>

        </div>
        <!-- About Area End-->
    <!-- Contact form Start -->
    <div class="contact-form bg-height pb-top" data-background="assets/img/gallery/section_bg05.png">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 col-lg-8">
                    <div class="form-wrapper">
                    <!--Section Tittle  -->
                        <div class="row ">
                            <div class="col-xl-12">
                                <div class="section-tittle section-tittle2 mb-70">
                                    <span>Fill up to get a quote</span>
                                    <h2>World's Leading Escrow Agency !</h2>
                                </div>
                            </div>
                        </div>
                        <!--End Section Tittle  -->
                        <form id="contact-form" action="#" method="POST">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-box user-icon mb-30">
                                    <input type="text" name="name" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-box email-icon mb-30">
                                    <input type="text" name="email" placeholder="Phone">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 mb-30">
                                <div class="select-itms">
                                    <select name="select" id="select2">
                                        <option value="">Rentals</option>
                                        <option value="">Sales</option>
                                        <option value="">Advisory</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-box subject-icon mb-30">
                                    <input type="Email" name="subject" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-box message-icon mb-30">
                                    <textarea name="message" id="message" placeholder="Message"></textarea>
                                </div>
                            <div class="submit-info">
                                    <button class="submit-btn2" type="submit">Submit Now</button>
                            </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Shape -->
        <!-- <div class="from-shape d-none d-xl-block">
            <img src="assets/img/gallery/from_Shape.png" alt="">
        </div> -->
    </div>
    <!-- Contact form End -->
    
    <div class="services-area section-padding3">
        <div class="container">
            <div class="row align-items-end mb-50">
                <div class="cl-xl-6 col-lg-6 col-md-10">
                    <!-- Section Tittle -->
                    <div class="section-tittle ">
                        <span>Why Choose Us</span>
                        <h2>Advantages for Buyers and Sellers</h2>
                    </div> 
                </div>
            </div>
            <!-- Nav Card -->
            <div class="tab-content" id="nav-tabContent">
                <!-- card one -->
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-10">
                            <div class="single-services mb-100">
                                <div class="services-img">
                                    <img src="assets/img/gallery/services2.png" alt="">
                                    </div>
                                    <div class="services-caption">
                                    <span>FAIR AND TRANSPARENT</span>
                                    </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-10">
                            <div class="single-services mb-100">
                                <div class="services-img">
                                    <img src="assets/img/gallery/services3.png" alt="">
                                    </div>
                                    <div class="services-caption">
                                    <span>SAFE AND SECURE AND WITHOUT THE RISK OF PREPAYMENT</span>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Nav Card -->
        </div>
    </div>


    <!-- Want To work -->
    <section class="wantToWork-area w-padding2 section-bg" data-background="assets/img/gallery/section_bg03.png">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-xl-7 col-lg-9 col-md-8">
                    <div class="wantToWork-caption wantToWork-caption2">
                        <h2>Are you Searching For a First-Class Consultant?</h2>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-3 col-md-4">
                    <a href="contact.php" class="btn btn-black f-right">Contact Naw </a>
                </div>
            </div>
        </div>
    </section>
    <!-- Want To work End -->
        <!-- Team Start -->
        <div class="team-area section-padding30">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 col-md-10">
                        <!-- Section Tittle -->
                        <div class="section-tittle mb-70">
                            <span>Our advisors </span>
                            <h2>Meet Our Dedicated Team Members.</h2>
                        </div> 
                    </div>
                </div>
                <div class="row">
                    <!-- single Tem -->
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="assets/img/gallery/team2.png" alt="">
                            </div>
                            <div class="team-caption">
                                <h3><a href="#">Ethan Welch</a></h3>
                                <span>Chir advisors</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="assets/img/gallery/team3.png" alt="">
                            </div>
                            <div class="team-caption">
                                <h3><a href="#">Trevor Stanley</a></h3>
                                <span>Junior advisors</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="assets/img/gallery/team1.png" alt="">
                            </div>
                            <div class="team-caption">
                                <h3><a href="#">Allen Guzman</a></h3>
                                <span>Senior Lawadvisorsyer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Team End -->
            <!--? Testimonial Start -->
            <div class="testimonial-area testimonial-padding" data-background="assets/img/gallery/section_bg04.png">
            <div class="container ">
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-10 col-lg-10 col-md-9">
                        <div class="h1-testimonial-active">
                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                <!-- Testimonial Content -->
                                <div class="testimonial-caption ">
                                    <div class="testimonial-top-cap">
                                        <svg 
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="67px" height="49px">
                                       <path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
                                        d="M57.053,48.208 L42.790,48.208 L52.299,29.240 L38.036,29.240 L38.036,0.790 L66.562,0.790 L66.562,29.240 L57.053,48.208 ZM4.755,48.208 L14.263,29.240 L0.000,29.240 L0.000,0.790 L28.527,0.790 L28.527,29.240 L19.018,48.208 L4.755,48.208 Z"/>
                                       </svg>
                                        <p>Thank you everyone. Escrow Options has earned my business forever. Thank you so much, Faith and Karen, for your stellar service!</p>
                                    </div>
                                    <!-- founder -->
                                    <div class="testimonial-founder d-flex align-items-center justify-content-center">
                                        <div class="founder-img">
                                            <img src="assets/img/comment/comment_3.png" alt="">
                                        </div>
                                        <div class="founder-text">
                                            <span>Oliver jems</span>
                                            <p>Client</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                <!-- Testimonial Content -->
                                <div class="testimonial-caption ">
                                    <div class="testimonial-top-cap">
                                        <svg 
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="67px" height="49px">
                                       <path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
                                        d="M57.053,48.208 L42.790,48.208 L52.299,29.240 L38.036,29.240 L38.036,0.790 L66.562,0.790 L66.562,29.240 L57.053,48.208 ZM4.755,48.208 L14.263,29.240 L0.000,29.240 L0.000,0.790 L28.527,0.790 L28.527,29.240 L19.018,48.208 L4.755,48.208 Z"/>
                                       </svg>
                                        <p>I want to thank you for doing an awesome job once again with our escrow. It's so comforting to know my clients will be well taken care of through the diligence and professionalism you put forth.</p>
                                    </div>
                                    <!-- founder -->
                                    <div class="testimonial-founder d-flex align-items-center justify-content-center">
                                        <div class="founder-img">
                                            <img src="assets/img/gallery/Homepage_testi.png" alt="">
                                        </div>
                                        <div class="founder-text">
                                            <span>Antonio Banderas</span>
                                            <p>Client</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonial End -->
   
</main>

<?php include 'footer.php' ?>