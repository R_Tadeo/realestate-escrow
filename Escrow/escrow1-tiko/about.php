<?php include 'header.php' ?>


<section class="section section-intro context-dark">
        <div class="intro-bg" style="background: url(img/about2.jpg) no-repeat;background-size:cover;background-position: top center;"></div>
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-xl-8 text-center">
              <h1 class="font-weight-bold wow fadeInLeft">About Us</h1>
              <p class="intro-description wow fadeInRight">Feel free to learn more about our team and company on this page. We are always happy to help you!</p>
            </div>
          </div>
        </div>
      </section>
<!-- About page about section-->
<section class="section section-md">
        <div class="container">
          <div class="row row-40 justify-content-center">
            <div class="col-lg-6 col-12">
              <div class="offset-top-45 offset-lg-right-45">
                <div class="section-name wow fadeInRight" data-wow-delay=".2s">About us</div>
                <h3 class="wow fadeInLeft text-capitalize" data-wow-delay=".3s">A Few Words<span class="text-primary"> about us</span></h3>
                <p class="font-weight-bold text-gray-dark wow fadeInUp" data-wow-delay=".4s">To be the world leaders in real estate, reaching our goals by helping others achieve theirs. Everyone wins.</p>
                <p class="font-weight-bold text-gray-dark wow fadeInUp" data-wow-delay=".4s">Escrow is:</p>
                <p class="wow fadeInUp" data-wow-delay=".4s">
                - Regularly audited by government authorities. <br>
                - Armed with an in-depth knowledge of the escrow process. <br>
                - The safest service to trust with your money. <br>
                - A licensed and regulated escrow company compliant with Escrow Law. <br>
                - Online escrow that is simple and safe for Buyers and Sellers.
                </p>
                <div class="offset-top-20">
                  <!--Linear progress bar-->
                  <article class="progress-linear">
                    <div class="progress-header progress-header-simple">
                      <p>Management</p><span class="progress-value">98</span>
                    </div>
                    <div class="progress-bar-linear-wrap">
                      <div class="progress-bar-linear"></div>
                    </div>
                  </article>
                  <!--Linear progress bar-->
                  <article class="progress-linear">
                    <div class="progress-header progress-header-simple">
                      <p>Marketing</p><span class="progress-value">99</span>
                    </div>
                    <div class="progress-bar-linear-wrap">
                      <div class="progress-bar-linear"></div>
                    </div>
                  </article>
                  <!--Linear progress bar-->
                  <article class="progress-linear">
                    <div class="progress-header progress-header-simple">
                      <p>Analysis</p><span class="progress-value">97</span>
                    </div>
                    <div class="progress-bar-linear-wrap">
                      <div class="progress-bar-linear"></div>
                    </div>
                  </article>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-sm-10 col-12">
              <div class="block-decorate-img wow fadeInLeft" data-wow-delay=".2s"><img src="img/about1.jpg" alt="" width="570" height="351"/>
              </div>
            </div>
          </div>
        </div>
      </section>

<!-- Our Mission-->
<section class="section bg-default section-md">
        <div class="container">
          <div class="row row-30">
            <div class="col-md-6 wow fadeInLeft">
              <h2 class="title-icon"><span class="icon-lg novi-icon offset-right-10 mercury-icon-target-2"></span><span>Our <span class="text-light">Mission</span></span></h2>
              <p class="big">Our mission is to provide accounting and tax services of a superior quality. The responsive, personal attention we provide for each client, regardless how large or small, is the key to our approach.</p>
              <ul class="list-marked-2 wow fadeInLeft">
                <li>Providing high-quality accounting & tax services</li>
                <li>Sharing professional experience in tax management</li>
                <li>Offering full support to all our clients</li>
              </ul>
            </div>
            <div class="col-md-6">
              <div class="row wow fadeInRight">
                <div class="col-6">
                  <!--Box counter-->
                  <div class="box-counter">
                    <div class="box-counter-main">
                      <div class="counter">750</div>
                    </div>
                    <p class="box-counter-title">Satisfied Clients</p>
                  </div>
                </div>
                <div class="col-6">
                  <!--Box counter-->
                  <div class="box-counter">
                    <div class="box-counter-main">
                      <div class="counter">68</div>
                    </div>
                    <p class="box-counter-title">Team Members</p>
                  </div>
                </div>
                <div class="col-6">
                  <!--Box counter-->
                  <div class="box-counter">
                    <div class="box-counter-main">
                      <div class="counter">243</div>
                    </div>
                    <p class="box-counter-title">Successful Cases</p>
                  </div>
                </div>
                <div class="col-6">
                  <!--Box counter-->
                  <div class="box-counter">
                    <div class="box-counter-main">
                      <div class="counter">99</div><span>%</span>
                    </div>
                    <p class="box-counter-title">Satisfaction Rate</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Our team-->
      <section class="section section-md">
        <div class="container">
          <div class="row row-50 justify-content-center">
            <div class="col-md col-12 text-center">
              <div class="section-name wow fadeInRight" data-wow-delay=".2s">People Behind Our Success</div>
              <h3 class="wow fadeInLeft text-capitalize" data-wow-delay=".3s">Meet Our<span class="text-primary"> Team</span></h3>
            </div>
          </div>
          <div class="row row-50 justify-content-center">
            <div class="col-xl-4 col-sm-6 col-10 wow fadeInLeft" data-wow-delay=".3s">
              <div class="team-classic-wrap">
                <div class="team-classic-img"><img src="img/team2.jpg" alt="" width="370" height="198"/>
                </div>
                <div class="block-320 text-center">
                  <h4 class="font-weight-bold">Jane McMillan</h4><span class="d-block">Business Advisor</span>
                  <hr class="offset-top-40"/>
                  <ul class="justify-content-center social-links offset-top-30">
                    <li><a class="fa fa-linkedin" href="#"></a></li>
                    <li><a class="fa fa fa-twitter" href="#"></a></li>
                    <li><a class="fa fa-facebook" href="#"></a></li>
                    <li><a class="fa fa-instagram" href="#"></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-sm-6 col-10 wow fadeInRight" data-wow-delay=".3s">
              <div class="team-classic-wrap">
                <div class="team-classic-img"><img src="img/team3.jpg" alt="" width="370" height="198"/>
                </div>
                <div class="block-320 text-center">
                  <h4 class="font-weight-bold">Robert Smith</h4><span class="d-block">Marketing Manager</span>
                  <hr class="offset-top-40"/>
                  <ul class="justify-content-center social-links offset-top-30">
                    <li><a class="fa fa-linkedin" href="#"></a></li>
                    <li><a class="fa fa fa-twitter" href="#"></a></li>
                    <li><a class="fa fa-facebook" href="#"></a></li>
                    <li><a class="fa fa-instagram" href="#"></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--Brands-->
      <section class="section section-md bg-gray-lighten">
        <div class="container">
          <div class="row">
            <!-- Owl Carousel-->
            <div class="owl-carousel text-center owl-brand" data-items="1" data-sm-items="2" data-md-items="3" data-lg-items="3" data-xl-items="5" data-xxl-items="5" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false" data-autoplay="true">
              <div class="item"><img src="img/business1.png" alt="" width="200" height="24"/>
              </div>
              <div class="item"><img src="img/business2.png" alt="" width="200" height="24"/>
              </div>
              <div class="item"><img src="img/business3.png" alt="" width="200" height="24"/>
              </div>
              <div class="item"><img src="img/business4.png" alt="" width="200" height="24"/>
              </div>
              <div class="item"><img src="img/business5.png" alt="" width="200" height="24"/>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--Cta section-->
      <section class="section section-md" style="background:url(img/contact1.jpg) no-repeat; background-size: cover;">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-10 col-12 text-center"><span class="text-white d-block cta-big-text font-weight-medium">Still Have Some Questions Left?</span>
              <h2 class="text-white"><span class="d-block">Get in Touch with Us:<a class="underline-link" href="tel:#"> 855-456-7634</a></span></h2>
              <p class="text-white">Feel free to contact our team to learn more about the services provided by us and multiple offers for Your business!</p><a class="button-circle button-default-outline button button-lg button-width-210" href="contacts.php">contact us</a>
            </div>
          </div>
        </div>
      </section>

<?php include 'footer.php' ?>