<?php include 'header.php' ?>
<section class="section section-intro context-dark">
        <div class="intro-bg" style="background: url(img/service1.jpg) no-repeat;background-size:cover;background-position: top center;"></div>
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-xl-8 text-center">
              <h1 class="font-weight-bold wow fadeInLeft">Services</h1>
              <p class="intro-description wow fadeInRight">Our services can save you time, money, and the aggravation of managing installment payments on your real estate investment.</p>
            </div>
          </div>
        </div>
      </section>

      <div class="container py-5 my-5">
  <section class="p-md-3 mx-md-5">
    <div class="row mb-5 d-flex align-items-center">
      <div class="col-lg-6 col-md-6">
        <div class="card">
          <div class="card-header white">
            <h4 class="font-weight-bold mb-0 wow fadeInLeft">Rentals</h4>
          </div>
          <div class="card-body">
            <p class="card-text wow fadeInLeft">
            Our management service solves all tenant concerns and channels them effectively. <br>
            Month by month, balances, receipts and account statements are generated that inform both Owners and Tenants of the situation of their rent.
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4">
        <div class="view overlay rounded z-depth-1">
          <img src="img/service11.jpg" class="img-fluid wow fadeInRight"
            alt="Sample project image" />
          <a href="#">
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
      </div>
    </div>
    <div class="row mb-5 d-flex align-items-center justify-content-end">
      <div class="col-md-6 col-lg-4">
        <div class="view overlay rounded z-depth-1">
          <img src="img/service2.jpg" class="img-fluid"
            alt="Sample project image" />
          <a href="#">
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
      </div>
      <div class="col-lg-6 col-md-6">
        <div class="card">
          <div class="card-header white">
            <h4 class="font-weight-bold mb-0 wow fadeInRight">Sales</h4>
          </div>
          <div class="card-body">
            <p class="card-text wow fadeInRight">
            We make sure that the buyer of your property is determined to buy and has the resources to carry out the sale. <br>
            The delivery of the sale opinion is one of the most important points of our service because at that moment is when we advise you in detail after having determined everything in relation to your sale (valuation, sale process, tax opinion).
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="row d-flex align-items-center">
      <div class="col-lg-6 col-md-6">
        <div class="card">
          <div class="card-header white">
            <h4 class="font-weight-bold mb-0 wow fadeInLeft">Advisory</h4>
          </div>
          <div class="card-body">
            <p class="card-text wow fadeInLeft">
            With the best data analysis technology and a team of experts, 
            Escrow conducts candidate research so that you have the peace 
            of mind of having the best client.
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4">
        <div class="view overlay rounded z-depth-1">
          <img src="img/service3.jpg" class="img-fluid wow fadeInRight"
            alt="Sample project image" />
          <a href="#">
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="container my-5 p-5 z-depth-1">


  <!--Section: Content-->
  <section class="text-center dark-grey-text">

    <!-- Section heading -->
    <h4 class="font-weight-bold mb-1 pb-2 wow fadeInLeft">ADVANTAGES FOR</h4>
    <!-- Section description -->
    <h3 class="font-weight-bold mb-4 pb-2 wow fadeInRight">BUYERS AND SELLERS</h3>

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-4 mb-4">

        <span class="icon-lg novi-icon offset-right-10 mercury-icon-lock wow fadeInLeft"></span>
        <h5 class="font-weight-bold my-4 wow fadeInLeft">SAFE AND SECURE AND WITHOUT THE RISK OF PREPAYMENT</h5>
        <p class="grey-text mb-md-0 wow fadeInLeft">
        The buyer can be convinced of the quality before the seller receives the money. The seller does not pay until the buyer has irrevocably deposited the money into the escrow account.
        </p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 mb-4">

        <span class="icon-lg novi-icon offset-right-10 mercury-icon-scales wow fadeInUp"></span>
        <h5 class="font-weight-bold my-4 wow fadeInUp">FAIR AND TRANSPARENT</h5>
        <p class="grey-text mb-md-0 wow fadeInUp">The step-by-step process ensures fair and transparent handling. Deliberate fraud is excluded as no merchandise is delivered or money is paid without proper consideration.
A payment or refund of money is only made by agreement between both parties.
        </p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 mb-4">

        <span class="icon-lg novi-icon offset-right-10 mercury-icon-globe wow fadeInRight"></span>
        <h5 class="font-weight-bold my-4 wow fadeInRight">INTERNATIONAL</h5>
        <p class="grey-text mb-0 wow fadeInRight">Carefree cooperation across country borders - Escrow supports deposits and withdrawals in 119 countries and EUR, USD, GBP, CHF, AUD and CAD currencies.
        </p>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </section>
  <!--Section: Content-->


</div>

<section class="section section-md" style="background:url(img/contact1.jpg) no-repeat; background-size: cover;">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-10 col-12 text-center"><span class="text-white d-block cta-big-text font-weight-medium">Still Have Some Questions Left?</span>
              <h2 class="text-white"><span class="d-block">Get in Touch with Us:<a class="underline-link" href="tel:#"> 855-456-7634</a></span></h2>
              <p class="text-white">Feel free to contact our team to learn more about the services provided by us and multiple offers for Your business!</p><a class="button-circle button-default-outline button button-lg button-width-210" href="contacts.php">contact us</a>
            </div>
          </div>
        </div>
      </section>

<?php include 'footer.php' ?>