<!--Footer-->
<footer class="section footer-classic section-sm">
        <div class="container">
          <div class="row row-30">
            <div class="col-lg-5 wow fadeInUp">
              <!--Brand--><a class="brand" href="index.php"><span class="icon-lg novi-icon offset-right-10 mercury-icon-lib text-white ">Escrow</spam></a><br><br>
              <p class="footer-classic-description offset-top-0 offset-right-25">Real estate office dedicated to the commercialization of Real Estate. Experts in sale, purchase and rental of properties. Comprehensive real estate that provides a wide range of services such as: mortgage loans, investment plans, legal and financial advice</p>
            </div>
            <div class="col-lg-4 col-sm-8 wow fadeInUp">
              <P class="footer-classic-title">contact info</P>
              <div class="d-block offset-top-0">2164 Wall St, New York<span class="d-lg-block">NY 10122, USA</span></div><a class="d-inline-block accent-link" href="mailto:#">info@demolink.org</a><a class="d-inline-block" href="tel:#">+1 800 123 45 67</a>
              <ul>
                <li>Mon-Thu:<span class="d-inline-block offset-left-10 text-white">9:30 AM - 9:00 PM</span></li>
                <li>Fri:<span class="d-inline-block offset-left-10 text-white">10:00 AM - 9:00 PM</span></li>
                <li>Sat:<span class="d-inline-block offset-left-10 text-white">10:00 AM - 3:00 PM</span></li>
              </ul>
            </div>
            <div class="col-lg-3 col-sm-4 wow fadeInUp" data-wow-delay=".3s">
              <P class="footer-classic-title">Quick Links</P>
              <ul class="footer-classic-nav-list">
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About us</a></li>
                <li><a href="services.php">Services</a></li>
                <li><a href="contacts.php">Contacts</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container wow fadeInUp" data-wow-delay=".4s">
          <div class="footer-classic-aside">
            <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span> Escrow. All Rights Reserved.</p>
            <ul class="social-links">
              <li><a class="fa fa-linkedin" href="#"></a></li>
              <li><a class="fa fa fa-twitter" href="#"></a></li>
              <li><a class="fa fa-facebook" href="#"></a></li>
              <li><a class="fa fa-instagram" href="#"></a></li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <!--coded by Drel-->
  </body>
</html>