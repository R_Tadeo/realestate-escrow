<?php include 'header.php' ?>


<section class="section section-intro context-dark ">
        <div class="intro-bg" style="background: url(img/contact1.jpg) no-repeat;background-size:cover;background-position: top center;"></div>
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-xl-8 text-center">
              <h1 class="font-weight-bold wow fadeInLeft">Contacts</h1>
              <p class="intro-description wow fadeInRight">Ready to look forward to Quality Contracts, Quality Servicing, and Fewer Defaults? Contact Escrow now and put a professional team to work for you!</p>
            </div>
          </div>
        </div>
      </section>
<!--Mailform-->
<div class="container my-5">

  <!--Section: Content-->
  <section class="text-center dark-grey-text mb-5">

    <div class="card">
      <div class="card-body rounded-top border-top p-5">
        <form class="mb-4 mx-md-5" action="">

          <div class="row">
            <div class="col-md-6 mb-4">

              <!-- Name -->
              <input type="text" id="name" class="form-control" placeholder="Name">

            </div>
            <div class="col-md-6 mb-4">

              <!-- Email -->
              <input type="email" id="email" class="form-control" placeholder="Email">

            </div>
          </div>

          <div class="row">
            <div class="col-md-12 mb-4">

              <!-- Subject -->
              <input type="text" id="subject" class="form-control" placeholder="Subject">

            </div>
          </div>

          <div class="row">
            <div class="col-md-12">

              <div class="form-group mb-4">
                <textarea class="form-control rounded" id="message" rows="3" placeholder="How can we help?"></textarea>
              </div>

              <div class="text-center">
                <button type="submit" class="btn btn-info btn-md">Submit</button>
              </div>

            </div>
          </div>

        </form>
        
      </div>
    </div>

  </section>
  <!--Section: Content-->

</div>
      <!--Google Map-->
      <section class="section">
        <!--Please, add the data attribute data-key="YOUR_API_KEY" in order to insert your own API key for the Google map.-->
        <!--Please note that YOUR_API_KEY should replaced with your key.-->
        <!--Example: <div class="google-map-container" data-key="YOUR_API_KEY">-->
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3024.5439689836494!2d-74.0110142842607!3d40.70604014591637!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a165bedccab%3A0x2cb2ddf003b5ae01!2sWall%20St%2C%20New%20York%2C%20NY%2C%20EE.%20UU.!5e0!3m2!1ses-419!2smx!4v1599252076103!5m2!1ses-419!2smx" width="100%" height="500px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
      </section>

<?php include 'footer.php' ?>