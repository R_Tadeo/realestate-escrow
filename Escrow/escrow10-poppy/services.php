<?php include'header.php';?>
    <!-- END nav -->

    
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">Services</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Services <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>
    <br>
    <section class="section section-lg bg-gray-1" style="background:  #eaf0f1;
">
        <div class="container">
          <div class="row row-50">
            <div class="col-lg-6 pr-xl-5"><img src="images/buy.jpg" alt="" width="518" height="434"/>
            </div>
            <div class="col-lg-6">
              <h2>BUY</h2>
              <div class="text-with-divider">
                <div class="divider"></div>
              </div>
              <p>
A trust is a trust transaction for the purchase of real estate carried out through a third party in this case us. Therefore, we act as fiduciary and the buyer as beneficiary. In other words, a trust gives you the right to manage the property. Under the terms of the transaction, we will have the title safe. At the same time, the buyer becomes the legal owner of the purchased property, with the right to rent, sell, improve and benefit from it, as it suits him best.</p><p>
There are three parties to the bank trust agreement: the seller, the buyer, and a duly authorized Mexican bank. The official names are:</p>
<p> Settlor (seller), Trustee (the nominal buyer known as the bank), Trustee (the actual buyer)</p>
            </div>
          </div>
        </div>
    </section>
<br>
    <section class="section section-lg bg-gray-1">
        <div class="container">
          <div class="row row-50">
            
            <div class="col-lg-6">
              <h2>SELL</h2>
              <div class="text-with-divider">
                <div class="divider"></div>
                
              </div>
              <p>
secure your money at all times and clean the investment or property purchase process, we support you at all times</p><p>In the event that a selling creditor takes an action against you, the assets subject to the trust are exempt from that action and, consequently, are protected, because they are outside the guarantee of their creditors</p><p>La independencia del patrimonio del fideicomiso respecto de los patrimonios del fiduciario y del fideicomitente asegura que dichos bienes</p>
            </div><div class="col-lg-6 pr-xl-5"><img src="images/sale.jpg" alt="" width="518" height="434"/>
            </div>
          </div>
        </div>
    </section>
    <br>
    <section class="section section-lg bg-gray-1" style="background:  #eaf0f1;
">
        <div class="container">
          <div class="row row-50">
            <div class="col-lg-6 pr-xl-5"><img src="images/project-2.jpg" alt="" width="518" height="434"/>
            </div>
            <div class="col-lg-6">
              <h2>ADVISORY</h2>
              <div class="text-with-divider">
                <div class="divider"></div>
              </div>
              <p>We help you discover your needs, analyzing past, present and future circumstances, taking into account your age, your available assets, your tax rate, your professional and family situation, and the rest of the investments you may have.</p><p>s
The relationship between client-advisor is the basis of good advice, so we are committed to having excellent communication, and obtaining trust (with rights and obligations for each party). always looking out for their interests</p>
            </div>
          </div>
        </div>
    </section><br>

   
    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-6 col-lg-3">
            <div class="ftco-footer-widget mb-5">
            	<h2 class="ftco-heading-2">Have a Questions?</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">52 8857898089</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">nfo@security.com</span></a></li>
	              </ul>
	            </div>
	            <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-6 col-lg-2">
            <div class="ftco-footer-widget mb-5 ml-md-4">
              <h2 class="ftco-heading-2">Links</h2>
              <ul class="list-unstyled">
                <li><a href="index.php"><span class="ion-ios-arrow-round-forward mr-2"></span>Home</a></li>
                <li><a href="about.php"><span class="ion-ios-arrow-round-forward mr-2"></span>About</a></li>
                <li><a href="services.php"><span class="ion-ios-arrow-round-forward mr-2"></span>Services</a></li>
                
                <li><a href="contact.php"><span class="ion-ios-arrow-round-forward mr-2"></span>Contact</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-6 col-lg-4">
            <div class="ftco-footer-widget mb-5">
              <h2 class="ftco-heading-2">Recent Blog</h2>
              <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/ddd.jpg);"></a>
                <div class="text">
                  
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span> Oct. 16, 2019</a></div>
                    <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                  </div>
                </div>
              </div>
              <div class="block-21 mb-5 d-flex">
                <a class="blog-img mr-4" style="background-image: url(images/image_2.jpg);"></a>
                <div class="text">
                 
                  <div class="meta">
                    <div><a href="#"><span class="icon-calendar"></span> Oct. 16, 2019</a></div>
                    <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="ftco-footer-widget mb-5">
            	<h2 class="ftco-heading-2">Subscribe Us!</h2>
              <form action="#" class="subscribe-form">
                <div class="form-group">
                  <input type="text" class="form-control mb-2 text-center" placeholder="Enter email address">
                  <input type="submit" value="Subscribe" class="form-control submit px-3">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved  <i class="icon-heart" aria-hidden="true"></i> by ME
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>