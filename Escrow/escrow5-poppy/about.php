<?php include'header.php';?>
  
      <!-- Breadcrumbs -->
      <section class="breadcrumbs-custom bg-image context-dark" style="background-image: url(images/about-us-banner-new-03.jpg);">
        <div class="breadcrumbs-custom-inner">
          <div class="container breadcrumbs-custom-container"style="color:  #000000;">
            <div class="breadcrumbs-custom-main">
              <h6 class="breadcrumbs-custom-subtitle title-decorated">ABOUT US</h6>
              <h1 class="breadcrumbs-custom-title">ABOUT US</h1>
            </div>
            <ul class="breadcrumbs-custom-path">
              <li><a href="index.html">Home</a></li>
              <li class="active">About</li>
            </ul>
          </div>
        </div>
      </section>
      <section class="section section-md">
        <div class="container">
          <!-- Profile Modern-->
          <article class="profile-modern">
            <div class="profile-modern-figure"><img class="profile-modern-image" src="images/hhh.png" alt="" width="336" height="336"/><a class="profile-modern-button" href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F124804277704%2F%3Fmessaging_source%3Dsource%253Apages%253Amessage_shortlink" target="_blank"><span class="icon mdi mdi-facebook-messenger"></span><span class="icon mdi mdi-facebook-messenger"></span></a>
            </div>
            <div class="profile-modern-main">
              <div class="profile-modern-header">
                <div class="profile-modern-header-item">
                  <h3>about</h3>
                  <p>Buy, Sell Consultant</p>
                </div>
                <div class="profile-modern-item">
                  <div class="group group-xs group-middle"><a class="icon icon-sm icon-creative mdi mdi-facebook" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-twitter" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-instagram" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-google" href="#"></a><a class="icon icon-sm icon-creative mdi mdi-linkedin" href="#"></a></div>
                </div>
              </div>
              <div class="row row-30">
                <div class="col-lg-6">
                  <p>We'r personal finance company that guides people through pivotal steps of their financial journey. Whether you’re a recent college graduate searching for your first mortgage, or a retiree seeking effective ways to safely grow your savings, we provides accurate rate information, intuitive calculators and curated editorial content to help you reach your goals. </p>
                </div>

                <div class="col-lg-6">
                        <!-- Linear progress bar-->
                        <article class="progress-linear">
                          <div class="progress-header">
                            <p>buying</p><span class="progress-value">90</span>
                          </div>
                          <div class="progress-bar-linear-wrap">
                            <div class="progress-bar-linear"></div>
                          </div>
                        </article>
                        <!-- Linear progress bar-->
                        <article class="progress-linear">
                          <div class="progress-header">
                            <p>selling</p><span class="progress-value">65</span>
                          </div>
                          <div class="progress-bar-linear-wrap">
                            <div class="progress-bar-linear"></div>
                          </div>
                        </article>
                        <!-- Linear progress bar-->
                        <article class="progress-linear">
                          <div class="progress-header">
                            <p>catered</p><span class="progress-value">100</span>
                          </div>
                          <div class="progress-bar-linear-wrap">
                            <div class="progress-bar-linear"></div>
                          </div>
                        </article>
                </div>
              </div>
            </div>
          </article>
          <br>
          <p>Born in 1999 as jk and associates as a company financial advice,
 our breadth of experience has fueled our reputation as a financial authority. Trusted and approachable, we make decisions based on fact and experience, 
. It’s why tens of millions of people and many of the world’s premier media outlets turn to we.</p>
<p>We have great experience in managing escrow accounts, we support you so that you have the best offer, so your money is safe and guaranteed during the transaction.</p>
        </div>
      </section>
      <!-- Nathan’s Projects-->
      <section class="section section-lg bg-gray-100 text-center">
        <div class="container">
          <h3>out team</h3>
          <div class="isotope offset-top-2" data-isotope-layout="masonry">
            <div class="row row-30">
              <div class="col-12 col-sm-6 col-lg-4 isotope-item wow-outer">
                          <!-- Thumbnail Corporate-->
                          <article class="thumbnail-corporate wow slideInDown"><img class="thumbnail-corporate-image" src="images/p1.png" alt="" width="370" height="256"/>
                            <div class="thumbnail-corporate-caption">
                              <p class="thumbnail-corporate-title">J.J. BENITES</p>
                              <p>I provide my clients with tools and strategies to improve their operations. I have been doing this for 2 decades, so I guarantee my service </p><a class="thumbnail-corporate-link" href="#"><span class="icon mdi mdi-plus"></span><span class="icon mdi mdi-plus"></span></a>
                            </div>
                            <div class="thumbnail-corporate-dummy"> </div>
                          </article>
              </div>
              <div class="col-12 col-sm-6 col-lg-4 isotope-item wow-outer">
                          <!-- Thumbnail Corporate-->
                          <article class="thumbnail-corporate wow slideInUp"><img class="thumbnail-corporate-image" src="images/p2.png" alt="" width="370" height="256"/>
                            <div class="thumbnail-corporate-caption">
                              <p class="thumbnail-corporate-title">  H.P. ROBERT </p>
                              <p>i'm advisor in business since 1990 so i offer quality service and I attend to your needs. have your sure money by me during process.</p><a class="thumbnail-corporate-link" href="#"><span class="icon mdi mdi-plus"></span><span class="icon mdi mdi-plus"></span></a>
                            </div>
                            <div class="thumbnail-corporate-dummy"> </div>
                          </article>
              </div>
              
              <div class="col-12 col-sm-6 col-lg-4 isotope-item wow-outer">
                          <!-- Thumbnail Corporate-->
                          <article class="thumbnail-corporate wow slideInDown"><img class="thumbnail-corporate-image" src="images/p3.png" alt="" width="370" height="256"/>
                            <div class="thumbnail-corporate-caption">
                              <p class="thumbnail-corporate-title">K.M. MOSPH</p>
                              <p>i  will jointly analyze and offer objective advice to enhance your business.</p><a class="thumbnail-corporate-link" href="#"><span class="icon mdi mdi-plus"></span><span class="icon mdi mdi-plus"></span></a>
                            </div>
                            <div class="thumbnail-corporate-dummy"> </div>
                          </article>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="section section-lg bg-gray-100">
        <div class="container">
          <h3 class="wow-outer text-center"><span class="wow slideInUp">Testimonials & Information Video</span></h3>
          <div class="row row-50 justify-content-center justify-content-lg-between">
            <div class="col-md-10 col-lg-5 wow-outer">
              <div class="owl-carousel wow slideInLeft" data-items="1" data-dots="true" data-nav="false" data-loop="true" data-margin="30" data-stage-padding="0" data-mouse-drag="false">
                <blockquote class="quote-modern">
                  <svg class="quote-modern-mark" x="0px" y="0px" width="35px" height="25px" viewbox="0 0 35 25">
                    <path d="M27.461,10.206h7.5v15h-15v-15L25,0.127h7.5L27.461,10.206z M7.539,10.206h7.5v15h-15v-15L4.961,0.127h7.5                L7.539,10.206z"></path>
                  </svg>
                  <div class="quote-modern-text">
                    <p>As a broker we have utilized multiple forms of seller financing in the past, including Bond for Deed contracts. We have found   not only very knowledgeable when it comes to preparing Bond for Deed contracts, but very beneficial in helping buyers and sellers </p>
                  </div>
                  <div class="quote-modern-meta">
                    <div class="quote-modern-avatar"><img src="images/jjjj.jpg" alt="" width="96" height="96"/>
                    </div>
                    <div class="quote-modern-info">
                      <cite class="quote-modern-cite">Albert Webb</cite>
                    
                    </div>
                  </div>
                </blockquote>
                <blockquote class="quote-modern">
                  <svg class="quote-modern-mark" x="0px" y="0px" width="35px" height="25px" viewbox="0 0 35 25">
                    <path d="M27.461,10.206h7.5v15h-15v-15L25,0.127h7.5L27.461,10.206z M7.539,10.206h7.5v15h-15v-15L4.961,0.127h7.5                L7.539,10.206z"></path>
                  </svg>
                  <div class="quote-modern-text">
                    <p>I have been working with Consulter for the past few months and they have accomplished an extraordinary effort in executing our Campaign and have delivered their skills and abilities to meet our promised targets. They have shown their excellence in providing top-notch support.</p>
                  </div>
                  <div class="quote-modern-meta">
                    <div class="quote-modern-avatar"><img src="images/ae4N35Dk.jpg" alt="" width="96" height="96"/>
                    </div>
                    <div class="quote-modern-info">
                      <cite class="quote-modern-cite">Kelly McMillan/cite>
                    </div>
                  </div>
                </blockquote>
                <blockquote class="quote-modern">
                  <svg class="quote-modern-mark" x="0px" y="0px" width="35px" height="25px" viewbox="0 0 35 25">
                    <path d="M27.461,10.206h7.5v15h-15v-15L25,0.127h7.5L27.461,10.206z M7.539,10.206h7.5v15h-15v-15L4.961,0.127h7.5                L7.539,10.206z"></path>
                  </svg>
                  <div class="quote-modern-text">
                    <p>I just wanted to thank your customer service representative who contacted me twice regarding my order status. He left me a second message today with detailed information. I never had this superb customer service from any other companies before.</p>
                  </div>
                  <div class="quote-modern-meta">
                    <div class="quote-modern-avatar"><img src="images/KumashiroHeadshot.jpg" alt="" width="96" height="96"/>
                    </div>
                    <div class="quote-modern-info">
                      <cite class="quote-modern-cite">Harold Barnett</cite>
                    </div>
                  </div>
                </blockquote>
              </div>
            </div>
            <div class="col-md-10 col-lg-6 wow-outer">
              <div class="thumbnail-video-1 bg-gray-700 wow slideInLeft">
                <iframe width="100%" height="420" src="https://www.youtube.com/embed/Zc89EpIbC0E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <div class="thumbnail-video__overlay video-overlay" style="background-size: auto 100px;
       background-size: auto 80%;background-image: url(images/maxresdefault.jpg)">
                  <div class="button-video"></div>
                  <h5>Lawrence Alvarado</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Map & contacts-->
 
     <?php include'footer.php';?>