<?php include'header.php';?>

      <!-- FScreen-->
      <section class="section section-halfscreen bg-image" style="background-image: url(images/yeso.jpg);">
        <div class="section-halfscreen-inner">
          <div class="section-halfscreen-image wow fadeIn" style="background-image: url(images/Fotolia_78278791_Subscription_Monthly_M.jpg);"></div>
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-lg-5">
                <div class="section-halfscreen-content">
                  <div class="wow-outer heading-3">
                    <h2 class="wow fadeInUp" data-wow-delay=".2s"><span class="font-weight-light">ESCROW</span><br><span class="font-weight-bold sumbol text-primary">&</span><span class="font-weight-light">TRUST</span></h2 ><H2 class="font-weight-bold">SERVICES</H2>
                  </div>
                  <div class="wow-outer p">
                    <p class="custom-article wow fadeInDown" data-wow-delay=".2s">WE ARE A COMPANY WITH 20 YEARS OF EXPERIENCE AND WE DEDICATE ON HELP IN PROCESS THE BUY AND SELL. WE HAVE A ESCROW SERVICE SO YOUR TRANSACTION IS SURE </p>
                  </div><a class="button button-primary button-winona wow fadeIn" href="About.phps">More DETAILS</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="tabs-horizontal"></div>
      <!-- Small Features-->
      <section class="section section-lg bg-gray-100">
        <div class="container">
          <div class="row row-30">
            <div class="col-sm-6 col-lg-4 wow-outer">
              <!-- Box Minimal-->
              <article class="box-minimal">
                <div class="box-minimal-icon linearicons-pie-chart wow fadeIn"></div>
                <div class="box-minimal-main wow-outer">
                  <h4 class="box-minimal-title wow slideInDown">Analytics</h4>
                  <p class="wow fadeInUpSmall">Let us help you set the right direction with analytics services to good business.</p>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4 wow-outer">
              <!-- Box Minimal-->
              <article class="box-minimal">
                <div class="box-minimal-icon linearicons-briefcase wow fadeIn" data-wow-delay=".1s"></div>
                <div class="box-minimal-main wow-outer">
                  <h4 class="box-minimal-title wow slideInDown" data-wow-delay=".1s">Management</h4>
                  <p class="wow fadeInUpSmall" data-wow-delay=".1s">The escrow services we offer allow client  to better manage  process the  during the negotiation.</p>
                </div>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4 wow-outer">
              <!-- Box Minimal-->
              <article class="box-minimal">
                <div class="box-minimal-icon linearicons-credit-card wow fadeIn" data-wow-delay=".2s"></div>
                <div class="box-minimal-main wow-outer">
                  <h4 class="box-minimal-title wow slideInDown" data-wow-delay=".2s">Finance  </h4>
                  <p class="wow fadeInUpSmall" data-wow-delay=".2s">Diverse services provided by our experts guarantee increased  for  money security </p>
                </div>
              </article>
            </div>
          </div>
        </div>
      </section>
      <!-- Advantages and Achievements-->
      <section class="section section-lg text-center">
        <div class="container">
          <h3 class="wow-outer"><span class="wow slideInUp">Our Achievements</span></h3>
          <p class="wow-outer"><span class="text-width-1 wow slideInDown">Since our establishment in 1999, our team has achieved a lot. Below are some statistics and interesting facts about our company.</span></p>
          <div class="row row-50">
            <div class="col-6 col-md-3 wow-outer">
              <!-- Counter Minimal-->
              <article class="counter-minimal wow slideInDown">
                <div class="counter-minimal-icon linearicons-star"></div>
                <div class="counter-minimal-main">
                  <div class="counter">40</div>
                </div>
                <h5 class="counter-minimal-title">CONCRETED SALES</h5>
              </article>
            </div>
            <div class="col-6 col-md-3 wow-outer">
              <!-- Counter Minimal-->
              <article class="counter-minimal wow slideInDown">
                <div class="counter-minimal-icon linearicons-folder"></div>
                <div class="counter-minimal-main">
                  <div class="counter">95</div><span class="value">%</span>
                </div>
                <h5 class="counter-minimal-title">Satisfied Clients</h5>
              </article>
            </div>
            <div class="col-6 col-md-3 wow-outer">
              <!-- Counter Minimal-->
              <article class="counter-minimal wow slideInDown">
                <div class="counter-minimal-icon linearicons-flag"></div>
                <div class="counter-minimal-main">
                  <div class="counter">20</div>
                </div>
                <h5 class="counter-minimal-title">Years of Experience</h5>
              </article>
            </div>
            <div class="col-6 col-md-3 wow-outer">
              <!-- Counter Minimal-->
              <article class="counter-minimal wow slideInDown">
                <div class="counter-minimal-icon linearicons-lamp"></div>
                <div class="counter-minimal-main">
                  <div class="counter">100</div>
                </div>
                <h5 class="counter-minimal-title">BUSINES NOW</h5>
              </article>
            </div>
          </div>
        </div>
      </section>
      <!-- CTA Thin-->
      <section class="section section-xs bg-primary-darken text-center">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-sm-10 col-md-12">
              <div class="box-cta-thin">
                <h4 class="title wow-outer"><span class="wow slideInRight">Helping Worldwide in this process</span></h4>
                <div class="wow-outer button-outer"><a class="button button-white-outline button-winona wow slideInLeft" href="contacts.php">Get in Touch</a></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Services-->
      <section class="section section-lg bg-gray-100 text-center">
        <div class="container">
          <h3 class="wow-outer"><span class="wow slideInUp">What We Offer</span></h3>
          <div class="row row-30">
            <div class="col-sm-6 col-lg-4 wow-outer">
              <!-- Box Creative-->
              <article class="box-creative wow slideInDown">
                <div class="box-creative-icon linearicons-store"></div>
                <h4 class="box-creative-title"><a href="#">BUY</a></h4>
                <p>This service helps you discover the attractiveness of your products for customers taking buying patterns into account.</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4 wow-outer">
              <!-- Box Creative-->
              <article class="box-creative wow slideInDown" data-wow-delay=".05s">
                <div class="box-creative-icon linearicons-briefcase"></div>
                <h4 class="box-creative-title"><a href="#">SELL</a></h4>
                <p>Strategic Planning can help you clarify your vision and mission, and reach your goals as you build your company’s strategy.</p>
              </article>
            </div>
            <div class="col-sm-6 col-lg-4 wow-outer">
              <!-- Box Creative-->
              <article class="box-creative wow slideInDown" data-wow-delay=".1s">
                <div class="box-creative-icon linearicons-bubble-text"></div>
                <h4 class="box-creative-title"><a href="#">ADVISORY</a></h4>
                <p>We also provide free consultations to clients looking for better understanding of their business capabilities.</p>
              </article>
            </div>
          </div>
        </div>
      </section>
     
         <?php include'footer.php';?>
    