<?php include 'header.php' ?>

      <!-- Classic Breadcrumbs-->
      <section class="section novi-background breadcrumb-classic pt-5">
        <div class="container section-34 section-sm-50">
          <div class="row align-items-xl-center">
            <div class="col-xl-5 d-none d-xl-block text-xl-left">
              <h2><span class="big">Contact Us</span></h2>
            </div>
            <div class="col-xl-2 d-none d-md-block"><span class="icon icon-white icon-lg mdi mdi-map-marker-circle"></span></div>
            <div class="offset-top-0 offset-md-top-10 col-xl-5 offset-xl-top-0 small text-xl-right">
              <ul class="list-inline list-inline-dashed p">
                <li class="list-inline-item"><a href="index.html">Home</a></li>
                <li class="list-inline-item">Contact Us
                </li>
              </ul>
            </div>
          </div>
        </div>
        <svg class="svg-triangle-bottom" xmlns="http://www.w3.org/2000/svg" version="1.1">
          <defs>
            <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
              <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
              <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
            </lineargradient>
          </defs>
          <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
        </svg>
      </section>
      <!-- Contact Us-->
      <section class="section section-98 section-md-110 text-xl-left">
        <div class="container">
          <div class="row justify-content-sm-center justify-content-sm-center">
            <div class="col-xl-4">
              <h3 class="text-uppercase font-weight-bold">How to Find us</h3>
              <hr class="divider hr-xl-left-0 bg-mantis">
              <p>If you have any questions, just fill in the contact form, and we will answer you shortly. If you are living nearby, come visit Intense in one of our comfortable offices.</p>
              <address class="contact-info offset-top-50">
                <p class="h6 text-uppercase font-weight-bold text-picton-blue letter-space-none offset-top-none">Intense Office 1</p>
                <p>9863 - 9867 MILL ROAD, CAMBRIDGE, MG09 99HT.</p>
                <dl class="offset-top-0">
                  <dt>Telephone</dt>
                  <dd><a href="tel:#">+1 800 603 6035</a></dd>
                </dl>
                <dl>
                  <dt>E-mail:</dt>
                  <dd><a href="mailto:#">mail@demolink.org</a></dd>
                </dl>
              </address>
              <address class="contact-info offset-top-50">
                <p class="h6 text-uppercase font-weight-bold text-picton-blue letter-space-none">Intense Office 2</p>
                <p>9870 ST VINCENT PLACE, GLASGOW, DC 45 FR 45.</p>
                <dl class="offset-top-0">
                  <dt>Telephone</dt>
                  <dd><a href="tel:#">+1 800 559 6580</a></dd>
                </dl>
                <dl>
                  <dt>E-mail:</dt>
                  <dd><a href="mailto:#">mail@demolink.org</a></dd>
                </dl>
              </address>
            </div>
            <div class="col-md-8 offset-top-66 offset-xl-top-0">
              <h3 class="text-uppercase font-weight-bold">Get in touch</h3>
              <hr class="divider hr-xl-left-0 bg-mantis">
              <!-- RD Mailform-->
              <form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                <div class="row">
                  <div class="col-xl-6">
                    <div class="form-group">
                      <label class="form-label form-label-outside" for="contact-us-name">Name:</label>
                      <input class="form-control" id="contact-us-name" type="text" name="name" data-constraints="@Required">
                    </div>
                  </div>
                  <div class="col-xl-6 offset-top-20 offset-xl-top-0">
                    <div class="form-group">
                      <label class="form-label form-label-outside" for="contact-us-email">E-Mail:</label>
                      <input class="form-control" id="contact-us-email" type="email" name="email" data-constraints="@Required @Email">
                    </div>
                  </div>
                  <div class="col-xl-12 offset-top-20">
                    <div class="form-group">
                      <label class="form-label form-label-outside" for="contact-us-message">Message:</label>
                      <textarea class="form-control" id="contact-us-message" name="message" data-constraints="@Required"></textarea>
                    </div>
                  </div>
                </div>
                <div class="group-sm text-center text-xl-left offset-top-30">
                  <button class="btn btn-primary" type="submit">Send</button>
                  <button class="btn btn-default" type="reset">Reset</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
<div class="container-fluid mt-1 mb-5">
<!-- Section: Block Content -->
<section class="mb-4">
  <style>
    .map-container {
      overflow: hidden;
      position: relative;
      height: 0;
    }

    .map-container iframe {
      left: 0;
      top: 0;
      height: 100%;
      width: 100%;
      position: absolute;
    }
  </style>
  <!-- Google Maps -->
  <div id="full-width-map" class="z-depth-1-half map-container" style="height: 500px">
    <iframe src="https://maps.google.com/maps?q=manhatan&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0"
      style="border:0" allowfullscreen></iframe>
  </div>
  <!-- Google Maps -->
</section>
<!-- Section: Block Content -->
</div>

<?php include 'footer.php' ?>