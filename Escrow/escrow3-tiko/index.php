<?php include 'header.php' ?>

      <!--Swiper-->
      <div class="swiper-container swiper-slider" data-loop="true" data-autoplay="true" data-height="100vh" data-dragable="false" data-min-height="480px">
        <div class="swiper-wrapper text-center">
          <div class="swiper-slide" data-slide-bg="images/index1.jpg" data-preview-bg="images/index1.jpg">
            <div class="swiper-caption swiper-parallax">
              <div class="swiper-slide-caption">
                <div class="container">
                  <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                      <h1><span class="big text-uppercase" data-caption-animate="fadeIn" data-caption-delay="700">Welcome to Intense</span></h1>
                    </div>
                    <div class="col-xl-10 offset-top-30">
                      <h4 class="d-none d-lg-block text-light" data-caption-animate="fadeIn" data-caption-delay="900">
                      We make sure everything we do is with an integrity based approach – from our commitment to the highest quality of escrow services, to the way we engage with our customers.
                      </h4>
                      <div class="group group-xl offset-lg-top-50"><a class="btn btn-primary btn-lg btn-anis-effect" href="https://ld-wp2.template-help.com/novi-builder/intense/" target="_blank" data-caption-animate="fadeIn" data-caption-delay="1200"><span class="btn-text">Know our services</span></a><a class="btn btn-default btn-lg btn-anis-effect" href="https://www.templatemonster.com/intense-multipurpose-html-template.html" target="_blank" data-caption-animate="fadeIn" data-caption-delay="1200"><span class="btn-text">Contact us</span></a></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide" data-preview-bg="video/index1.jpg">
                  <div class="bg-vide" data-vide-bg="video/intro-video/intro.mp4" data-vide-options="posterType: jpg">
                    <div class="swiper-caption swiper-parallax">
                      <div class="swiper-slide-caption">
                        <div class="container">
                          <div class="row justify-content-xl-center">
                            <div class="col-xl-12">
                              <h1><span class="big text-uppercase" data-caption-animate="fadeIn" data-caption-delay="700">Built as a framework</span></h1>
                            </div>
                            <div class="col-xl-10 offset-top-30">
                              <h4 class="d-none d-lg-block text-light" data-caption-animate="fadeIn" data-caption-delay="900">
                                Intense was designed as extended version of bootstrap, built as a complex
                                and flexible solution with a dozen of completely new
                                utilities, components and plugins.
                              </h4>
                              <div class="group group-xl offset-lg-top-50"><a class="btn btn-icon btn-icon-left btn-default btn-lg btn-anis-effect" href="#" data-caption-animate="fadeIn" data-caption-delay="1200"><span class="btn-text">See it in action</span></a></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
          </div>
          <div class="swiper-slide" data-slide-bg="images/index2.jpg" data-preview-bg="images/index2.jpg">
            <div class="swiper-caption swiper-parallax">
              <div class="swiper-slide-caption">
                <div class="container">
                  <div class="row justify-content-xl-center">
                    <div class="col-xl-12">
                      <h1><span class="big text-uppercase" data-caption-animate="fadeIn" data-caption-delay="700">ULTRA sharp & RESPONSIVE</span></h1>
                    </div>
                    <div class="col-xl-10 offset-top-30">
                      <h4 class="d-none d-lg-block text-light offset-bottom-0" data-caption-animate="fadeIn" data-caption-delay="900">
                        Beautiful and clean designs are optimized for all screen sizes
                        and types. Taste a new meaning of Retina Ready.
                      </h4>
                      <div class="group group-xl offset-lg-top-50"><a class="btn btn-icon btn-icon-left btn-primary btn-lg btn-anis-effect" href="#" target="_blank" data-caption-animate="fadeIn" data-caption-delay="1200"><span class="novi-icon icon mdi mdi-cart-outline"></span><span class="btn-text">Buy intense now</span></a></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-button swiper-button-prev swiper-parallax">
          <div class="preview">
            <div class="preview__img preview__img-3"></div>
            <div class="preview__img preview__img-2"></div>
            <div class="preview__img preview__img-1"></div>
          </div>
        </div>
        <div class="swiper-button swiper-button-next swiper-parallax">
          <div class="preview">
            <div class="preview__img preview__img-1"></div>
            <div class="preview__img preview__img-2"></div>
            <div class="preview__img preview__img-3"></div>
          </div>
        </div>
        <div class="swiper-pagination"></div>
      </div>
      <!-- Welcome-->
      <section class="section novi-background section-50 section-sm-top-110 section-sm-bottom-98 section-mock-up mb-1" id="home-section-welcome">
        <div class="container">
          <div class="row justify-content-sm-center">
            <div class="col-lg-10">
              <h3>Intense is the
                      <!-- Text Rotator--><span class="text-rotator" data-text-class=" text-mantis text-ubold" data-enable="1200" data-words="most complex,most powerful,most flexible" data-rotate-interval="3000" data-rotate-animation="fadeInUp, fadeOutUp"><span class="rotate-area"></span></span>&nbsp;Real Estate Industry.
              </h3>
              <p>
              Our services are driven by our in-depth understanding of the Real Estate industry, backed by over 20 years of experience, and our passionate team of professionals. Delivering excellent customer experiences is our best competitive differentiator. You can call us your biggest fans. We leverage Alta Best Practices to ensure a well-trained and productive team of experts and operational success, ensuring customer satisfaction.
              </p>
            </div>
          </div>
        </div>
      </section>
      <!-- Mock Up-->
      <section class="section novi-background mt-1 mb-5" style="overflow: hidden;">
        <div class="mock-up-wrapper d-none d-md-inline-block">
          <div class="mock-up-desktop-wrapper wow fadeInUpBig" data-wow-duration="1s" data-wow-delay="0s"><img class="mock-up-desktop img-fluid" src="images/index3.jpg" width="615" height="508" alt=""></div>
          <div class="mock-up-circle d-none d-lg-block wow zoomIn" data-wow-delay="1.6s"><img src="images/index4.jpg" width="309" height="315" alt=""></div>
          <div class="mock-up-mobile-wrapper wow fadeInUp" data-wow-delay="0.9s"><img class="mock-up-mobile" src="images/index5.jpg" width="247" height="470" alt=""></div>
        </div>
      </section>
      <!-- Section main future-->
      <section class="section novi-background section-66 section-top-50 bg-mantis section-triangle section-triangle-bottom context-dark">
        <div class="container">
          <div class="row justify-content-center">
            <h2 class="col-lg-8"><span class="big">Take Full Control of Your Site</span></h2>
            <div class="col-lg-8">
            <p>Our amazing Escrow Options Group family is dedicated to providing an experience that goes above and beyond the norm. We value and foster relationships and are committed to upholding integrity while having an incredible depth of knowledge.</p>
            </div>
          </div>
        </div>
        <svg class="svg-triangle-bottom" xmlns="http://www.w3.org/2000/svg" version="1.1">
          <defs>
            <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
              <stop offset="0%" style="stop-color:rgb(99,189,98);stop-opacity:1"></stop>
              <stop offset="100%" style="stop-color:rgb(99,189,98);stop-opacity:1"></stop>
            </lineargradient>
          </defs>
          <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
        </svg>
      </section>
      <!-- Parallax-->
            <section class="section parallax-container" data-parallax-img="images/index6.jpg">
              <div class="parallax-content context-dark">
                <div class="container">
                  <div class="row justify-content-sm-center section-top-124 section-bottom-110">
                    <div class="col-sm-10 col-md-11 col-xl-12">
                      <div class="row justify-content-sm-center">
                        <div class="col-md-6 col-xl-4">
                                <!-- Icon Box Type 4--><span class="icon novi-icon icon-circle icon-bordered icon-outlined icon-pink-filled mdi mdi-clock"></span>
                                <div>
                                  <h4 class="text-uppercase offset-top-30 font-weight-bold">24 HOUR ACCESS TO TRANSACTIONS</h4>
                                  <p class="inset-left-7p inset-right-7p">GreenFolders Paperless Solution offers instant access to transactions.</p>
                                </div>
                        </div>
                        <div class="col-md-6 col-xl-4 offset-top-66 offset-md-top-0">
                                <!-- Icon Box Type 4--><span class="icon novi-icon icon-circle icon-bordered icon-outlined icon-malibu-filled mdi mdi-pen"></span>
                                <div>
                                  <h4 class="text-uppercase offset-top-30 font-weight-bold">ELECTRONIC DOCUMENT SIGNING</h4>
                                  <p class="inset-left-7p inset-right-7p">Close your deals faster with Docusign Electronic Signing.</p>
                                </div>
                        </div>
                        <div class="col-md-6 col-xl-4 offset-top-66 offset-xl-top-0">
                                <!-- Icon Box Type 4--><span class="icon novi-icon icon-circle icon-bordered icon-outlined icon-carrot-filled mdi mdi-message-reply"></span>
                                <div>
                                  <h4 class="text-uppercase offset-top-30 font-weight-bold">MULTI-LINGUAL OFFICERS & STAFF</h4>
                                  <p class="inset-left-7p inset-right-7p">Our Escrow Officers can serve you in different languages.</p>
                                </div>
                        </div>
                        <div class="col-md-6 col-xl-4 offset-top-66">
                                <!-- Icon Box Type 4--><span class="icon novi-icon icon-circle icon-bordered icon-outlined icon-red-filled mdi mdi-account-switch"></span>
                                <div>
                                  <h4 class="text-uppercase offset-top-30 font-weight-bold">INDEPENDENT ESCROW COMPANY</h4>
                                  <p class="inset-left-7p inset-right-7p">Licensed & regulated by the California Department of Business Oversight</p>
                                </div>
                        </div>
                        <div class="col-md-6 col-xl-4 offset-top-66">
                                <!-- Icon Box Type 4--><span class="icon novi-icon icon-circle icon-bordered icon-outlined icon-blue-gray-filled mdi mdi-certificate"></span>
                                <div>
                                  <h4 class="text-uppercase offset-top-30 font-weight-bold">SSAE CERTIFIED AND SOC COMPLIANT</h4>
                                  <p class="inset-left-7p inset-right-7p">Protecting Your Money and Identity.</p>
                                </div>
                        </div>
                        <div class="col-md-6 col-xl-4 offset-top-66">
                                <!-- Icon Box Type 4--><span class="icon novi-icon icon-circle icon-bordered icon-outlined icon-mantis-filled mdi mdi-chart-line"></span>
                                <div>
                                  <h4 class="text-uppercase offset-top-30 font-weight-bold">PROFESSIONAL ADVISORY</h4>
                                  <p class="inset-left-7p inset-right-7p">With the best data analysis technology and a team of experts.</p>
                                </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
      <!-- About Us-->
      <section class="section novi-background section-98 section-md-110">
        <div class="container">
          <div class="row text-xl-left justify-content-center">
            <div class="col-md-8 col-xl-6">
              <div class="inset-xl-right-20">
                <h3>About Us</h3>
                <hr class="divider hr-xl-left-0 bg-blue-gray">
                <p>Intence is a Escrow office dedicated to the commercialization of Real Estate. Experts in sale, purchase and rental of properties. Comprehensive real estate that provides a wide range of services such as: mortgage loans, investment plans, legal and financial advice.</p>
                <p>Today Intense offers a full range of web design, app development and content marketing services, including building full-featured ecommerce websites and web startups with custom functionality.</p>
            </div>
            </div>
            <div class="col-md-8 col-xl-6 offset-top-30 offset-xl-top-0">
              <div class="shadow-drop-xl">
                <img src="images/index8.jpg" alt="">
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- What Clients say-->
            <section class="section parallax-container bg-gray-darkest" data-parallax-img="images/index7.jpg">
              <div class="parallax-content context-dark">
                <div class="container section-98 section-sm-110">
                  <h2><span class="big">What Clients Say</span></h2>
                  <hr class="divider divider-md bg-mantis">
                  <div class="row offset-top-66">
                          <!-- Custom Pagination-->
                          <ul class="list-inline owl-custom-pagination">
                            <li class="list-inline-item owl-dot-custom rounded-circle img-bordered-white" data-owl-item="0"><img class="rounded-circle" width="90" height="90" src="images/team3.jpg" alt=""></li>
                            <li class="list-inline-item owl-dot-custom rounded-circle img-bordered-white" data-owl-item="1"><img class="rounded-circle" width="90" height="90" src="images/team4.jpg" alt=""></li>
                            <li class="list-inline-item owl-dot-custom rounded-circle img-bordered-white" data-owl-item="2"><img class="rounded-circle" width="90" height="90" src="images/team1.jpg" alt="Alex Murphy"></li>
                            <li class="list-inline-item owl-dot-custom rounded-circle img-bordered-white" data-owl-item="3"><img class="rounded-circle" width="90" height="90" src="images/team5.jpg" alt=""></li>
                            <li class="list-inline-item owl-dot-custom rounded-circle img-bordered-white" data-owl-item="4"><img class="rounded-circle" width="90" height="90" src="images/team2.jpg" alt=""></li>
                          </ul>
                          <!-- Testimonials Slider with Custom Pagination-->
                          <div class="owl-carousel owl-carousel-default owl-carousel-class-light veil-lg-owl-dots veil-owl-nav reveal-lg-owl-nav inset-left-7p inset-right-7p" data-mouse-drag="false" data-active="2" data-loop="false" data-dots="true" data-dots-custom=".owl-custom-pagination" data-nav="true" data-nav-class="[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]">
                            <div>
                              <blockquote class="quote quote-custom-image offset-top-0 offset-md-top-24"><img class="rounded-circle img-bordered-white img-fluid d-block mx-auto d-md-none" width="90" height="90" src="images/team1.jpg" alt="">
                                <p class="quote-body offset-top-34">I want to thank you for doing an awesome job once again with our escrow. It's so comforting to know my clients will be well taken care of through the diligence and professionalism you put forth.</p>
                                <div class="offset-top-41">
                                  <h3 class="font-accent">Kira Force</h3>
                                </div>
                                <p class="text-uppercase font-weight-bold text-spacing-120 offset-top-10"><span class="small text-malibu">Marketing, at theme.co</span></p>
                              </blockquote>
                            </div>
                            <div>
                              <blockquote class="quote quote-custom-image offset-top-0 offset-md-top-24"><img class="rounded-circle img-bordered-white img-fluid d-block mx-auto d-md-none" width="90" height="90" src="images/team1.jpg" alt="">
                                <p class="quote-body offset-top-34">The themes are beautiful But what really makes the difference is the customer service. Both pre and post sales support is second to none. Very fast, friendly and competent. I can only recommend this service.</p>
                                <div class="offset-top-41">
                                  <h3 class="font-accent">Diana Russo</h3>
                                </div>
                                <p class="text-uppercase font-weight-bold text-spacing-120 offset-top-10"><span class="small text-malibu">Freelance UX/UI designer</span></p>
                              </blockquote>
                            </div>
                            <div>
                              <blockquote class="quote quote-custom-image offset-top-0 offset-md-top-24"><img class="rounded-circle img-bordered-white img-fluid d-block mx-auto d-md-none" width="90" height="90" src="images/team1.jpg" alt="Alex Murphy">
                                <p class="quote-body offset-top-34">I am glad I’ve purchased a template from TemplateMonster. Extraordinary customer service, always available, perfect care and knowledge of the technicians. Essential if you are not an expert programmer. In any case highly recommended. Congratulations!</p>
                                <div class="offset-top-41">
                                  <h3 class="font-accent">Alex Murphy</h3>
                                </div>
                                <p class="text-uppercase font-weight-bold text-spacing-120 offset-top-10"><span class="small text-malibu">Free   lance Developer</span></p>
                              </blockquote>
                            </div>
                            <div>
                              <blockquote class="quote quote-custom-image offset-top-0 offset-md-top-24"><img class="rounded-circle img-bordered-white img-fluid d-block mx-auto d-md-none" width="90" height="90" src="images/team1.jpg" alt="">
                                <p class="quote-body offset-top-34">We have purchased several themes and contacted support once, the case had to be passed to technical support but it was resolved within the day. Very fast responses and excellent templates to choose from. Would highly recommend.</p>
                                <div class="offset-top-41">
                                  <h3 class="font-accent">Amanda Smith</h3>
                                </div>
                                <p class="text-uppercase font-weight-bold text-spacing-120 offset-top-10"><span class="small text-malibu">Sales, at themes.com</span></p>
                              </blockquote>
                            </div>
                            <div>
                              <blockquote class="quote quote-custom-image offset-top-0 offset-md-top-24"><img class="rounded-circle img-bordered-white img-fluid d-block mx-auto d-md-none" width="90" height="90" src="images/team1.jpg" alt="">
                                <p class="quote-body offset-top-34">Amazing Support and friendly technicians. I have bothered the team on countless occasions and they have never once seemed perturbed or annoyed by me. Thanks so much!</p>
                                <div class="offset-top-41">
                                  <h3 class="font-accent">Sam Cole</h3>
                                </div>
                                <p class="text-uppercase font-weight-bold text-spacing-120 offset-top-10"><span class="small text-malibu">Founder, at YourTax.com</span></p>
                              </blockquote>
                            </div>
                          </div>
                  </div>
                </div>
              </div>
            </section>
      <section class="section novi-background section-66 section-lg-98 section-xl-124 bg-lighter">
        <div class="container">
          <div>
            <h1><span class="big">Expecience the best Option.</span></h1>
          </div>
        </div>
      </section>

<?php include 'footer.php' ?>