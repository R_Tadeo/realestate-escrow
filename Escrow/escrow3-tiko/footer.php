<!-- Default footer-->
<footer class="section novi-background section-relative section-top-66 section-bottom-34 page-footer bg-black context-dark">
        <div class="container">
          <div class="row justify-content-sm-center text-xl-left grid-group-md">
            <div class="col-sm-12 col-xl-3">
              <!-- Footer brand-->
              <div class="footer-brand"><a href="index.html"><img style='margin-top: -5px;margin-left: -15px;' width='178' height='31' src='images/intense/logo-light.png' alt=''/></a></div>
              <p class="text-darker offset-top-4">Feel the power of the future</p>
                    <ul class="list-inline">
                      <li class="list-inline-item"><a class="icon novi-icon fa fa-facebook icon-xxs icon-circle icon-darkest-filled" href="#"></a></li>
                      <li class="list-inline-item"><a class="icon novi-icon fa fa-twitter icon-xxs icon-circle icon-darkest-filled" href="#"></a></li>
                      <li class="list-inline-item"><a class="icon novi-icon fa fa-google-plus icon-xxs icon-circle icon-darkest-filled" href="#"></a></li>
                      <li class="list-inline-item"><a class="icon novi-icon fa fa-linkedin icon-xxs icon-circle icon-darkest-filled" href="#"></a></li>
                    </ul>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-5 col-xl-4 text-lg-left">
              <h6 class="text-uppercase text-spacing-60">Newsletter</h6>
              <p>Intence is a Escrow office dedicated to the commercialization of Real Estate. Experts in sale, purchase and rental of properties.</p>
              <div class="offset-top-30">
                      <form class="rd-mailform" data-form-output="form-subscribe-footer" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                        <div class="form-group">
                          <div class="input-group input-group-sm"><span class="input-group-prepend"><span class="input-group-text input-group-icon"><span class="novi-icon mdi mdi-email"></span></span></span>
                            <input class="form-control" placeholder="Type your E-Mail" type="email" name="email" data-constraints="@Required @Email"><span class="input-group-append">
                              <button class="btn btn-sm btn-primary" type="submit">Subscribe</button></span>
                          </div>
                        </div>
                        <div class="form-output" id="form-subscribe-footer"></div>
                      </form>
              </div>
            </div>
            <div class="col-sm-5 col-lg-3 col-xl-2 text-sm-left">
              <h6 class="text-uppercase text-spacing-60">Useful Links</h6>
              <div class="d-block">
                <div class="d-inline-block">
                  <ul class="list list-marked">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">About Us</a></li>
                    <li><a href="services.php">Services</a></li>
                    <li><a href="contact.php">Contact Us</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-7 text-sm-left col-lg-4 col-xl-3">
              <h6 class="text-uppercase text-spacing-60">Latest news</h6>
                    <!--Post Widget-->
                    <article class="post widget-post text-left text-picton-blue"><a class="d-block" href="#">
                        <div class="unit flex-row unit-spacing-xs align-items-center">
                          <div class="unit-body">
                            <div class="post-meta"><span class="novi-icon icon-xxs mdi mdi-arrow-right"></span>
                              <time class="text-dark" datetime="2019-01-01">05/14/2019</time>
                            </div>
                            <div class="post-title">
                              <h6 class="text-regular">Let’s Change the world</h6>
                            </div>
                          </div>
                        </div></a></article>
                    <!--Post Widget-->
                    <article class="post widget-post text-left text-picton-blue"><a class="d-block" href="#">
                        <div class="unit flex-row unit-spacing-xs align-items-center">
                          <div class="unit-body">
                            <div class="post-meta"><span class="novi-icon icon-xxs mdi mdi-arrow-right"></span>
                              <time class="text-dark" datetime="2019-01-01">05/14/2019</time>
                            </div>
                            <div class="post-title">
                              <h6 class="text-regular">The meaning of Web Design</h6>
                            </div>
                          </div>
                        </div></a></article>
                    <!--Post Widget-->
                    <article class="post widget-post text-left text-picton-blue"><a class="d-block" href="#">
                        <div class="unit flex-row unit-spacing-xs align-items-center">
                          <div class="unit-body">
                            <div class="post-meta"><span class="novi-icon icon-xxs mdi mdi-arrow-right"></span>
                              <time class="text-dark" datetime="2019-01-01">05/14/2019</time>
                            </div>
                            <div class="post-title">
                              <h6 class="text-regular">Get Started with TemplateMonster</h6>
                            </div>
                          </div>
                        </div></a></article>
            </div>
          </div>
        </div>
        <div class="container offset-top-50">
          <p class="small text-darker">Intense &copy; <span class="copyright-year"></span>
            . Design&nbsp;by&nbsp;<a href="https://www.templatemonster.com">TemplateMonster</a>
          </p>
        </div>
      </footer>
    </div>
    <!-- Global RD Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <!--Coded by ATOM-->
  </body>
</html>