<?php include 'header.php' ?>

      <!-- Classic Breadcrumbs-->
      <section class="section novi-background breadcrumb-classic pt-5">
        <div class="container section-34 section-sm-50">
          <div class="row align-items-xl-center">
            <div class="col-xl-5 d-none d-xl-block text-xl-left">
              <h2><span class="big">About Us</span></h2>
            </div>
            <div class="col-xl-2 d-none d-md-block"><span class="icon icon-white icon-lg mdi mdi-account-multiple"></span></div>
            <div class="offset-top-0 offset-md-top-10 col-xl-5 offset-xl-top-0 small text-xl-right">
              <ul class="list-inline list-inline-dashed p">
                <li class="list-inline-item"><a href="index.html">Home</a></li>
                <li class="list-inline-item">About Us
                </li>
              </ul>
            </div>
          </div>
        </div>
        <svg class="svg-triangle-bottom" xmlns="http://www.w3.org/2000/svg" version="1.1">
          <defs>
            <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
              <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
              <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
            </lineargradient>
          </defs>
          <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
        </svg>
      </section>
      <!-- Who we are?-->
      <section class="section novi-background section-98 section-sm-124 pb-5">
        <div class="container">
          <div class="row">
            <div class="col-xl-5 text-xl-left">
              <h1>Who we are</h1>
              <hr class="divider bg-mantis hr-xl-left-0">
            </div>
          </div>
          <div class="row justify-content-sm-center align-items-md-center offset-top-10 grid-group-md text-lg-left">
            <div class="col-md-10 col-xl-5">
              <p>Intence is a Escrow office dedicated to the commercialization of Real Estate. Experts in sale, purchase and rental of properties. Comprehensive real estate that provides a wide range of services such as: mortgage loans, investment plans, legal and financial advice.</p>
              <p>Today Intense offers a full range of web design, app development and content marketing services, including building full-featured ecommerce websites and web startups with custom functionality.</p>
            </div>
            <div class="col-md-10 col-xl-7">
                    <!-- Linear progress bar-->
                    <div class="progress-linear" data-to="98">
                      <div class="progress-header clearfix"><span class="small text-ubold pull-left">MANAGEMENT</span><span class="small text-ubold pull-right progress-value">70</span></div>
                      <div class="progress-bar-linear-wrap">
                        <div class="progress-bar-linear bg-success"></div>
                      </div>
                    </div>
              <div class="offset-top-50">
                      <!-- Linear progress bar-->
                      <div class="progress-linear" data-to="99">
                        <div class="progress-header clearfix"><span class="small text-ubold pull-left">MARKETING</span><span class="small text-ubold pull-right progress-value">54</span></div>
                        <div class="progress-bar-linear-wrap">
                          <div class="progress-bar-linear bg-info"></div>
                        </div>
                      </div>
              </div>
              <div class="offset-top-50">
                      <!-- Linear progress bar-->
                      <div class="progress-linear" data-to="98">
                        <div class="progress-header clearfix"><span class="small text-ubold pull-left">ANALYSIS</span><span class="small text-ubold pull-right progress-value">87</span></div>
                        <div class="progress-bar-linear-wrap">
                          <div class="progress-bar-linear bg-warning"></div>
                        </div>
                      </div>
              </div>
              <div class="offset-top-50">
                      <!-- Linear progress bar-->
                      <div class="progress-linear" data-to="100">
                        <div class="progress-header clearfix"><span class="small text-ubold pull-left">SATISFACTION</span><span class="small text-ubold pull-right progress-value">65</span></div>
                        <div class="progress-bar-linear-wrap">
                          <div class="progress-bar-linear bg-danger"></div>
                        </div>
                      </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-sm-center offset-top-66 pt-5">
            <div class="col-md-10 col-xl-12">
              <div class="row">
                <div class="col-md-6 col-xl-3">
                        <!-- Box Member-->
                        <div class="box-member"><img class="img-fluid" src="images/agent-1.jpg" alt="">
                          <h5 class="font-weight-bold offset-top-20"><a>John Doe</a> <small class="#">Founder</small>
                          </h5>
                          <div class="box-member-wrap">
                            <div class="box-member-caption">
                              <div class="box-member-caption-inner">
                                      <ul class="list-inline list-inline-xs">
                                        <li class="list-inline-item"><a class="icon novi-icon fa fa-facebook icon-xs icon-circle icon-darker-filled" href="#"></a></li>
                                        <li class="list-inline-item"><a class="icon novi-icon fa fa-twitter icon-xs icon-circle icon-darker-filled" href="#"></a></li>
                                        <li class="list-inline-item"><a class="icon novi-icon fa fa-google-plus icon-xs icon-circle icon-darker-filled" href="#"></a></li>
                                      </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                </div>
                <div class="col-md-6 col-xl-3 offset-top-66 offset-md-top-0 offset-xl-top-0">
                        <!-- Box Member-->
                        <div class="box-member"><img class="img-fluid" src="images/agent-2.jpg" alt="">
                          <h5 class="font-weight-bold offset-top-20"><a>Julia Smith</a> <small class="#">CEO</small>
                          </h5>
                          <div class="box-member-wrap">
                            <div class="box-member-caption">
                              <div class="box-member-caption-inner">
                                      <ul class="list-inline list-inline-xs">
                                        <li class="list-inline-item"><a class="icon novi-icon fa fa-facebook icon-xs icon-circle icon-darker-filled" href="#"></a></li>
                                        <li class="list-inline-item"><a class="icon novi-icon fa fa-twitter icon-xs icon-circle icon-darker-filled" href="#"></a></li>
                                        <li class="list-inline-item"><a class="icon novi-icon fa fa-google-plus icon-xs icon-circle icon-darker-filled" href="#"></a></li>
                                      </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                </div>
                <div class="col-md-6 col-xl-3 offset-top-66 offset-md-top-30 offset-xl-top-0">
                        <!-- Box Member-->
                        <div class="box-member"><img class="img-fluid" src="images/agent-3.jpg" alt="">
                          <h5 class="font-weight-bold offset-top-20"><a>Bernard Show</a> <small class="#">Co-Founder</small>
                          </h5>
                          <div class="box-member-wrap">
                            <div class="box-member-caption">
                              <div class="box-member-caption-inner">
                                      <ul class="list-inline list-inline-xs">
                                        <li class="list-inline-item"><a class="icon novi-icon fa fa-facebook icon-xs icon-circle icon-darker-filled" href="#"></a></li>
                                        <li class="list-inline-item"><a class="icon novi-icon fa fa-twitter icon-xs icon-circle icon-darker-filled" href="#"></a></li>
                                        <li class="list-inline-item"><a class="icon novi-icon fa fa-google-plus icon-xs icon-circle icon-darker-filled" href="#"></a></li>
                                      </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                </div>
                <div class="col-md-6 col-xl-3 offset-top-66 offset-md-top-30 offset-xl-top-0">
                        <!-- Box Member-->
                        <div class="box-member"><img class="img-fluid" src="images/agent-4.jpg" alt="">
                          <h5 class="font-weight-bold offset-top-20"><a>Ronald Oswald</a> <small class="#">Advisor</small>
                          </h5>
                          <div class="box-member-wrap">
                            <div class="box-member-caption">
                              <div class="box-member-caption-inner">
                                      <ul class="list-inline list-inline-xs">
                                        <li class="list-inline-item"><a class="icon novi-icon fa fa-facebook icon-xs icon-circle icon-darker-filled" href="#"></a></li>
                                        <li class="list-inline-item"><a class="icon novi-icon fa fa-twitter icon-xs icon-circle icon-darker-filled" href="#"></a></li>
                                        <li class="list-inline-item"><a class="icon novi-icon fa fa-google-plus icon-xs icon-circle icon-darker-filled" href="#"></a></li>
                                      </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Who we are?-->
      <section class="section novi-background section-98 section-sm-110 bg-gray-darkest context-dark">
        <div class="container-fluid">
          <div class="row justify-content-md-center">
            <div class="col-md-8 col-lg-12">
              <div class="row">
                <div class="col-lg-3 col-md-6">
                        <!-- Counter type 2-->
                        <div class="counter-type-2"><span class="icon novi-icon mdi mdi-home-variant text-mantis"></span>
                          <div class="offset-top-10"><span class="h1 font-weight-bold counter" data-speed="3000" data-from="0" data-to="58249"></span>
                          </div>
                          <div class="offset-xxl-top-10">
                            <div></div>
                          </div>
                        </div>
                </div>
                <div class="col-lg-3 col-md-6 offset-top-66 offset-md-top-0">
                        <!-- Counter type 2-->
                        <div class="counter-type-2"><span class="icon novi-icon mdi mdi-account-multiple-outline text-malibu"></span>
                          <div class="offset-top-10"><span class="h1 font-weight-bold counter" data-speed="2500" data-from="0" data-to="246"></span><span class="h1 font-weight-bold">K</span>
                          </div>
                          <div class="offset-xxl-top-10">
                            <div></div>
                          </div>
                        </div>
                </div>
                <div class="col-lg-3 col-md-6 offset-top-66 offset-lg-top-0">
                        <!-- Counter type 2-->
                        <div class="counter-type-2"><span class="icon novi-icon mdi mdi-clock text-neon-carrot"></span>
                          <div class="offset-top-10"><span class="h1 font-weight-bold counter" data-speed="1500" data-from="0" data-to="1200"></span>
                          </div>
                          <div class="offset-xxl-top-10">
                            <div></div>
                          </div>
                        </div>
                </div>
                <div class="col-lg-3 col-md-6 offset-top-66 offset-lg-top-0">
                        <!-- Counter type 2-->
                        <div class="counter-type-2"><span class="icon novi-icon mdi mdi-emoticon text-ku-crimson"></span>
                          <div class="offset-top-10"><span class="h1 font-weight-bold counter" data-speed="1300" data-from="0" data-to="834"></span><span class="h1 font-weight-bold">K</span>
                          </div>
                          <div class="offset-xxl-top-10">
                            <div></div>
                          </div>
                        </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      
      <!-- What clients Say?-->
      <section class="section novi-background section-98 section-sm-110">
        <div class="container">
          <h1>What Clients Say</h1>
          <hr class="divider divider-md bg-mantis">
          <div class="row justify-content-sm-center offset-top-66">
            <div class="col-md-8 col-xl-4">
                    <!-- Boxed Testimonials-->
                    <blockquote class="quote quote-classic quote-classic-boxed">
                      <div class="quote-body">
                        <p>
                          <q>I want to thank you for doing an awesome job once again with our escrow. It's so comforting to know my clients will be well taken care of through the diligence and professionalism you put forth.</q>
                        </p>
                        <div class="quote-meta"><img class="rounded-circle quote-img" width="80" height="80" src="images/team1.jpg" alt=""></div>
                      </div>
                      <h6 class="quote-author text-uppercase">
                        <cite class="text-normal">Alex Murphy</cite>
                      </h6>
                      <p class="quote-desc">FREELANCE DEVELOPER</p>
                    </blockquote>
            </div>
            <div class="offset-top-41 col-md-8 col-xl-4 offset-xl-top-0">
                    <!-- Boxed Testimonials-->
                    <blockquote class="quote quote-classic quote-classic-boxed">
                      <div class="quote-body">
                        <p>
                          <q>As my blood pressure returns to normal, I wanted to say thank you for you and your team’s above and beyond service (understatement). Really a pleasure doing business with your company.</q>
                        </p>
                        <div class="quote-meta"><img class="rounded-circle quote-img" width="80" height="80" src="images/team2.jpg" alt=""></div>
                      </div>
                      <h6 class="quote-author text-uppercase">
                        <cite class="text-normal">Amanda Smith</cite>
                      </h6>
                      <p class="quote-desc">SALES, AT THEMES.COM</p>
                    </blockquote>
            </div>
            <div class="offset-top-41 col-md-8 col-xl-4 offset-xl-top-0">
                    <!-- Boxed Testimonials-->
                    <blockquote class="quote quote-classic quote-classic-boxed">
                      <div class="quote-body">
                        <p>
                          <q>Thank you everyone. Escrow Options has earned my business forever. Thank you so much, Faith and Karen, for your stellar service!</q>
                        </p>
                        <div class="quote-meta"><img class="rounded-circle quote-img" width="80" height="80" src="images/team3.jpg" alt="alisa milano"></div>
                      </div>
                      <h6 class="quote-author text-uppercase">
                        <cite class="text-normal">Sam Cole</cite>
                      </h6>
                      <p class="quote-desc">FOUNDER, AT YOURTAX.COM</p>
                    </blockquote>
            </div>
          </div>
        </div>
      </section>

<?php include 'footer.php' ?>