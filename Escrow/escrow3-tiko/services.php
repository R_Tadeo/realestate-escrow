<?php include 'header.php' ?>

      <!-- Classic Breadcrumbs-->
      <section class="section novi-background breadcrumb-classic pt-5">
        <div class="container section-34 section-sm-50">
          <div class="row align-items-xl-center">
            <div class="col-xl-5 d-none d-xl-block text-xl-left">
              <h2><span class="big">Services</span></h2>
            </div>
            <div class="col-xl-2 d-none d-md-block"><span class="icon icon-white icon-lg mdi mdi-account-multiple"></span></div>
            <div class="offset-top-0 offset-md-top-10 col-xl-5 offset-xl-top-0 small text-xl-right">
              <ul class="list-inline list-inline-dashed p">
                <li class="list-inline-item"><a href="index.html">Home</a></li>
                <li class="list-inline-item">Services
                </li>
              </ul>
            </div>
          </div>
        </div>
        <svg class="svg-triangle-bottom" xmlns="http://www.w3.org/2000/svg" version="1.1">
          <defs>
            <lineargradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
              <stop offset="0%" style="stop-color:rgb(110,192,161);stop-opacity:1"></stop>
              <stop offset="100%" style="stop-color:rgb(111,193,156);stop-opacity:1"></stop>
            </lineargradient>
          </defs>
          <polyline points="0,0 60,0 29,29" fill="url(#grad1)"></polyline>
        </svg>
      </section>

      <div class="container my-5 p-5 z-depth-1">


<!--Section: Content-->
<section class="text-center dark-grey-text">

  <!-- Section heading -->
  <h4 class="font-weight-bold mb-1 pb-2 offset-none wow fadeInLeft">SERVICES FOR</h4>
  <!-- Section description -->
  <h5 class="font-weight-bold mb-4 pb-2 wow fadeInRight">Our services can save you time, money, and the aggravation of managing installment payments on your real estate investment.</h5>

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-md-4 mb-4">

    <span class="icon novi-icon mdi mdi-home-variant"></span>
      <h5 class="font-weight-bold my-4 wow fadeInLeft">Rentals</h5>
      <p class="grey-text mb-md-0 wow fadeInLeft">
      The buyer can be convinced of the quality before the seller receives the money. The seller does not pay until the buyer has irrevocably deposited the money into the escrow account.
      </p>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-4 mb-4">

    <span class="icon novi-icon mdi mdi-cash-usd"></span>
      <h5 class="font-weight-bold my-4 wow fadeInUp">Sales</h5>
      <p class="grey-text mb-md-0 wow fadeInUp">Our management service solves all tenant concerns and channels them effectively.
Month by month, balances, receipts and account statements are generated that inform both Owners and Tenants of the situation of their rent.
      </p>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-md-4 mb-4">

    <span class="icon novi-icon mdi mdi-chart-areaspline"></span>
      <h5 class="font-weight-bold my-4 wow fadeInRight">Advisory</h5>
      <p class="grey-text mb-0 wow fadeInRight">With the best data analysis technology and a team of experts, Escrow conducts candidate research so that you have the peace of mind of having the best client.
      </p>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

</section>
<!--Section: Content-->


</div>


<!-- Why Choose Us-->
<section class="section novi-background section-top-98 section-sm-top-110 pb-5 section-sm-bottom-110 section-lg-top-66 section-bottom-98 section-xl-bottom-0 bg-lightest">
        <div class="container">
          <div class="row justify-content-md-center align-items-lg-center">
            <div class="col-xl-5 d-none d-xl-inline-block"><img class="img-fluid" width="470" height="770" src="images/services1.jpg" alt=""></div>
            <div class="col-md-10 col-xl-5 section-lg-bottom-50">
              <h1 class="offset-none">Why Choose Us</h1>
              <hr class="divider bg-mantis">
              <div class="offset-top-66 offset-xl-top-50">
                      <!-- Icon Box Type 2-->
                      <div class="unit unit-sm flex-md-row text-md-left">
                        <div class="unit-left"><span class="icon novi-icon text-gray mdi mdi-monitor"></span></div>
                        <div class="unit-body">
                          <h4 class="font-weight-bold text-malibu offset-md-top-14">We use cutting-edge technologies</h4>
                          <p>Our team of professional analysts is constantly roaming the web in search of promising techniques, which will make your business better.</p>
                        </div>
                      </div>
                <div class="offset-top-66 offset-xl-top-34">
                        <!-- Icon Box Type 2-->
                        <div class="unit unit-sm flex-md-row text-md-left">
                          <div class="unit-left"><span class="icon novi-icon text-gray mdi mdi-newspaper"></span></div>
                          <div class="unit-body">
                            <h4 class="font-weight-bold text-malibu offset-md-top-14">We value your time</h4>
                            <p>We know how important it is for you to maintain your schedule, that’s why we do everything to fit it. Our team works on your project in multiple threads.</p>
                          </div>
                        </div>
                </div>
                <div class="offset-top-66 offset-xl-top-34">
                        <!-- Icon Box Type 2-->
                        <div class="unit unit-sm flex-md-row text-md-left">
                          <div class="unit-left"><span class="icon novi-icon text-gray mdi mdi-headset"></span></div>
                          <div class="unit-body">
                            <h4 class="font-weight-bold text-malibu offset-md-top-14">We provide qualified support</h4>
                            <p>Our support team is online 24/7, and is ready to help you with any design - related issue.</p>
                          </div>
                        </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<?php include 'footer.php' ?>