<?php include 'header.php' ?>

<div class="templateux-cover" style="background-image: url(images/slider-2.jpg);">
      <div class="container">
        <div class="row align-items-lg-center">

          <div class="col-lg-6 order-lg-1 text-center mx-auto">
            <h1 class="heading mb-3 text-white" data-aos="fade-up">Services</h1>
            <p class="lead mb-5 text-white" data-aos="fade-up"  data-aos-delay="100">Our services can save you time, money, and the aggravation of managing installment payments on your real estate investment.</p>
          </div>
          
        </div>
      </div>
    </div> <!-- .templateux-cover -->

    <div class="templateux-section pt-0 pb-0">
      <div class="container">
        <div class="row">
          <div class="col-md-12 templateux-overlap">
            <div class="row">
              <div class="col-md-4" data-aos="fade-up" data-aos-delay="100">
                <div class="media block-icon-1 d-block text-left">
                  <div class="icon mb-3">
                    <img src="images/flaticon/svg/001-consultation.svg" alt="Image" class="img-fluid">
                  </div>
                  <div class="media-body">
                    <h3 class="h5 mb-4">Buy</h3>
                    <p>We have an advisor who will make your experience in the acquisition of your property easier</p>
                  </div>
                </div> <!-- .block-icon-1 -->
              </div>
              <div class="col-md-4" data-aos="fade-up" data-aos-delay="200">
                <div class="media block-icon-1 d-block text-left">
                  <div class="icon mb-3">
                    <img src="images/flaticon/svg/002-discussion.svg" alt="Image" class="img-fluid">
                  </div>
                  <div class="media-body">
                    <h3 class="h5 mb-4">Sales</h3>
                    <p>We have a highly trusted staff that supports you to protect your property while the process last</p>
                  </div>
                </div> <!-- .block-icon-1 -->
              </div>
              <div class="col-md-4" data-aos="fade-up" data-aos-delay="300">
                <div class="media block-icon-1 d-block text-left">
                  <div class="icon mb-3">
                    <img src="images/flaticon/svg/003-turnover.svg" alt="Image" class="img-fluid">
                  </div>
                  <div class="media-body">
                    <h3 class="h5 mb-4">Advisory</h3>
                    <p>We advise you on what you need to guarantee a successful and quality service.</p>
                  </div>
                </div> <!-- .block-icon-1 -->
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div> <!-- .templateux-section -->


    <div class="templateux-section bg-light">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center mb-5">
            <h2>Our Features</h2>
          </div>
          <div class="col-md-4 mb-4" data-aos="fade-up">
            <div class="media block-icon-1 d-block text-center">
              <div class="icon mb-3">
                <img src="images/flaticon/svg/004-gear.svg" alt="Image" class="img-fluid">
              </div>
              <div class="media-body">
                <h3 class="h5 mb-4">HR Management</h3>
                <p>We carry out good management of our projects.</p>
              </div>
            </div> <!-- .block-icon-1 -->
          </div>
          <div class="col-md-4 mb-4" data-aos="fade-up" data-aos-delay="100">
            <div class="media block-icon-1 d-block text-center">
              <div class="icon mb-3">
                <img src="images/flaticon/svg/005-conflict.svg" alt="Image" class="img-fluid">
              </div>
              <div class="media-body">
                <h3 class="h5 mb-4">Risk Analysis</h3>
                <p>We know when it is a good deal or not.</p>
              </div>
            </div> <!-- .block-icon-1 -->
          </div>
          <div class="col-md-4 mb-4" data-aos="fade-up" data-aos-delay="200">
            <div class="media block-icon-1 d-block text-center">
              <div class="icon mb-3">
                <img src="images/flaticon/svg/006-meeting.svg" alt="Image" class="img-fluid">
              </div>
              <div class="media-body">
                <h3 class="h5 mb-4">Organization</h3>
                <p>We are associated with many recognized companies</p>
              </div>
            </div> <!-- .block-icon-1 -->
          </div>

        </div>
      </div>
    </div> <!-- .templateux-section -->

    <div class="templateux-section">
      <div class="container">
        <div class="row text-center mb-5">
          <div class="col-12">
            <h2>Happy Customers</h2>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-8 testimonial-wrap">
            <div class="quote">&ldquo;</div>
            <div class="owl-carousel wide-slider-testimonial">
              <div class="item">
                <blockquote class="block-testomonial">
                  <p>&ldquo;I want to thank you for doing an awesome job once again with our escrow. It's so comforting to know my clients will be well taken care of through the diligence and professionalism you put forth.&rdquo;</p>
                  <p><cite>Drew Wilson</cite></p>
                </blockquote>
              </div>

              <div class="item">
                <blockquote class="block-testomonial">
                <p>&ldquo;As my blood pressure returns to normal, I wanted to say thank you for you and your team’s above and beyond service (understatement). Really a pleasure doing business with your company.&rdquo;</p>
                  <p><cite>Craig Smith</cite></p>
                </blockquote>
              </div>

              <div class="item">
                <blockquote class="block-testomonial">
                <p>&ldquo;Thank you everyone. Escrow Options has earned my business forever. Thank you so much, for your stellar service!&rdquo;</p>
                  <p><cite>Bob Robe</cite></p>
                </blockquote>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- .container -->
    </div> <!-- .templateux-section -->

<?php include 'footer.php' ?>