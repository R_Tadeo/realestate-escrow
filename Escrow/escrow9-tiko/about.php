<?php include 'header.php' ?>

<div class="templateux-cover" style="background-image: url(images/hero_3.jpg);">
      <div class="container">
        <div class="row align-items-lg-center">

          <div class="col-lg-6 order-lg-1 text-center mx-auto">
            <h1 class="heading mb-3 text-white" data-aos="fade-up">About Us</h1>
            <p class="lead mb-5 text-white" data-aos="fade-up"  data-aos-delay="100">Feel free to learn more about our team and company on this page. We are always happy to help you!</p>
          </div>
          
        </div>
      </div>
    </div> <!-- .templateux-cover -->



    <div class="templateux-section">

      <div class="container"  data-aos="fade-up">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="mb-5">Our Agency</h2>
            <div class="owl-carousel wide-slider">
              <div class="item">
                <img src="images/slider-1.jpg" alt="Free template by TemplateUX.com" class="img-fluid">
              </div>
              <div class="item">
                <img src="images/slider-2.jpg" alt="Free template by TemplateUX.com" class="img-fluid">
              </div>
              <div class="item">
                <img src="images/slider-3.jpg" alt="Free template by TemplateUX.com" class="img-fluid">
              </div>
            </div> <!-- .owl-carousel -->
          </div>
          <div class="col-lg-5 pl-lg-5">
            <h2 class="mb-5">Why Us?</h2>
            <p>To be the world leaders in Escrow, we have 24 years of experience, reaching our goals by helping others achieve theirs. Everyone wins.</p>
            <div class="accordion" id="accordionExample">


              <div class="accordion-item">
                <h2 class="mb-0 rounded mb-2">
                  <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Trust</a>
                </h2>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div class="accordion-body">
                    <p>Firm hope that a person has that something happens, is or works in a certain way, or that another person acts as they want.</p>
                  </div>
                </div>
              </div>
              
              <div class="accordion-item">
                <h2 class="mb-0 rounded mb-2">
                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Integrity
                  </a>
                </h2>
                
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                  <div class="accordion-body">
                    <p>A person of integrity is one who always does the right thing; that he does everything that he considers good for himself without affecting the interests of other individuals.</p>
                  </div>
                </div>
              </div>
              
              <div class="accordion-item">
                <h2 class="mb-0 rounded mb-2">
                  <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  Honesty
                  </a>
                </h2>
                
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                  <div class="accordion-body">
                    <p>Honesty is the quality of honest, adjective with the meanings of decent, decorous, modest, modest, reasonable, just, honest, upright and honest.</p>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div> <!-- .templateux-section -->

    <div class="templateux-section bg-light py-5" id="templateux-counter-section">
      <div class="container">
        <div class="row">
          <div class="col-md">
            <div class="templateux-counter text-center">
              <span class="templateux-number" data-number="99399">0</span>
              <span class="templateux-label">Number of Clients</span>
            </div>
          </div>
          <div class="col-md">
            <div class="templateux-counter text-center">
              <span class="templateux-number" data-number="199">0</span>
              <span class="templateux-label">Number of Personnel</span>
            </div>
          </div>
          <div class="col-md">
            <div class="templateux-counter text-center">
              <span class="templateux-number" data-number="24">0</span>
              <span class="templateux-label">Years Of Experience</span>
            </div>
          </div>    
        </div>
      </div>
      
    </div>
  </div> <!-- .templateux-section -->

    

    <div class="templateux-section pb-0">
      <div class="container">
        <div class="row text-center mb-5">
          <div class="col-12">
            <h2>Our Team</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-6 col-sm-6 col-md-6 col-lg-4 mb-4">
            <div class="staff" class="staff-img" style="background-image: url(images/person_1.jpg);">
              <a class="desc">
                <h3>Bob Miller</h3>
                <span>CEO and Co-Founder</span>
                <div class="parag">
                  <p>I provide my clients with tools and strategies to improve their operations.</p>
                </div>
              </a>
            </div>
          </div>
          <div class="col-6 col-sm-6 col-md-6 col-lg-4 mb-4">
            <div class="staff" class="staff-img" style="background-image: url(images/person_2.jpg);">
              <a class="desc">
                <h3>Jean Smith</h3>
                <span>Director</span>
                <div class="parag">
                  <p>I will jointly analyze and offer objective advice to enhance your business.</p>
                </div>
              </a>
            </div>
          </div>
          <div class="col-6 col-sm-6 col-md-6 col-lg-4 mb-4">
            <div class="staff" class="staff-img" style="background-image: url(images/person_3.jpg);">
              <a class="desc">
                <h3>David Wilson</h3>
                <span>Advisor</span>
                <div class="parag">
                  <p>I'm advisor in business since 1990 so i offer quality service and I attend to your needs.</p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="templateux-section">
      <div class="container">
        <div class="row text-center mb-5">
          <div class="col-12">
            <h2>Happy Customers</h2>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-8 testimonial-wrap">
            <div class="quote">&ldquo;</div>
            <div class="owl-carousel wide-slider-testimonial">
              <div class="item">
                <blockquote class="block-testomonial">
                  <p>&ldquo;I want to thank you for doing an awesome job once again with our escrow. It's so comforting to know my clients will be well taken care of through the diligence and professionalism you put forth.&rdquo;</p>
                  <p><cite>Drew Wilson</cite></p>
                </blockquote>
              </div>

              <div class="item">
                <blockquote class="block-testomonial">
                <p>&ldquo;As my blood pressure returns to normal, I wanted to say thank you for you and your team’s above and beyond service (understatement). Really a pleasure doing business with your company.&rdquo;</p>
                  <p><cite>Craig Smith</cite></p>
                </blockquote>
              </div>

              <div class="item">
                <blockquote class="block-testomonial">
                <p>&ldquo;Thank you everyone. Escrow Options has earned my business forever. Thank you so much, for your stellar service!&rdquo;</p>
                  <p><cite>Bob Robe</cite></p>
                </blockquote>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- .container -->
    </div> <!-- .templateux-section -->

<?php include 'footer.php' ?>