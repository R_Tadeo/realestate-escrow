<?php include 'header.php' ?>

<div class="templateux-cover" style="background-image: url(images/slider-1.jpg);">
      <div class="container">
        <div class="row align-items-lg-center">

          <div class="col-lg-6 order-lg-1 text-center mx-auto">
            <h1 class="heading mb-3 text-white" data-aos="fade-up">Contact us</h1>
            <p class="lead mb-5 text-white" data-aos="fade-up"  data-aos-delay="100">If you have any questions, just fill in the contact form, and we will answer you shortly. If you are living nearby, come visit Intense in one of our comfortable offices.</p>
          </div>
          
        </div>
      </div>
    </div> <!-- .templateux-cover -->



    <div class="templateux-section">
      <div class="container">
        <div class="row">
          <div class="col-md-7 pr-md-7 mb-5">
            <form action="#" method="post">
              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name">
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email">
              </div>
              <div class="form-group">
                <label for="message">Message</label>
                <textarea name="message" id="message" cols="30" rows="10" class="form-control"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-primary py-3 px-5" value="Send Message">
              </div>
            </form>
          </div>
          <div class="col-md-5">
            <div class="media block-icon-1 d-block text-center">
              <div class="icon mb-3"><span class="ion-ios-location-outline"></span></div>
              <div class="media-body">
                <h3 class="h5 mb-4">Arthur Bldg flr, New York City, USA</h3>
              </div>
            </div> <!-- .block-icon-1 -->

            <div class="media block-icon-1 d-block text-center">
              <div class="icon mb-3"><span class="ion-ios-telephone-outline"></span></div>
              <div class="media-body">
                <h3 class="h5 mb-4">+1 209 923 2302</h3>
              </div>
            </div> <!-- .block-icon-1 -->

            <div class="media block-icon-1 d-block text-center">
              <div class="icon mb-3"><span class="ion-ios-email-outline"></span></div>
              <div class="media-body">
                <h3 class="h5 mb-4">info@yourcompany.com</h3>
              </div>
            </div> <!-- .block-icon-1 -->

          </div>
        </div> <!-- .row -->

      </div>
    </div> <!-- .templateux-section -->
    <section class="mb-4">

    <style>
      .map-container {
        overflow: hidden;
        position: relative;
        height: 0;
      }

      .map-container iframe {
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        position: absolute;
      }
    </style>

    <!-- Google Maps -->
    <div id="full-width-map" class="z-depth-1-half map-container" style="height: 500px">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d24143.819170955885!2d-73.90323423175005!3d40.8504200657308!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2f47d4a49f21f%3A0xc2511c6a544ed303!2sArthur%20Ave%2C%20The%20Bronx%2C%20NY%2C%20EE.%20UU.!5e0!3m2!1ses-419!2smx!4v1601304408567!5m2!1ses-419!2smx" frameborder="0"
        style="border:0" allowfullscreen></iframe>
    </div>
    <!-- Google Maps -->

  </section>

<?php include 'footer.php' ?>