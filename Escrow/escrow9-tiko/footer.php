<footer class="templateux-footer bg-light">
  <div class="container">

    <div class="row mb-5">
      <div class="col-md-4 pr-md-6">
        <div class="block-footer-widget">
          <h3>About</h3>
          <p>EscrowInc is a escrow office dedicated to the commercialization of Real Estate. Experts in sale, purchase and rental of properties.</p>
        </div>
      </div>

          <div class="col-md-3" style="margin-left: 10%; margin-right: 50px;">
            <div class="block-footer-widget">
              <h3>Liks</h3>
              <ul class="list-unstyled">
              <li><a href="index.php" class="animsition-link">Home</a></li>
              <li><a href="about.php" class="animsition-link">About</a></li>
              <li><a href="services.php" class="animsition-link">Services</a></li>
              <li><a href="contact.php" class="animsition-link">Contact</a></li>
              </ul>
            </div>
          </div>

          <div class="col-md-3">
            <div class="block-footer-widget">
              <h3>Connect With Us</h3>
              <ul class="list-unstyled block-social">
                <li><a href="#" class="p-1"><span class="icon-facebook-square"></span></a></li>
                <li><a href="#" class="p-1"><span class="icon-twitter"></span></a></li>
                <li><a href="#" class="p-1"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
        </div> <!-- .row -->

      </div>
    </div> <!-- .row -->

    <div class="row pt-5 text-center">
      <div class="col-md-12 text-center"><p>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | EscrowInc</a>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      </p></div>
    </div> <!-- .row -->

  </div>
</footer> <!-- .templateux-footer -->


</div> <!-- .js-animsition -->


<script src="js/scripts-all.js"></script>
<script src="js/main.js"></script>

</body>
</html>