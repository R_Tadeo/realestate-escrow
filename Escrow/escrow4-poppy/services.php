<?php include'header.php';?>

	<section class="section-30 section-md-40 section-lg-66 section-xl-bottom-90 bg-gray-dark page-title-wrap" style="background-image: url(images/banner-rh.jpg);">
        <div class="container">
          <div class="page-title">
            <h2>SERVICES</h2>
          </div>
        </div>
      </section>
            <section class="bg-whisper">
        <div class="container">
          <div class="row align-items-lg-end">
            <div class="offset-md-1 offset-lg-0 col-md-8 col-lg-6 col-xl-5">
              <div class="section-60 section-md-90">
                <div class="inset-md-left-100">
                  <h3>OUR SERVICES</h3>
                </div>
                <div class="inset-md-left-30 inset-md-right-30">
                  <ul class="list-xl">
                    <li>
                      <article class="icon-box-horizontal">
                        <div class="unit unit-horizontal unit-spacing-md">
                          <div class="unit-left"><span class="novi-icon icon icon-primary icon-lg mercury-icon-satellite"></span></div>
                          <div class="unit-body">
                            <h5><a href="#">BUY</a></h5>
                            <p>We have an advisor who will make your experience in the acquisition of your property easier</p>
                          </div>
                        </div>
                      </article>

                    </li>
                    <li>
                      <article class="icon-box-horizontal">
                        <div class="unit unit-horizontal unit-spacing-md">
                          <div class="unit-left"><span class="novi-icon icon icon-primary icon-lg mercury-icon-partners"></span></div>
                          <div class="unit-body">
                            <h5><a href="#">SELL</a></h5>
                            <p>we have a highly trusted staff that supports you to protect your property while the process last</p>
                          </div>
                        </div>
                      </article>

                    </li>
                    <li>
                      <article class="icon-box-horizontal">
                        <div class="unit unit-horizontal unit-spacing-md">
                          <div class="unit-left"><span class="novi-icon icon icon-primary icon-lg mercury-icon-money"></span></div>
                          <div class="unit-body">
                            <h5><a href="#">ADVICE</a></h5>
                            <p>we advise you on what you need ensuring a quality service/p> </p>
                          </div>
                        </div>
                      </article>

                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-9 col-lg-6 col-xl-7 offset-top-30 offset-md-top-0 d-none d-lg-block">
              <div class="image-wrap-1"><img src="images/okk.jpeg" alt="" width="670" height="578"/>
              </div>
            </div>
          </div>
        </div>
      </section>
     
      <section> 
        
        <center><div style="width: 80%;">
        <br>
      <h3>BUY</h3>

      <p>It is a vehicle of Anglo-Saxon law, whose main advantage is flexibility and adaptability to various situations, for example:</p>
      <ul class="list-marked-variant-2">
                          <li><a href="">Estate planning</a></li>
                          <li><a href=""> Asset protection</a></li>
                          <li><a href="">Implementation of complex investment structures</a></li>
                          <li><a href=""> Deposit in custody of goods in commercial transactions</a></li>
                          <li><a href="">Charity, between
others</a></li>
                         
                        </ul></div>
    </center>
  
      </section>
 <section > 
        
        <center><div style="width: 80%;">
        <br>
      <h3>SELL</h3>
      <p>It is essential to invest in data protection, being an essential element to ensure the confidentiality, continuity and success of your business, as well as the protection of your assets.

</p></div>
    </center>
  
      </section>
       <section> 
        
        <center><div style="width: 80%;">
        <br>
      <h3>ADVICE</h3>
      <p>We offer company incorporation and administration services in all jurisdictions that are legally appropriate for our clients and their objectives.</p>
      <p>The understanding of your needs, our professional team, the strategic location of our offices and the consequent proximity of our advisers provide the opportunity to take advantage of the particularities of each jurisdiction and to offer customized solutions.</p></div>
    </center>
  
      </section>
      <br>

<?php include'footer.php';?>