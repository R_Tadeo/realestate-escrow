<?php include'header.php';?>

      <section class="section-30 section-md-40 section-lg-66 section-xl-bottom-90 bg-gray-dark page-title-wrap" style="background-image: url(images/ba.jpg);">
        <div class="container">
          <div class="page-title">
            <h2>About Us</h2>
          </div>
        </div>
      </section>

      <section class="section-66 section-md-90 section-xl-bottom-120">
        <div class="container">
          <h3>Who We Are</h3>
          <div class="row row-40 justify-content-xl-between align-items-lg-center">
            <div class="col-md-6 col-xl-5 text-secondary">
              <div class="inset-md-right-15 inset-xl-right-0">
                <p>We are a company that started work in the 90's, so we have extensive experience in advising and the escrow service process.</p>
                <p>In February 1990 we started with services based in manhatan with only a basic staff, now we are an international company with several branches and we have a workforce of more than 200 employees</p>
              </div>
            </div>
            <div class="col-md-6">
              <ul class="list-progress">
                <li>sell</p>
                  <div class="progress-bar-js progress-bar-horizontal progress-bar-sisal" data-value="70" data-stroke="4" data-easing="linear" data-counter="true" data-duration="1000" data-trail="100"></div>
                </li>
                <li>
                  <p class="animated fadeIn">buy</p>
                  <div class="progress-bar-js progress-bar-horizontal progress-bar-laser" data-value="54" data-stroke="4" data-easing="linear" data-counter="true" data-duration="1000" data-trail="100"></div>
                </li>
                <li>
                  <p class="animated fadeIn">escrow</p>
                  <div class="progress-bar-js progress-bar-horizontal progress-bar-fuscous-gray" data-value="87" data-stroke="4" data-easing="linear" data-counter="true" data-duration="1000" data-trail="100"></div>
                </li>
                <li>
                  <p class="animated fadeIn">advisory</p>
                  <div class="progress-bar-js progress-bar-horizontal progress-bar-leather" data-value="90" data-stroke="4" data-easing="linear" data-counter="true" data-duration="1000" data-trail="100"></div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container" style="color: black">
          <p>
            The people who are part of the team are a key element within the company. The collective effort and discipline within the team allows us to execute and deliver projects successfully, always forming teams that have the best skills that are needed according to the field in which we have to operate to satisfy the needs of our clients. Mutual trust is essential to the ethical conduct of each of our employees. Trust is built with transparency, with the responsibility given to each employee and partner to make decisions and we expect for each activity the assignment under the principles of equity and justice, based on the capabilities of each employee and partner, with trust that we share the same vision of achieving as a team to fulfill our mission. Leadership is essential to induce action was, with clear objectives and efficient responses to our clients.
          </p>
          
        </div>
      </section>
      <hr> 

      <section><center>
        <div class="container">
<h3>
what is trust</h3>
        <iframe width="80%" height="427" src="https://www.youtube.com/embed/Zc89EpIbC0E" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></center></section>

      <section class="section-60 section-lg-90 bg-whisper">
        <div class="container">
          <div class="row row-40 align-items-sm-end">
            <div class="col-sm-6 col-md-4 col-lg-3">
              <div class="thumbnail-variant-2-wrap">
                <div class="thumbnail thumbnail-variant-2">
                  <figure class="thumbnail-image"><img src="images/pu/1.jpg" alt="" width="246" height="300"/>
                  </figure>
                  <div class="thumbnail-inner">
                    <div class="link-group"><span class="novi-icon icon icon-xxs icon-primary material-icons-local_phone"></span><a class="link-white" href="tel:#">+1 (409) 987–5874</a></div>
                    <div class="link-group"><span class="novi-icon icon icon-xxs icon-primary fa-envelope-o"></span><a class="link-white" href="mailto:#">info@demolink.org</a></div>
                  </div>
                  <div class="thumbnail-caption">
                    <p class="text-header"><a href="#">Amanda Smith</a></p>
                    <div class="divider divider-md bg-teak"></div>
                    <p class="text-caption">Paralegal</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
              <div class="thumbnail-variant-2-wrap">
                <div class="thumbnail thumbnail-variant-2">
                  <figure class="thumbnail-image"><img src="images/desc/hg2.jpg" alt="" width="246" height="300"/>
                  </figure>
                  <div class="thumbnail-inner">
                    <div class="link-group"><span class="novi-icon icon icon-xxs icon-primary material-icons-local_phone"></span><a class="link-white" href="tel:#">+1 (409) 987–5874</a></div>
                    <div class="link-group"><span class="novi-icon icon icon-xxs icon-primary fa-envelope-o"></span><a class="link-white" href="mailto:#">info@demolink.org</a></div>
                  </div>
                  <div class="thumbnail-caption">
                    <p class="text-header"><a href="#">John Doe</a></p>
                    <div class="divider divider-md bg-teak"></div>
                    <p class="text-caption">Attorney</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
              <div class="thumbnail-variant-2-wrap">
                <div class="thumbnail thumbnail-variant-2">
                  <figure class="thumbnail-image"><img src="images/pu/3.jpg" alt="" width="246" height="300"/>
                  </figure>
                  <div class="thumbnail-inner">
                    <div class="link-group"><span class="novi-icon icon icon-xxs icon-primary material-icons-local_phone"></span><a class="link-white" href="tel:#">+1 (409) 987–5874</a></div>
                    <div class="link-group"><span class="novi-icon icon icon-xxs icon-primary fa-envelope-o"></span><a class="link-white" href="mailto:#">info@demolink.org</a></div>
                  </div>
                  <div class="thumbnail-caption">
                    <p class="text-header"><a href="#">Vanessa Ives</a></p>
                    <div class="divider divider-md bg-teak"></div>
                    <p class="text-caption">Legal Assistant</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-12 col-lg-3 text-center">
              <div class="block-wrap-1">
                <div class="block-number">06</div>
                <h3 class="text-normal">Experts</h3>
                <p class="h5 h5-smaller text-style-4">in Their sell</p>
                <p>If you or your business is challenge, contact us today to arrange a free initial consultation with an adviser and he help in your problem.</p><a class="link link-group link-group-animated link-bold link-secondary" href="contact.php"><span>Read more</span><span class="novi-icon icon icon-xxs icon-primary fa fa-angle-right"></span></a>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section parallax-container bg-black" data-parallax-img="images/test.jpg">
        <div class="parallax-content">
          <div class="section-60 section-md-90 section-xl-120">
            <div class="container text-center">
              <div class="row row-40 justify-content-lg-center">
                <div class="col-sm-12">
                  <h3>What People Say</h3>
                </div>
                <div class="col-lg-11 col-xl-9">
                  <div class="owl-carousel-inverse">
                    <div class="owl-carousel owl-nav-position-numbering" data-autoplay="true" data-items="1" data-stage-padding="0" data-loop="false" data-margin="30" data-nav="true" data-numbering="#owl-numbering-1" data-animation-in="fadeIn" data-animation-out="fadeOut">
                      <div class="item">
                              <blockquote class="quote-minimal quote-minimal-inverse">
                                <div class="quote-body">
                                  <p>
                                    <q>We are very satisfied with our experience with them. They helped us from the beginning of the process until the purchase of my property was made. thanks for your support</q>
                                  </p>
                                </div>
                                <div class="quote-meta">
                                  <cite>Mark Wilson</cite>
                                  <p class="caption">Client</p>
                                  <div class="unit-left"><img class="img-circle" style="width:60px; height:60px;" src="images/desc/h12.jpg" alt="" />
                    </div>
                                </div>
                              </blockquote>
                      </div>
                      <div class="item">
                              <blockquote class="quote-minimal quote-minimal-inverse">
                                <div class="quote-body">
                                  <p>
                                    <q>Outstanding service and results over the last several years! We have closed several Bond for Deed contracts with they and all have resulted in a positive outcome! We received thorough accounting records in a timely manner for each and every transaction! We love the way ESCROW SERVICES takes the "worry" out of the monthly handling of mortgage payments, escrows, etc! Keep up the GOOD WORK!"</q>
                                  </p>
                                </div>
                                <div class="quote-meta">
                                  <cite>Kate Wilson</cite>
                                  <p class="caption">Client</p>
                                  <div class="unit-left"><img class="img-circle" style="width:60px; height:60px;" src="images/desc/m15.jpg" alt="" />
                    </div>
                                </div>
                              </blockquote>
                      </div>
                      <div class="item">
                              <blockquote class="quote-minimal quote-minimal-inverse">
                                <div class="quote-body">
                                  <p>
                                    <q>Thank you for helping us get into our new home. Working with they helped us understand how a Bond for Deed could help us. You answered all of our questions and made the whole process very easy</q>
                                  </p>
                                </div>
                                <div class="quote-meta">
                                  <cite>Sam Cole</cite>
                                  <p class="caption">Client</p>
                                  <div class="unit-left"><img class="img-circle" style="width:60px; height:60px;" src="images/desc/m33.jpg" alt="" />
                    </div>
                                </div>
                              </blockquote>
                      </div>
                    </div>
                    <div class="owl-numbering owl-numbering-default" id="owl-numbering-1">
                      <div class="numbering-current"></div>
                      <div class="numbering-separator"></div>
                      <div class="numbering-count"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      

<?php include'footer.php';?>