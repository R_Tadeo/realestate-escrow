<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <title>Home</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/iii.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=PT+Serif:400,700,400italic,700italic%7CLato:300,300italic,400,400italic,700,900%7CMerriweather:700italic">
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
	
  </head>
  <body>
    <div class="preloader">
      <div class="preloader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"> </div>
        </div>
        <p>Loading...</p>
      </div>
    </div>
    <div class="page">

      <header class="page-head">
        <div class="rd-navbar-wrap">
                      <nav class="rd-navbar rd-navbar-default" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="53px" data-xl-stick-up-offset="53px" data-xxl-stick-up-offset="53px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
                          <div class="rd-navbar-inner">
                              <div class="rd-navbar-aside-wrap">
                                    <div class="rd-navbar-aside">
                                        <div class="rd-navbar-aside-toggle" data-rd-navbar-toggle=".rd-navbar-aside">
                                          <span></span>
                                        </div>
                                        <div class="rd-navbar-aside-content">
                                           <ul class="rd-navbar-aside-group list-units">
                                              <li>
                                                 <div class="unit unit-horizontal unit-spacing-xs align-items-center">
                                                      <div class="unit-left">
                                                           <span class="novi-icon icon icon-xxs icon-primary material-icons-phone">
                                                           </span>
                                                      </div>
                                                      <div class="unit-body"><a class="link-dusty-gray" href="tel:#">+1 (232) 349–8447</a>
                                                      </div>
                                                 </div>
                                             </li>
                                             <li>
                                                 <div class="unit unit-horizontal unit-spacing-xs align-items-center">
                                                      <div class="unit-left"><span class="novi-icon icon icon-xxs icon-primary fa-envelope-o"></span>
                                                      </div>
                                                       <div class="unit-body"><a class="link-dusty-gray" href="mailto:#">info@moneysafe.com</a>
                                                       </div>
                                                 </div>
                                            </li>
                                         </ul>
                                         <div class="rd-navbar-aside-group">
                                              <ul class="list-inline list-inline-reset">
                                                 <li><a class="novi-icon icon icon-circle icon-nobel-filled icon-xxs-smaller fa fa-facebook" href="#"></a>
                                                 </li>
                                                 <li><a class="novi-icon icon icon-circle icon-nobel-filled icon-xxs-smaller fa fa-twitter" href="#"></a>
                                                 </li>
                                                  <li><a class="novi-icon icon icon-circle icon-nobel-filled icon-xxs-smaller fa fa-google-plus" href="#"></a>
                                                  </li>
                                                </ul>
                                          </div>
                                         </div>
                                    </div>
              </div>
              <div class="rd-navbar-group">
                <div class="rd-navbar-panel">
                  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button><a class="rd-navbar-brand brand" href="index.php"><img style="width:143px; height:100px;"  src="images/SAFE.png" alt="" /></a>
                </div>
                <div class="rd-navbar-nav-wrap">
                  <div class="rd-navbar-nav-inner">
                  <!--  <div class="rd-navbar-btn-wrap"><a class="button button-smaller button-primary-outline" href="#">Appointment</a></div>-->
                    <ul class="rd-navbar-nav">
                      <li class="active"><a href="index.php">Home</a>
                      </li>
                      <li><a href="about-us.php">About </a>
                      </li>
                      <li><a href="services.php">services</a>
                      </li>
                      <li><a href="contact-us.php">Contacts</a>
                      </li>

                    </ul>
                  </div>
                </div>

              </div>
            </div>
          </nav>
        </div>
        <hr>
      </header>