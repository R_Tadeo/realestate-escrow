<?php include 'header.php' ?>

<!-- Breadcrumbs-->

<section class="section section-md mb-1" style="background: url(img/about1.jpg) no-repeat;background-size:cover;background-position: top center; height: 400px;">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-xl-8 text-center">
              <h1 class="font-weight-bold wow fadeInLeft mt-5 text-white">About Us</h1>
              <h6 class="intro-description wow fadeInRight text-white mt-3">Feel free to learn more about our team and company on this page. We are always happy to help you!</h6>
            </div>
          </div>
        </div>
      </section>
<!--Mailform-->

<section class="section section-md">
        <div class="container">
          <div class="row row-40 justify-content-center">
            <div class="col-lg-6 col-12">
              <div class="offset-top-45 offset-lg-right-45">
                <div class="section-name wow fadeInRight" data-wow-delay=".2s">About us</div>
                <h3 class="wow fadeInLeft text-capitalize" data-wow-delay=".3s">A Few Words<span class="text-primary"> about us</span></h3>
                <p class="font-weight-bold text-gray-dark wow fadeInUp" data-wow-delay=".4s">To be the world leaders in real estate, reaching our goals by helping others achieve theirs. Everyone wins.</p>
                <p class="font-weight-bold text-gray-dark wow fadeInUp" data-wow-delay=".4s">Escrow is:</p>
                <p class="wow fadeInUp" data-wow-delay=".4s">
                - Regularly audited by government authorities. <br>
                - Armed with an in-depth knowledge of the escrow process. <br>
                - The safest service to trust with your money. <br>
                - A licensed and regulated escrow company compliant with Escrow Law. <br>
                - Online escrow that is simple and safe for Buyers and Sellers.
                </p>
              </div>
            </div>
            <div class="col-lg-6 col-sm-10 col-12">
              <div class="block-decorate-img wow fadeInLeft" data-wow-delay=".2s"><img src="img/about2.jpg" alt="" width="570" height="351"/>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Our Principles-->
      <section class="section section-lg bg-gray-100">
        <div class="container text-center text-lg-left wow fadeInRight">
          <h2><span class="text-light">Our</span> Principles</h2>
          <div class="row row-30 number-counter-2">
            <div class="col-md-4">
              <div class="box-numbered-left unit">
                <div class="unit-left">
                  <div class="index-counter"></div>
                </div>
                <div class="unit-body">
                  <h5 class="title">Trust</h5>
                  <div class="content">Firm hope that a person has that something happens, is or works in a certain way, or that another person acts as they want.</div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="box-numbered-left unit">
                <div class="unit-left">
                  <div class="index-counter"></div>
                </div>
                <div class="unit-body">
                  <h5 class="title">Integrity</h5>
                  <div class="content">A person of integrity is one who always does the right thing; that he does everything that he considers good for himself without affecting the interests of other individuals.</div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="box-numbered-left unit">
                <div class="unit-left">
                  <div class="index-counter"></div>
                </div>
                <div class="unit-body">
                  <h5 class="title">Honesty</h5>
                  <div class="content">Honesty is the quality of honest, adjective with the meanings of decent, decorous, modest, modest, reasonable, just, honest, upright and honest.</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="container my-5 z-depth-1 px-0 rounded">


  <!--Section: Content-->
  <section class="white-text grey p-5 rounded wow fadeInLeft">
    
    <h3 class="text-center font-weight-bold mb-4 pb-2">Counter</h3>

    <div class="row">

      <div class="col-md-4 mb-4">
        <div class="row">
          <div class="col-6 pr-0">
            <h4 class="display-4 text-right mb-0 count-up" data-from="0" data-to="42" data-time="2000">42</h4>
          </div>

          <div class="col-6">
            <p class="text-uppercase font-weight-normal mb-1">Projects</p>
            <p class="mb-0"><i class="mercury-icon-briefcase" style="font-size:40px;"></i></p>
          </div>
        </div>
      </div>

      <div class="col-md-4 mb-4">
        <div class="row">
          <div class="col-6 pr-0">
            <h4 class="display-4 text-right mb-0 count1" data-from="0" data-to="3500" data-time="2000">3,500</h4>
          </div>

          <div class="col-6">
            <p class="text-uppercase font-weight-normal mb-1">Customers</p>
            <p class="mb-0"><i class="mercury-icon-users" style="font-size:40px;"></i></p>
          </div>
        </div>
      </div>

      <div class="col-md-4 mb-4">
        <div class="row">
          <div class="col-6 pr-0">
            <h4 class="display-4 text-right"><span class="d-flex justify-content-end"><span class="count2" data-from="0" data-to="100" data-time="2000">0</span> %</span></h4>
          </div>

          <div class="col-6">
            <p class="text-uppercase font-weight-normal mb-1">Satisfaction</p>
            <p class="mb-0"><i class="mercury-icon-partners" style="font-size:40px;"></i></p>
          </div>
        </div>
      </div>

    </div>

  </section>
  <!--Section: Content-->


</div>
      <!-- Our Clients-->
      <section class="section section-lg bg-gray-800">
        <div class="container text-center text-xl-left">
          <h2>Our <span class="text-light">Clients</span></h2>
          <div class="container z-depth-1 my-5">


    <!-- Logo carousel -->
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" data-interval="1800">
      <div class="carousel-inner">
        <!-- First slide -->
        <div class="carousel-item active">
          <!--Grid row-->
          <div class="row">

            <!--First column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/5.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/7.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/Second column-->

            <!--Third column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/6.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/Third column-->

            <!--Fourth column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/9.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/Fourth column-->

          </div>
          <!--/Grid row-->
        </div>
        <!-- First slide -->

        <!-- Second slide -->
        <div class="carousel-item">
          <!--Grid row-->
          <div class="row">

            <!--First column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/11.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/10.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/Second column-->

            <!--Third column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/12.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/Third column-->

            <!--Fourth column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/13.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/Fourth column-->

          </div>
          <!--/Grid row-->
        </div>
        <!-- Second slide -->
      </div>

    </div>
    <!-- Logo carousel -->
    

</div>
        </div>
      </section>
      <!-- Our Team-->
      <section class="section section-lg bg-default">
        <div class="container text-center text-lg-left">
          <h2><span class="text-light">Our</span> Team</h2>
          <div class="row row-40">
            <div class="col-md-6">
              <div class="team-box-left">
                <div class="team-meta unit align-items-center">
                  <div class="unit-left"><img class="img" src="img/team1.jpg" alt="" width="138" height="69"/>
                  </div>
                  <div class="unit-body">
                    <h5 class="title">Victor Jackson</h5>
                    <div class="subtitle">CEO and Financial Consultant</div>
                  </div>
                </div>
                <div class="content">Victor Jackson is an award winning entrepreneur, technologist and lecturer, 
                    having won numerous awards including being named the inaugural BRW Entrepreneur of the Year in 2011. 
                    Victor was previously founder and CEO of Sensory Networks Inc., a vendor of high performance network 
                    security processors, which Intel Corporation (NASDAQ: INTC) announced in 2013 it was acquiring. Victor 
                    is also an Adjunct Associate Professor at the Department of Electrical and Information Engineering at 
                    the University of Sydney, where for the last 12 years he has taught Cryptography, and from 2010, Technology 
                    Entrepreneurship. He is the co-author of over 20 US patent applications.</div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="team-box-left">
                <div class="team-meta unit align-items-center">
                  <div class="unit-left"><img class="img" src="img/team2.jpg" alt="" width="138" height="69"/>
                  </div>
                  <div class="unit-body">
                    <h5 class="title">Olivia Smith</h5>
                    <div class="subtitle">Tax Manager</div>
                  </div>
                </div>
                <div class="content">Olivia Smith is our Escrow Manager and is responsible for managing all funds held 
                    on behalf of our customers. Olivia manages the escrow operations of Escrow.com from incoming funds to 
                    disbursement and is responsible for both internal and external audits. Escrow Manager is a designation 
                    that requires approval from the California Department of Business Oversight, which has been granted to 
                    Olivia due to her years of experience in accounting and payments.</div>
              </div>
            </div>
          </div>
        </div>
      </section>
      

<?php include 'footer.php' ?>