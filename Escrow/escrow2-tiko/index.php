<?php include 'header.php' ?>


      <!--Swiper-->
      <section class="section swiper-container swiper-slider swiper-slider-1 context-dark" data-loop="true" data-autoplay="5000" data-simulate-touch="false">
        <div class="swiper-wrapper">
          <div class="swiper-slide" data-slide-bg="img/carusell.jpg">
            <div class="swiper-slide-caption section-lg">
              <div class="container">
                <div class="row">
                  <div class="col-md-9 col-lg-7 offset-md-1 offset-xxl-0">
                    <h1><span class="d-block" data-caption-animate="fadeInUp" data-caption-delay="100">Professional Accounting</span><span class="d-block text-light" data-caption-animate="fadeInUp" data-caption-delay="200">& Tax Services</span></h1>
                    <p class="lead" data-caption-animate="fadeInUp" data-caption-delay="350">Providing every client with the attention they deserve.</p>
                    <div class="button-wrap-default" data-caption-animate="fadeInUp" data-caption-delay="450"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide" data-slide-bg="img/carusell1.jpg">
            <div class="swiper-slide-caption section-lg">
              <div class="container">
                <div class="row">
                  <div class="col-md-9 col-lg-7 offset-md-1 offset-xxl-0">
                    <h1><span class="d-block" data-caption-animate="fadeInUp" data-caption-delay="100">Full Accounting Support</span><span class="d-block text-light" data-caption-animate="fadeInUp" data-caption-delay="200">for Your Business</span></h1>
                    <p class="lead" data-caption-animate="fadeInUp" data-caption-delay="350">Get rid of any accounting issues with our team’s assistance.</p>
                    <div class="button-wrap-default" data-caption-animate="fadeInUp" data-caption-delay="450"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-slide" data-slide-bg="img/carusell2.jpg">
            <div class="swiper-slide-caption section-lg">
              <div class="container">
                <div class="row">
                  <div class="col-md-9 col-lg-7 offset-md-1 offset-xxl-0">
                    <h1><span class="d-block" data-caption-animate="fadeInUp" data-caption-delay="100">#1 Tax Services Provider</span><span class="d-block text-light" data-caption-animate="fadeInUp" data-caption-delay="200">Since the Late 1990s</span></h1>
                    <p class="lead" data-caption-animate="fadeInUp" data-caption-delay="350">We offer specialist tax knowledge and full 24/7 support.</p>
                    <div class="button-wrap-default" data-caption-animate="fadeInUp" data-caption-delay="450"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--Swiper Pagination-->
        <div class="swiper-pagination-wrap">
          <div class="container">
            <div class="row">
              <div class="col-md-9 col-lg-7 offset-md-1 offset-xxl-0">
                <div class="swiper-pagination"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--Icon List 2-->
      <section class="section section-sm section-sm-bottom-100 bg-primary">
        <div class="container wow fadeInRight">
          <div class="row row-30">
            <div class="col-md-4 wow fadeInUp">
              <div class="box-icon-2">
                <div class="icon novi-icon mercury-icon-house"></div>
                <h5 class="title">Rentals</h5>
                <p class="text">Our management service solves all tenant concerns and channels them effectively.</p>
              </div>
            </div>
            <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
              <div class="box-icon-2">
                <div class="icon novi-icon mercury-icon-money-2"></div>
                <h5 class="title">Sales</h5>
                <p class="text">We make sure that the buyer of your property is determined to buy and has the resources to carry out the sale.</p>
              </div>
            </div>
            <div class="col-md-4 wow fadeInUp" data-wow-delay="0.4s">
              <div class="box-icon-2">
                <div class="icon novi-icon mercury-icon-speak"></div>
                <h5 class="title">Advisory</h5>
                <p class="text">With the best data analysis technology and a team of experts, Escrow conducts candidate research so that you have the peace of mind of having the best client.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Download Our Tax Guide App-->
      <section class="section bg-gray-100 box-image-left">
        <div class="container">
          <div class="row">
            <div class="col-md-6 wow fadeInLeft">
              <div class="section-lg section-lg-top-100">
                <h2 class="title-icon title-icon-2"><span class="icon icon-default mercury-icon-touch"></span><span>A Few Words<br><span class="text-light"> about us</span></span></h2>
                <p class="big">To be the world leaders in real estate, reaching our goals by helping others achieve theirs. Everyone wins.</p>
                <p class="big">With over 20 years of experience, we have a lot to offer to our clients. Here are some reasons why companies worldwide choose us.</p>
                <div class="group-sm group-wrap-2"><a class="button button-primary" href="about.php">Read more</a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="box-image-item box-image-video novi-background bg-image" style="background-image: url(img/index1.jpg)"></div>
      </section>
      <!-- Our Mission-->
      <section class="section bg-default section-md">
        <div class="container">
          <div class="row row-30">
            <div class="col-md-6">
              <h2 class="title-icon"><span class="icon icon-default mercury-icon-target-2"></span><span>Our <span class="text-light">Mission</span></span></h2>
              <p class="big">Our mission is to provide accounting and tax services of a superior quality. The responsive, personal attention we provide for each client, regardless how large or small, is the key to our approach.</p>
              <ul class="list-marked-2">
                <li>Providing high-quality accounting & tax services</li>
                <li>Sharing professional experience in tax management</li>
                <li>Offering full support to all our clients</li>
              </ul>
            </div>
            <div class="col-md-6">
              <div class="row">
                <div class="col-6">
                  <!--Box counter-->
                  <div class="box-counter">
                    <div class="box-counter-main">
                      <div class="counter">750</div>
                    </div>
                    <p class="box-counter-title">Satisfied Clients</p>
                  </div>
                </div>
                <div class="col-6">
                  <!--Box counter-->
                  <div class="box-counter">
                    <div class="box-counter-main">
                      <div class="counter">68</div>
                    </div>
                    <p class="box-counter-title">Team Members</p>
                  </div>
                </div>
                <div class="col-6">
                  <!--Box counter-->
                  <div class="box-counter">
                    <div class="box-counter-main">
                      <div class="counter">243</div>
                    </div>
                    <p class="box-counter-title">Successful Cases</p>
                  </div>
                </div>
                <div class="col-6">
                  <!--Box counter-->
                  <div class="box-counter">
                    <div class="box-counter-main">
                      <div class="counter">99</div><span>%</span>
                    </div>
                    <p class="box-counter-title">Satisfaction Rate</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>



      <section class="parallax-container" data-parallax-img="img/index2.jpg">
        <div class="parallax-content section-xl context-dark text-center">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-10 col-xl-9">
                <h2>Monthly <span class="text-light">Offer</span></h2>
                <div class="heading-5 font-weight-normal">Get our FREE financial consultation for your business taxes</div>
                <!--TimeCircles-->
                      <div class="countdown" data-countdown data-to="2019-12-31">
                        <div class="countdown-block countdown-block-days">
                          <svg class="countdown-circle" x="0" y="0" width="200" height="200" viewbox="0 0 200 200" data-progress-days="">
                            <circle class="countdown-circle-bg" cx="100" cy="100" r="90"></circle>
                            <circle class="countdown-circle-fg clipped" cx="100" cy="100" r="90"></circle>
                          </svg>
                          <div class="countdown-wrap">
                            <div class="countdown-counter" data-counter-days="">00</div>
                            <div class="countdown-title">days</div>
                          </div>
                        </div>
                        <div class="countdown-block countdown-block-hours">
                          <svg class="countdown-circle" x="0" y="0" width="200" height="200" viewbox="0 0 200 200" data-progress-hours="">
                            <circle class="countdown-circle-bg" cx="100" cy="100" r="90"></circle>
                            <circle class="countdown-circle-fg clipped" cx="100" cy="100" r="90"></circle>
                          </svg>
                          <div class="countdown-wrap">
                            <div class="countdown-counter" data-counter-hours="">00</div>
                            <div class="countdown-title">hours</div>
                          </div>
                        </div>
                        <div class="countdown-block countdown-block-minutes">
                          <svg class="countdown-circle" x="0" y="0" width="200" height="200" viewbox="0 0 200 200" data-progress-minutes="">
                            <circle class="countdown-circle-bg" cx="100" cy="100" r="90"></circle>
                            <circle class="countdown-circle-fg clipped" cx="100" cy="100" r="90"></circle>
                          </svg>
                          <div class="countdown-wrap">
                            <div class="countdown-counter" data-counter-minutes="">00</div>
                            <div class="countdown-title">minutes</div>
                          </div>
                        </div>
                        <div class="countdown-block countdown-block-seconds">
                          <svg class="countdown-circle" x="0" y="0" width="200" height="200" viewbox="0 0 200 200" data-progress-seconds="">
                            <circle class="countdown-circle-bg" cx="100" cy="100" r="90"></circle>
                            <circle class="countdown-circle-fg clipped" cx="100" cy="100" r="90"></circle>
                          </svg>
                          <div class="countdown-wrap">
                            <div class="countdown-counter" data-counter-seconds="">00</div>
                            <div class="countdown-title">seconds</div>
                          </div>
                        </div>
                      </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Our Clients-->
      <section class="section section-lg bg-gray-100">
        <div class="container text-center text-xl-left">
          <h2>Our <span class="text-light">Clients</span></h2>
          <div class="container z-depth-1 my-5">


    <!-- Logo carousel -->
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" data-interval="1800">
      <div class="carousel-inner">
        <!-- First slide -->
        <div class="carousel-item active">
          <!--Grid row-->
          <div class="row">

            <!--First column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/5.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/7.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/Second column-->

            <!--Third column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/6.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/Third column-->

            <!--Fourth column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/9.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/Fourth column-->

          </div>
          <!--/Grid row-->
        </div>
        <!-- First slide -->

        <!-- Second slide -->
        <div class="carousel-item">
          <!--Grid row-->
          <div class="row">

            <!--First column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/11.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/10.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/Second column-->

            <!--Third column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/12.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/Third column-->

            <!--Fourth column-->
            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
              <img src="https://mdbootstrap.com/img/Photos/Template/13.png" class="img-fluid px-4" alt="Logo">
            </div>
            <!--/Fourth column-->

          </div>
          <!--/Grid row-->
        </div>
        <!-- Second slide -->
      </div>

    </div>
    <!-- Logo carousel -->
    

</div>
        </div>
      </section>

<div class="container mt-5 mb-5">


  <!--Section: Content-->
  <section class="text-center dark-grey-text">

    <!-- Section heading -->
    <h3 class="font-weight-bold mb-4 pb-2">Testimonials</h3>

		<div class="wrapper-carousel-fix">
      <!-- Carousel Wrapper -->
      <div id="carousel-example-1" class="carousel no-flex testimonial-carousel slide carousel-fade" data-ride="carousel"
        data-interval="false">
        <!--Slides-->
        <div class="carousel-inner" role="listbox">
          <!--First slide-->
          <div class="carousel-item active">
            <div class="testimonial">
              <!--Avatar-->
              <div class="avatar mx-auto mb-4">
                <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg" class="rounded-circle img-fluid"
                  alt="First sample avatar image">
              </div>
              <!--Content-->
              <p>
                <i class="fas fa-quote-left"></i> Two years after CostraX’s involvement, we are remarkably successful and the leader in served markets. This brought industry knowledge, best practices, and real-world solutions to the intricate challenges we faced.
              </p>
              <h4 class="font-weight-bold">Anna Deynah</h4>
              <!--Review-->
              <i class="fas fa-star blue-text"> </i>
              <i class="fas fa-star blue-text"> </i>
              <i class="fas fa-star blue-text"> </i>
              <i class="fas fa-star blue-text"> </i>
              <i class="fas fa-star blue-text"> </i>
              <i class="fas fa-star-half-alt blue-text"> </i>
            </div>
          </div>
          <!--First slide-->
          <!--Second slide-->
          <div class="carousel-item">
            <div class="testimonial">
              <!--Avatar-->
              <div class="avatar mx-auto mb-4">
                <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(31).jpg" class="rounded-circle img-fluid"
                  alt="Second sample avatar image">
              </div>
              <!--Content-->
              <p>
                <i class="fas fa-quote-left"></i> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
                odit
                aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque
                porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia
                non numquam eius modi tempora incidunt ut labore. </p>
              <h4 class="font-weight-bold">Maria Kate</h4>
              <!--Review-->
              <i class="fas fa-star blue-text"> </i>
              <i class="fas fa-star blue-text"> </i>
              <i class="fas fa-star blue-text"> </i>
              <i class="fas fa-star blue-text"> </i>
              <i class="fas fa-star blue-text"> </i>
            </div>
          </div>
          <!--Second slide-->
          <!--Third slide-->
          <div class="carousel-item">
            <div class="testimonial">
              <!--Avatar-->
              <div class="avatar mx-auto mb-4">
                <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(3).jpg" class="rounded-circle img-fluid"
                  alt="Third sample avatar image">
              </div>
              <!--Content-->
              <p>
                <i class="fas fa-quote-left"></i> Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus
                error sit voluptatem accusantium doloremque laudantium.</p>
              <h4 class="font-weight-bold">John Doe</h4>
              <!--Review-->
              <i class="fas fa-star blue-text"> </i>
              <i class="fas fa-star blue-text"> </i>
              <i class="fas fa-star blue-text"> </i>
              <i class="fas fa-star blue-text"> </i>
              <i class="far fa-star blue-text"> </i>
            </div>
          </div>
          <!--Third slide-->
        </div>
        <!--Slides-->
        <!--Controls-->
        <a class="carousel-control-prev left carousel-control" href="#carousel-example-1" role="button"
          data-slide="prev">
          <span class="icon-prev" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next right carousel-control" href="#carousel-example-1" role="button"
          data-slide="next">
          <span class="icon-next" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
        <!--Controls-->
      </div>
      <!-- Carousel Wrapper -->
    </div>

  </section>
  <!--Section: Content-->


</div>

<?php include 'footer.php' ?>