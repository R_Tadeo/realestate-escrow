<?php include 'header.php' ?>

<!-- Breadcrumbs-->

<section class="section section-md mb-1" style="background: url(img/services1.jpg) no-repeat;background-size:cover;background-position: top center; height: 400px;">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-xl-8 text-center">
              <h1 class="font-weight-bold wow fadeInLeft mt-5 text-white">Services</h1>
              <h6 class="intro-description wow fadeInRight text-white mt-3">Our services can save you time, money, and the aggravation of managing installment payments on your real estate investment.</h6>
            </div>
          </div>
        </div>
      </section>
<!--Mailform-->

<div class="container my-5 p-5 z-depth-1">


  <!--Section: Content-->
  <section class="dark-grey-text">

    <!-- Section heading -->
    <h2 class="text-center font-weight-bold mb-4 pb-2 wow fadeInLeft">What do we have to offer you?</h2>
    <!-- Section description -->
    <p class="text-center lead grey-text mx-auto mb-5 wow fadeInRight">These are our services and the benefits you get by acquiring them.</p>

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-4 wow fadeInLeft">

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-2">
            <i class="mercury-icon-house" style="font-size:40px;"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3">Rentals</h5>
            <p class="grey-text">Our management service solves all tenant concerns and channels them effectively.
Month by month, balances, receipts and account statements are generated that inform both Owners and Tenants of the situation of their rent.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-2">
          <i class="mercury-icon-money-2" style="font-size:40px;"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3">Sales</h5>
            <p class="grey-text">We make sure that the buyer of your property is determined to buy and has the resources to carry out the sale.
The delivery of the sale opinion is one of the most important points of our service because at that moment is when we advise you in detail after having determined everything in relation to your sale (valuation, sale process, tax opinion).</p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row mb-md-0 mb-3">

          <!-- Grid column -->
          <div class="col-2">
          <i class="mercury-icon-speak" style="font-size:40px;"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3">Advisory</h5>
            <p class="grey-text mb-md-0">With the best data analysis technology and a team of experts, Escrow conducts candidate research so that you have the peace of mind of having the best client.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 text-center">
        <img class="img-fluid" style="height: 700px;" src="img/services2.jpg"
          alt="Sample image">
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 wow fadeInRight">

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-2">
          <i class="mercury-icon-lock" style="font-size:40px;"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3">Safe and Secure and Without the risk of Prepayment
            </h5>
            <p class="grey-text">The buyer can be convinced of the quality before the seller receives the money. The seller does not pay until the buyer has irrevocably deposited the money into the escrow account.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-2">
          <i class="mercury-icon-scales" style="font-size:40px;"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3">Fair and Transparent</h5>
            <p class="grey-text">The step-by-step process ensures fair and transparent handling. Deliberate fraud is excluded as no merchandise is delivered or money is paid without proper consideration. A payment or refund of money is only made by agreement between both parties.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row">

          <!-- Grid column -->
          <div class="col-2">
          <i class="mercury-icon-globe" style="font-size:40px;"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3">International</h5>
            <p class="grey-text mb-0">Carefree cooperation across country borders - Escrow supports deposits and withdrawals in 119 countries and EUR, USD, GBP, CHF, AUD and CAD currencies.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </section>
  <!--Section: Content-->


</div>

<div class="container my-5">


  <!--Section: Content-->
  <section>
    <!-- Section heading -->
    <h3 class="font-weight-bold black-text mb-4 pb-2 text-center">Frequently Asked Questions</h3>
    <hr class="w-header">
    <!-- Section description -->
    <p class="lead text-muted mx-auto mt-4 pt-2 mb-5 text-center">Got a question? We've got answers. If you have some other questions, see our support center.</p>

		<div class="row">
      <div class="col-md-12 col-lg-10 mx-auto mb-5">
      
        <!--Accordion wrapper-->
        <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

          <!-- Accordion card -->
          <div class="card border-top border-bottom-0 border-left border-right border-light">

            <!-- Card header -->
            <div class="card-header border-bottom border-light" role="tab" id="headingOne1">
              <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
                aria-controls="collapseOne1">
                <h5 class="black-text font-weight-normal mb-0">
                  Is this a secure site for purchases? <i class="fas fa-angle-down rotate-icon"></i>
                </h5>
              </a>
            </div>

            <!-- Card body -->
            <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1"
              data-parent="#accordionEx">
              <div class="card-body">
                Absolutely! We work with top payment companies which guarantees your safety and security. All billing information is stored on our payment processing partner which has the most stringent level of certification available in the payments industry.
              </div>
            </div>

          </div>
          <!-- Accordion card -->

          <!-- Accordion card -->
          <div class="card border-bottom-0 border-left border-right border-light">

            <!-- Card header -->
            <div class="card-header border-bottom border-light" role="tab" id="headingTwo2">
              <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                aria-expanded="false" aria-controls="collapseTwo2">
                <h5 class="black-text font-weight-normal mb-0">
                  How long are your contracts? <i class="fas fa-angle-down rotate-icon"></i>
                </h5>
              </a>
            </div>

            <!-- Card body -->
            <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
              data-parent="#accordionEx">
              <div class="card-body">
              	Currently, we only offer monthly subscription. You can upgrade or cancel your monthly account at any time with no further obligation.
              </div>
            </div>

          </div>
          <!-- Accordion card -->

          <!-- Accordion card -->
          <div class="card border-bottom-0 border-left border-right border-light">

            <!-- Card header -->
            <div class="card-header border-bottom border-light" role="tab" id="headingThree3">
              <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
                aria-expanded="false" aria-controls="collapseThree3">
                <h5 class="black-text font-weight-normal mb-0">
                  Can I cancel my subscription? <i class="fas fa-angle-down rotate-icon"></i>
                </h5>
              </a>
            </div>

            <!-- Card body -->
            <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3"
              data-parent="#accordionEx">
              <div class="card-body">
								You can cancel your subscription anytime in your account. Once the subscription is cancelled, you will not be charged next month. You will continue to have access to your account until your current subscription expires.
              </div>
            </div>

          </div>
          <!-- Accordion card -->
          
          <!-- Accordion card -->
          <div class="card border-left border-right border-light">

            <!-- Card header -->
            <div class="card-header border-bottom border-light" role="tab" id="headingThree4">
              <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree4"
                aria-expanded="false" aria-controls="collapseThree4">
                <h5 class="black-text font-weight-normal mb-0">
                  Can I request refund? <i class="fas fa-angle-down rotate-icon"></i>
                </h5>
              </a>
            </div>

            <!-- Card body -->
            <div id="collapseThree4" class="collapse" role="tabpanel" aria-labelledby="headingThree4"
              data-parent="#accordionEx">
              <div class="card-body">
								Unfortunately, not. We do not issue full or partial refunds for any reason.    
              </div>
            </div>

          </div>
          <!-- Accordion card -->

        </div>
        <!-- Accordion wrapper -->
        
      </div>
    </div>

	</section>
  
  
</div>

<?php include 'footer.php' ?>