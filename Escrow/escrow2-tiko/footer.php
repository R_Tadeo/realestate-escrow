<footer class="section footer-classic">
        <div class="footer-inner-1">
          <div class="container">
            <div class="row row-40">
            <div class="col-lg-5 ml-5">
              <!--Brand--><a class="brand wow fadeInLeft" href="index.php"><img class="brand-logo-dark" src="images/logo-inverse-310x44.png"></a><br><br>
              <p class="footer-classic-description offset-top-0 offset-right-25 wow fadeInUp">Escrow office dedicated to the commercialization of Real Estate. Experts in sale, purchase and rental of properties. Comprehensive real estate that provides a wide range of services such as: mortgage loans, investment plans, legal and financial advice</p>
            </div>
              <div class="col-md-5 col-lg-2 ml-5 mr-5 wow fadeInUp">
                <h5 class="wow fadeInRight">Our Contacts</h5>
                <ul class="contact-list font-weight-bold">
                  <li>
                    <div class="unit unit-spacing-xs">
                      <div class="unit-left">
                        <div class="icon icon-sm icon-primary novi-icon mdi mdi-map-marker"></div>
                      </div>
                      <div class="unit-body"><a href="#">14973 Interurban Ave <br>New York, NY 98168</a></div>
                    </div>
                  </li>
                  <li>
                    <div class="unit unit-spacing-xs">
                      <div class="unit-left">
                        <div class="icon icon-sm icon-primary novi-icon mdi mdi-phone"></div>
                      </div>
                      <div class="unit-body"><a href="tel:#">+1 (800) 123 1234</a></div>
                    </div>
                  </li>
                  <li>
                    <div class="unit unit-spacing-xs">
                      <div class="unit-left">
                        <div class="icon icon-sm icon-primary novi-icon mdi mdi-clock"></div>
                      </div>
                      <div class="unit-body">
                        <ul class="list-0">
                          <li>Weekdays: 8:00–17:00</li>
                          <li>Weekends: Closed</li>
                        </ul>
                      </div>
                    </div>
                  </li>
                </ul><a class="d-inline-block big" href="mailto:#">mail@demolink.org</a>
              </div>
              <div class="col-md-6 col-lg-3 ml-3">
                <h5 class="wow fadeInRight">Links</h5>
                <div class="row row-5 justify-content-between">
                  <div class="col-sm-6 wow fadeInUp">
                    <ul class="footer-list big">
                      <li><a href="#">- Home</a></li>
                      <li><a href="#">- About</a></li>
                      <li><a href="#">- Services</a></li>
                      <li><a href="#">- Contact</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-inner-2">
          <div class="container">
            <p class="rights wow fadeInRight"><span>&copy;&nbsp;</span><span class="copyright-year"></span>. CostraX. All Rights Reserved.</a></p>
          </div>
        </div>
      </footer>
    </div>
    <div class="snackbars" id="form-output-global"></div>
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <!--coded by Starlight-->
  </body>
</html>