<?php include 'header.php' ?>

<!-- Breadcrumbs-->

<section class="section section-md mb-1" style="background: url(img/contact1.jpg) no-repeat;background-size:cover;background-position: top center; height: 400px;">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-xl-8 text-center">
              <h1 class="font-weight-bold wow fadeInLeft mt-5 text-white">Contact</h1>
              <h6 class="intro-description wow fadeInRight text-white mt-4">Ready to look forward to Quality Contracts, Quality Servicing, and Fewer Defaults? Contact Escrow now and put a professional team to work for you!</h6>
            </div>
          </div>
        </div>
      </section>
<!--Mailform-->
      <!-- Contacts-->
      <section class="section section-md">
        <div class="container">
          <h2 class="wow fadeInLeft">Contacts</h2>
          <div class="row row-40">
            <div class="col-md-10 col-lg-6 wow fadeInLeft">
              <h5>Our main office</h5>
              <div class="group-lg group-wrap-3">
                <div class="group-item">
                  <dl class="list-terms-2">
                    <dt>Phone:</dt>
                    <dd><a href="tel:#">+1 (800) 123 1234</a></dd>
                  </dl>
                  <dl class="list-terms-2">
                    <dt>Fax:</dt>
                    <dd><a href="tel:#">+1 (800) 123 1234</a></dd>
                  </dl><a class="d-inline-block font-weight-bold big link-underline" href="mailto:#">mail@demolink.org</a>
                </div>
                <div class="group-item">
                  <ul class="list list-0">
                    <li class="text-secondary">14973 Interurban Ave <br>Suite 101 <br>New York, NY 98168</li>
                    <li>Mon-Fri: 8:00 AM - 5:00 PM</li>
                  </ul>
                </div>
              </div>
              <!--Please, add the data attribute data-key="YOUR_API_KEY" in order to insert your own API key for the Google map.-->
              <!--Please note that YOUR_API_KEY should replaced with your key.-->
              <!--Example: <div class="google-map-container" data-key="YOUR_API_KEY">-->
              <div class="google-map-container google-map-container-2" data-center="9870 St Vincent Place, Glasgow, DC 45 Fr 45." data-zoom="5" data-icon="images/gmap_marker.png" data-icon-active="images/gmap_marker_active.png" data-styles="[{&quot;featureType&quot;:&quot;landscape&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:60}]},{&quot;featureType&quot;:&quot;road.local&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:40},{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;administrative.province&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;},{&quot;lightness&quot;:30}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ef8c25&quot;},{&quot;lightness&quot;:40}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;geometry.stroke&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;poi.park&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#b6c54c&quot;},{&quot;lightness&quot;:40},{&quot;saturation&quot;:-40}]},{}]">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14029379.265706783!2d-109.08828959480687!3d30.85471532818056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864070360b823249%3A0x16eb1c8f1808de3c!2sTexas%2C%20EE.%20UU.!5e0!3m2!1ses-419!2smx!4v1599748905234!5m2!1ses-419!2smx" width="600" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
              </div>
            </div>
            <div class="col-md-10 col-lg-6 wow fadeInRight">
              <h5>Get in Touch</h5>
              <!--RD Mailform-->
              <form class="rd-form rd-mailform" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                <div class="row row-10">
                  <div class="col-md-6">
                    <div class="form-wrap">
                      <input class="form-input" id="contact-first-name" type="text" name="name" data-constraints="@Required">
                      <label class="form-label" for="contact-first-name">First Name</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-wrap">
                      <input class="form-input" id="contact-last-name" type="text" name="name" data-constraints="@Required">
                      <label class="form-label" for="contact-last-name">Last Name</label>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="form-wrap">
                      <label class="form-label" for="contact-message">Message</label>
                      <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                    </div>
                  </div>
                  <div class="col-md-7 col-xl-8">
                    <div class="form-wrap">
                      <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                      <label class="form-label" for="contact-email">E-mail</label>
                    </div>
                  </div>
                  <div class="col-md-5 col-xl-4">
                    <button class="button button-size-1 button-block button-primary" type="submit">Send</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>

<?php include 'footer.php' ?>