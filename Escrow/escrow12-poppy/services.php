 <?php include'header.php';?>
    
    <main>

        <!-- slider Area Start-->
        <div class="slider-area ">
            <!-- Mobile Menu -->
            <div class="single-slider slider-height2 d-flex align-items-center" data-background="assets/img/hero/law_hero2.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Services</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <!-- slider Area End-->
            <section class="about-area">
                <div class="container-fluid">
                    <div class="row d-flex justify-content-end align-items-center">
                        <div class="col-lg-6 about-left">
                            <div class="single-about pb-30 container" style="width: 80%;" >
                                <h4>To buy</h4>
                                <p>No matter where you are shopping for a home, at some point you will find yourself mired in an escrow.</p><p>We have offices throughout the world where one of our advisors will gladly support you in making the purchase of the home of your dreams, avoiding losses as much as possible.
                                </p>
                            </div>
                            <hr>
                            <div class="single-about pb-30 container" style="width: 80%;">
                                <h4>Sell</h4>
                                <p>When you want to sell a property, you can secure the buying process through escrow services.</p><p>through which losses are avoided by ensuring the equity and investment of both parties</p>
                            </div>
                            <hr>
                            <div class="single-about container" style="width: 80%;" >
                                <h4>adviser </h4>
                                <p>we have qualified advisors who will support you at all times. that supports you by conducting market and investment studies, always assuring clients and giving them the importance they have
                                </p>
                            </div>                                                              
                        </div>          
                        <div class="col-lg-6 about-right no-padding">
                            <img class="img-fluid" src="assets/img/banner/559-1-scaled.jpg" alt="">
                            
                        </div>          
                    </div>
                </div>  
            </section>

        <br>
                   

    </main>
  
         <?php include'footer.php';?>