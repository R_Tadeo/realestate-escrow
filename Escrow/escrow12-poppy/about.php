<?php include'header.php';?>
    <main>

        <!-- slider Area Start-->
             <div class="about-low-area about-bg about-padding">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-5">
                        <!-- section tittle -->
                        <div class="section-tittle section-tittle-l mb-70">
                            <h2>About Us</h2>
                        </div>
                        <div class="about-caption mb-100">
                            <p>We are a tax, legal, financial and auditing and accounting consulting services company specialized in escrow services, .</p>
                           <!-- Counter Up -->
                            <div class="count-clients">
                                <div class="single-counter text-center">
                                    <span class="counter">250</span>
                                    <p>Happy Clients</p>
                                </div>
                                <div class="single-counter text-center">
                                    <span class="counter">920</span>
                                    <p>Consultancies</p>
                                </div>
                           </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7">
                        <!-- about-img -->
                        <div class="about-img ">
                            <div class="about-font-img f-left">
                                <img src="assets/img/hero/ds.jpg" alt="">
                            </div>
                            <div class="about-back-img f-right d-none d-md-block">
                                <img src="assets/img/hero/us.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="container" style="width: 80%">
            <div><h1></h1>
                <p></p>
<br>
<h1>Our mission</h1>
<p>
We are a company in which we are committed to our staff and our clients to permanently exceed the level of service and attention in all areas</p>
<br>
<h1>Our vision</h1>
<p>
To be the company that offers the best option in international tax and financial services in the world, with a superior level of quality and opportunity, both for our clients and for the possible users of our service.</p>
<h1>Professional quality</h1><p>Our main occupation is to provide professional and quality services. For this, our company has an extensive training program, in which all our staff, by institutional policy, actively and permanently participate.</p>

       </div>
            
        </section>
<br>

        <!-- About Law End-->

        

        <div class="testimonial-area testimonial-padding" data-background="assets/img/testmonial/testi_bg.png">
            <div class="container">
                <!--Section Tittle  -->
                <div class="row ">
                    <div class="col-xl-12">
                        <div class="section-tittle section-tittle-testi text-center mb-45">
                            <h2>Words From Clients</h2>
                        </div>
                    </div>
                </div>
                <!-- Testimonial contents -->
               <div class="row d-flex justify-content-center">
                    <div class="col-xl-8 col-lg-8 col-md-10">
                        <div class="h1-testimonial-active dot-style">
                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                 <!-- Testimonial Content -->
                                <div class="testimonial-caption ">
                                    <div class="testimonial-top-cap">
                                        <p>As a broker we have utilized multiple forms of seller financing in the past, including Bond for Deed contracts. We have found not only very knowledgeable when it comes to preparing Bond for Deed contracts, but very beneficial in helping buyers and sellers </p>
                                    </div>
                                    <!-- founder -->
                                    <div class="testimonial-founder  ">
                                        <div class="founder-img">
                                           <img src="desc/h10.jpg" alt="">
                                           <span>Devid jonathan</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                 <!-- Testimonial Content -->
                                <div class="testimonial-caption ">
                                    <div class="testimonial-top-cap">
                                        <p>I have been working with Consulter for the past few months and they have accomplished an extraordinary effort in executing our Campaign and have delivered their skills and abilities to meet our promised targets. They have shown their excellence in providing top-notch support.</p>
                                    </div>
                                    <!-- founder -->
                                    <div class="testimonial-founder  ">
                                        <div class="founder-img">
                                           <img src="desc/m25.jpg" alt="">
                                           <span>Jennifer Hoobs</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Testimonial -->
                            <div class="single-testimonial text-center">
                                 <!-- Testimonial Content -->
                                <div class="testimonial-caption ">
                                    <div class="testimonial-top-cap">
                                        <p>     I just wanted to thank your customer service representative who contacted me twice regarding my order status. He left me a second message today with detailed information. I never had this superb customer service from any other companies before.
</p>
                                    </div>
                                    <!-- founder -->
                                    <div class="testimonial-founder  ">
                                        <div class="founder-img">
                                           <img src="desc/h15.jpg" alt="">
                                           <span>Shean Lowis</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
        <div class="teams-area section-padding30">
            <div class="container">
                <div class="row ">
                    <div class="col-xl-12">
                        <div class="section-tittle section-tittle-f text-center mb-70">
                            <h2>Team Mates</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4">
                        <div class="single-teams text-center">
                            <div class="team-img">
                                <img src="assets/img/team/team_1.jpg" alt="">
                            </div>
                            <div class="team-caption">
                                <h4><a href="#">Jhon Smith</a></h4>
                                <span>Senior Lawyer</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4">
                        <div class="single-teams text-center">
                            <div class="team-img">
                                <img src="assets/img/team/team_2.jpg" alt="">
                            </div>
                            <div class="team-caption">
                                <h4><a href="#">Emma Bunton</a></h4>
                                <span>Professional Lawyer</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4">
                        <div class="single-teams text-center">
                            <div class="team-img">
                                <img src="assets/img/team/team_3.jpg" alt="">
                            </div>
                            <div class="team-caption">
                                <h4><a href="#">Bunton Jonathon</a></h4>
                                <span>Top Rated Lawyer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Team Mates End-->

        

    </main>
<?php include'footer.php';?>