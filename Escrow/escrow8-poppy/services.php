<?php include'header.php';?>
    <main>
        <!--? Hero Start -->
        <div class="slider-area2">
            <div class="slider-height2 hero-overly2 d-flex align-items-center"><center>
                <div class="container">

                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap hero-cap2 text-center">
                                <h2>Our Services</h2>
                            </div>
                        </div>
                    </div>
                </div></center>
            </div>
        </div><br>
        <!-- Hero End -->
       <div class="support-company-area pt-100 pb-100 section-bg fix" data-background="assets/img/gallery/section_bg02.jpg">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-6">
                    <div class="support-location-img">
                        <img src="assets/img/gallery/dddd.webp" alt="">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="right-caption">
                        <!-- Section Tittle -->
                        <div class="section-tittle section-tittle2 mb-50">
                            <h2>buy</h2>
                        </div>
                        <div class="support-caption" style="width: 80%" >
                            <p class="pera-top">We insure your property during the sale process by offering a quality service and with the best agents who will support this process.</p>
                            <p class="mb-65">we have specialized and qualified advisers in Conditioned Deposits.</p>
                            <a href="contact.php" class="btn post-btn">contact</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
<br>
    <div class="support-company-area pt-100 pb-100 section-bg fix" data-background="assets/img/gallery/ct.jpg">
        <div class="container">
            <div class="row align-items-center">
                
                <div class="col-xl-6 col-lg-6">
                    <div class="right-caption">
                        <!-- Section Tittle -->
                        <div class="section-tittle section-tittle2 mb-50">
                         
                            <h2>sell</h2>
                        </div>
                        <div class="support-caption" style="width: 80%;">
                            <p class="pera-top">We insure your property during the sale process by offering a quality service and with the best agents who will support this process.</p>
                            <a href="contact.php" class="btn post-btn">contact</a>
                        </div>
                    </div>
                </div><div class="col-xl-6 col-lg-6">
                    <div class="support-location-img">
                        <img src="assets/img/gallery/vt.jpg" alt="">
                    </div>
                </div>

            </div>
        </div>
         </div><br>
        <br>
          <div class="support-company-area pt-100 pb-100 section-bg fix" data-background="assets/img/gallery/ct2.jpg">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-6">
                    <div class="support-location-img">
                        <img src="assets/img/gallery/about.png" alt="">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="right-caption">
                        <!-- Section Tittle -->
                        <div class="section-tittle section-tittle2 mb-50">
                            
                            <h2>consultancies</h2>
                        </div>
                        <div class="support-caption">
                            <p class="pera-top">We make Trusts that guarantee obligations derived from various contracts such as:</p>
                            <p class="mb-65">Credit contracts</p><p class="mb-65">Real Estate Developments</p>
                            <p class="mb-65">Stock Control</p><p class="mb-65">Family protocols</p>
                            <p class="mb-65">Administration for Educational Purposes</p>
<p class="mb-65">Conditioned Deposits (Escrows)</p>
                            <a href="contact.php" class="btn post-btn">contact</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div><br>
   
    </main>
    <?php include'footer.php';?>