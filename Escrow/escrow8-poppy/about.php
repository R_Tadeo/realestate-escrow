<?php include'header.php';?>
    <main>
        <!--? Hero Start -->
        <div class="slider-area2">
            <div class="slider-height2 hero-overly2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap hero-cap2 text-center">
                                <h2>About Us</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <div class="about-details section-padding30">
            <div class="container">
                <div class="row">
                    <div class="offset-xl-1 col-lg-8">
                        <div class="about-details-cap mb-50">
                            <center><h1>know us</h1></center>
                            <p>We are a company founded in the 90's, since then our main objective is to help the client by offering them a quality service.
                            </p>
                            <p>we have highly qualified and certified personnel
We understand your need in detail and we give you a solution. leaders in trust services, always ensuring the client as both buyer and seller.</p>
<p>In our Corporate Trusts area we offer comprehensive administrative experience, trust services and solutions tailored to the needs of municipal and corporate clients who have access to the public and private debt markets to finance their projects.

</p>
                        </div>
                        <div><center><h2>how does it work</h2><img src="assets/img/gallery/esc.png" alt=""></center></div><br>
                        <div class="about-details-cap mb-50">
                            <h4>Our Mission</h4>
                            <center>
                            <h5>To be the leading company in trust services as well as to make business happen by guaranteeing the assets of the people.
                             </h5>
                             </center>
                        </div>

                        <div class="about-details-cap mb-50">
                            <h4>Our Vision</h4>
                            <h5>Be a safe and trustworthy business facilitator..
                             </h5>
                             <h5>have honesty, confidentiality, dedication and commitment to customer service and offer quality service</h5>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
        <!-- About Area End-->
        <!-- Team Start -->
        <div class="team-area section-padding30">
            <div class="container">
                <div class="row">
                    <div class="cl-xl-7 col-lg-8 col-md-10">
                        <!-- Section Tittle -->
                        <div class="section-tittle mb-70">
                            <span>Our Professional members </span>
                            <h2>Our Team Mambers</h2>
                        </div> 
                    </div>
                </div>
                <div class="row">
                    <!-- single Tem -->
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="assets/img/gallery/h1.png" alt="">
                            </div>
                            <div class="team-caption">
                                <h3><a href="#">Ethan Welch</a></h3>
                                <span>director</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="assets/img/gallery/team3.png" alt="">
                            </div>
                            <div class="team-caption">
                                <h3><a href="#">J.k. Jonnson</a></h3>
                                <span>Adviser</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-">
                        <div class="single-team mb-30">
                            <div class="team-img">
                                <img src="assets/img/gallery/h2.png" alt="">
                            </div>
                            <div class="team-caption">
                                <h3><a href="#">H.Y. Heands</a></h3>
                                <span>adviser</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Team End -->
        <!--? Testimonial Start -->
        
        <!-- Brand Area End -->
    </main>
    <?php include'footer.php';?>