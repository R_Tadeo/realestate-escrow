<?php include 'header.php' ?>

<section class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_1.jpg');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">Services</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Services <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-no-pb">
			<div class="container">
				<div class="row d-flex justify-content-center">
				<div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-4">Our Best Services</h2>
            <p>Our services can save you time, money, and the aggravation of managing installment payments on your real estate investment.</p>
          </div>
        </div>
				<div class="row no-gutters">
					<div class="col-lg-4 d-flex">
						<div class="services-2 noborder-left text-center ftco-animate">
							<div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-handshake"></span></div>
							<div class="text media-body">
								<h3>Rentals</h3>
								<p>Our management service solves all tenant concerns and channels them effectively. Month by month, balances, receipts and account statements are generated that inform both Owners and Tenants of the situation of their rent.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 d-flex">
						<div class="services-2 text-center ftco-animate">
							<div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-insurance"></span></div>
							<div class="text media-body">
								<h3>Sales</h3>
								<p>We make sure that the buyer of your property is determined to buy and has the resources to carry out the sale. The delivery of the sale opinion is one of the most important points of our service because at that moment is when we advise you in detail after having determined everything in relation to your sale (valuation, sale process, tax opinion).</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 d-flex">
						<div class="services-2 text-center ftco-animate">
							<div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-business"></span></div>
							<div class="text media-body">
								<h3>Advisory</h3>
								<p>With the best data analysis technology and a team of experts, Escrow conducts candidate research so that you have the peace of mind of having the best client.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

    <section class="ftco-section">
			<div class="container">
				<div class="row justify-content-center mb-5 pb-2">
				<div class="col-md-5 order-md-last wrap-about align-items-stretch">
						<div class="wrap-about-border ftco-animate">
							<div class="img" style="background-image: url(images/image_2.jpg); border"></div>
							<div class="text">
								<h3>Read Our Success Story for Inspiration</h3>
								<p>The Escrow Team was founded in 1985, by Matthew Robbins, to create a different experience for clients in the Real Estate industry. We aren't here to talk you into anything, but we are here to talk you through the process, so you can make the best and most informed decisions concerning your Real Estate Goals.</p>
								<p><a href="contact.php" class="btn btn-primary py-3 px-4">Contact us</a></p>
							</div>
						</div>
					</div>
					<div class="col-md-7 wrap-about pr-md-4 ftco-animate">
          	<h2 class="mb-4">Our Main Features</h2>
						<p>We are based on a business system, in which customer satisfaction is our top priority.</p>
						<p>Our team of professional analysts is constantly roaming the web in search of promising techniques, which will make your business better.</p>
						<div class="row mt-5">
							<div class="col-lg-6">
								<div class="services text-center">
									<div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-collaboration"></span></div>
									<div class="text media-body">
										<h3>Organization</h3>
										<p>We carry out good management of our projects.</p>
									</div>
								</div>
								<div class="services text-center">
									<div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-analysis"></span></div>
									<div class="text media-body">
										<h3>Risk Analysis</h3>
										<p>We know when it is a good deal or not.</p>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="services text-center">
									<div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-search-engine"></span></div>
									<div class="text media-body">
										<h3>Marketing Strategy</h3>
										<p>We know the sales market very well.</p>
									</div>
								</div>
								<div class="services text-center">
									<div class="icon mt-2 d-flex justify-content-center align-items-center"><span class="flaticon-money"></span></div>
									<div class="text media-body">
										<h3>Capital Market</h3>
										<p>We are associated with many recognized companies</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

<?php include 'footer.php' ?>