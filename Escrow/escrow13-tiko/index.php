<?php include 'header.php' ?>

<main>

<!-- slider Area Start-->
<div class="slider-area ">
    <div class="slider-active">
        <!-- Single Slider -->
        <div class="single-slider slider-height d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-8">
                        <div class="hero__caption">
                            <span data-animation="fadeInUp" data-delay=".4s">Get Every Single Solutions.</span>
                            <h1 data-animation="fadeInUp" data-delay=".6s">Professional Accounting & Tax Services</h1>
                            <P data-animation="fadeInUp" data-delay=".8s" >Providing every client with the attention they deserve.</P>
                            <!-- Hero-btn -->
                            <div class="hero__btn">
                                <a href="services.php" class="btn hero-btn"  data-animation="fadeInLeft" data-delay=".8s">Our Services</a>
                                <a href="contact.php" class="btn border-btn ml-15" data-animation="fadeInRight" data-delay="1.0s">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider Area End-->
<section class="about-area section-paddingt30">
    <div class="container">
        <div class="row ">
            <div class="col-lg-5">
                <div class="about-caption mb-50">
                    <h3>Portafolio Is</h3>
                    <p class="pera1">CoEscrow office dedicated to the commercialization of Real Estate. Experts in sale, purchase and rental of properties. Comprehensive real estate that provides a wide range of services such as: mortgage loans, investment plans, legal and financial advice.</p>
                </div>
            </div>
            <div class="col-lg-5 offset-lg-1">
                <div class="about-caption2">
                    <h3>Any Type Of Query<br> & Discussion.</h3>
                    <p>Contact us</p>
                   <div class="send-cv">
                        <a href="contact.php">contact@portafolio.com</a>
                        <i class="ti-arrow-right"></i>
                   </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Categories Area Start -->
<section class="categories-area section-padding3">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <!-- Section Tittle -->
                <div class="section-tittle mb-70">
                    <h2>What Services you will Get from Us!</h2>
                </div>
            </div>
        </div>
        <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6" >
                <div class="single-cat text-center mb-50" style="height: 560px;">
                    <div class="cat-icon">
                    <span class="box-default-icon novi-icon icon-lg mercury-icon-house"></span>
                    </div>
                    <div class="cat-cap">
                        <h5><a href="services.html">Rentals</a></h5>
                        <p>Our management service solves all tenant concerns and channels them effectively. Month by month, balances, receipts and account statements are generated that inform both Owners and Tenants of the situation of their rent.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6" >
                <div class="single-cat text-center mb-50" style="height: 560px;">
                    <div class="cat-icon">
                    <span class="box-default-icon novi-icon icon-lg mercury-icon-partners"></span>
                    </div>
                    <div class="cat-cap">
                        <h5><a href="services.html">Sales</a></h5>
                        <p>We make sure that the buyer of your property is determined to buy and has the resources to carry out the sale. The delivery of the sale opinion is one of the most important points of our service because at that moment is when we advise you in detail after having determined everything in relation to your sale.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6" >
                <div class="single-cat text-center mb-50" style="height: 560px;">
                    <div class="cat-icon">
                    <span class="box-default-icon novi-icon icon-lg mercury-icon-group"></span>
                    </div>
                    <div class="cat-cap">
                        <h5><a href="services.html">Advisory</a></h5>
                        <p>With the best data analysis technology and a team of experts, Escrow conducts candidate research so that you have the peace of mind of having the best client.</p>
                    </div>
                </div>
            </div>
         </div>
    </div>
</section>
<!-- Categories Area End -->
<!-- Want To Work Start -->
<section class="wantToWork-area w-padding2">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-xl-6 col-lg-8 col-md-8">
                <div class="wantToWork-caption wantToWork-caption2">
                    <h2>Dont worry for contact we are available</h2>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3">
                <a href="contact.php" class="btn btn-black f-right">Contact Me Now</a>
            </div>
        </div>
    </div>
</section>
<!-- Want To Work End -->
<!-- client-comments -->
<div class="client-comments section-paddingt30">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-8 col-md-10">
                <!-- Section Tittle -->
                <div class="section-tittle mb-70">
                    <h2>Happy customers</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- latest-blog-area start -->
    <div class="latest-blog-area">
        <div class="container">
            <div class="custom-row">
                <div class="blog-active">
                    <!-- single-items -->
                    <div class="col-xl-4">
                        <div class="blog-wrapper">
                            <div class="blog-inner">
                            <div class="blog-top">
                                <div class="person-img">
                                    <img src="assets/img/gallery/blog1.png" alt="">
                                </div>
                                <div class="comment-person">
                                    <h2>Bradley Erickson</h2>   
                                    <span>Client</span>
                                </div>
                            </div>
                                <p>I have used Escrow Services in the past on numerous occasions for Bond for Deeds. Bond for Deeds provide a very viable avenue to sell homes in a tough market. I find that buyers are motivated to do what they need to do to refinance in whatever time frame is agreed upon.</p>
                            </div>
                        </div>
                    </div>
                    <!-- single-items -->
                    <div class="col-xl-4">
                        <div class="blog-wrapper">
                            <div class="blog-inner">
                            <div class="blog-top">
                                    <div class="person-img">
                                        <img src="assets/img/gallery/blog2.png" alt="">
                                    </div>
                                    <div class="comment-person">
                                        <h2>Bradley Erickson</h2>   
                                        <span>Client</span>
                                    </div>
                            </div>
                                <p>They personal come to our meetings and speak to our agents about the advantages of Bond for Deed transactions in this difficult market. My experience is that they have explained and educated the Bond for Deed process to the agents in the same fashion as they do with their company's clients.</p>
                            </div>
                        </div>
                    </div>
                    <!-- single-items -->
                    <div class="col-xl-4">
                        <div class="blog-wrapper">
                            <div class="blog-inner">
                            <div class="blog-top">
                                    <div class="person-img">
                                        <img src="assets/img/gallery/blog3.png" alt="">
                                    </div>
                                    <div class="comment-person">
                                        <h2>Bradley Erickson</h2>   
                                        <span>Client</span>
                                    </div>
                            </div>
                                <p>They take the time to answer questions along with assuaging any fears buyers or sellers might have in a pragmatic, real life approach. Escrow Services, Inc., has been an integral part in offering options available to our clients in the buying/selling process"</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>           
    <!-- End latest-blog-area -->
</div>
<!-- Brand Area Start -->
<div class="brand-area pb-bottom">
    <div class="container">
        <div class="brand-active brand-border pt-50 pb-40">
            <div class="single-brand">
                <img src="assets/img/gallery/brand1.png" alt="">
            </div>
            <div class="single-brand">
                <img src="assets/img/gallery/brand2.png" alt="">
            </div>
            <div class="single-brand">
                <img src="assets/img/gallery/brand3.png" alt="">
            </div>
            <div class="single-brand">
                <img src="assets/img/gallery/brand4.png" alt="">
            </div>
            <div class="single-brand">
                <img src="assets/img/gallery/brand2.png" alt="">
            </div>
        </div>
    </div>
</div>
<!-- Brand Area End -->

</main>

<?php include 'footer.php' ?>