<?php include 'header.php' ?>

<main>

<!-- Hero Start -->
<div class="slider-area ">
    <div class="slider-height2 d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="hero-cap">
                        <h2>About us</h2>
                        <nav aria-label="breadcrumb ">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item"><a>About us</a></li> 
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Hero End -->
<!-- About Me Start -->
<div class="about-me pb-top">
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-lg-6 col-md-6">
                <div class="about-me-img mb-30">
                    <img src="assets/img/gallery/aboutme.png" alt="">
                </div>
            </div>
            <div class="col-lg-5 col-md-6">
                <div class="about-me-caption">
                    <h2>A Few Words About Us</h2> 
                    <p class="pb-30">To be the world leaders in real estate, reaching our goals by helping others achieve theirs. Everyone wins.</p>
                    <h5>Our mission is to provide accounting and tax services of a superior quality. The responsive, personal attention we provide for each client, regardless how large or small, is the key to our approach</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Me End -->
<!-- About Area start -->
<section class="about-area section-paddingt30">
    <div class="container">
        <div class="row ">
            <div class="col-lg-5">
                <div class="about-caption mb-50">
                    <h3>Portafolio Is</h3>
                    <p class="pera1">CoEscrow office dedicated to the commercialization of Real Estate. Experts in sale, purchase and rental of properties. Comprehensive real estate that provides a wide range of services such as: mortgage loans, investment plans, legal and financial advice.</p>
                </div>
            </div>
            <div class="col-lg-5 offset-lg-1">
                <div class="about-caption2">
                    <h3>Any Type Of Query<br> & Discussion.</h3>
                    <p>Contact us</p>
                   <div class="send-cv">
                        <a href="contact.php">contact@portafolio.com</a>
                        <i class="ti-arrow-right"></i>
                   </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Area End -->
<!-- Want To Work Start -->
<section class="wantToWork-area w-padding2">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-xl-6 col-lg-8 col-md-8">
                <div class="wantToWork-caption wantToWork-caption2">
                    <h2>Dont worry for contact we are available</h2>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3">
                <a href="contact.php" class="btn btn-black f-right">Contact Me Now</a>
            </div>
        </div>
    </div>
</section>
<!-- Want To Work End -->
<!-- client-comments -->
<div class="client-comments section-paddingt30">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-8 col-md-10">
                <!-- Section Tittle -->
                <div class="section-tittle mb-70">
                    <h2>Happy customers</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- latest-blog-area start -->
    <div class="latest-blog-area">
        <div class="container">
            <div class="custom-row">
                <div class="blog-active">
                    <!-- single-items -->
                    <div class="col-xl-4">
                        <div class="blog-wrapper">
                            <div class="blog-inner">
                            <div class="blog-top">
                                <div class="person-img">
                                    <img src="assets/img/gallery/blog1.png" alt="">
                                </div>
                                <div class="comment-person">
                                    <h2>Bradley Erickson</h2>   
                                    <span>Client</span>
                                </div>
                            </div>
                                <p>I have used Escrow Services in the past on numerous occasions for Bond for Deeds. Bond for Deeds provide a very viable avenue to sell homes in a tough market. I find that buyers are motivated to do what they need to do to refinance in whatever time frame is agreed upon.</p>
                            </div>
                        </div>
                    </div>
                    <!-- single-items -->
                    <div class="col-xl-4">
                        <div class="blog-wrapper">
                            <div class="blog-inner">
                            <div class="blog-top">
                                    <div class="person-img">
                                        <img src="assets/img/gallery/blog2.png" alt="">
                                    </div>
                                    <div class="comment-person">
                                        <h2>Bradley Erickson</h2>   
                                        <span>Client</span>
                                    </div>
                            </div>
                                <p>They personal come to our meetings and speak to our agents about the advantages of Bond for Deed transactions in this difficult market. My experience is that they have explained and educated the Bond for Deed process to the agents in the same fashion as they do with their company's clients.</p>
                            </div>
                        </div>
                    </div>
                    <!-- single-items -->
                    <div class="col-xl-4">
                        <div class="blog-wrapper">
                            <div class="blog-inner">
                            <div class="blog-top">
                                    <div class="person-img">
                                        <img src="assets/img/gallery/blog3.png" alt="">
                                    </div>
                                    <div class="comment-person">
                                        <h2>Bradley Erickson</h2>   
                                        <span>Client</span>
                                    </div>
                            </div>
                                <p>They take the time to answer questions along with assuaging any fears buyers or sellers might have in a pragmatic, real life approach. Escrow Services, Inc., has been an integral part in offering options available to our clients in the buying/selling process"</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>           
    <!-- End latest-blog-area -->
</div>
<!-- Brand Area Start -->
<div class="brand-area pb-bottom">
    <div class="container">
        <div class="brand-active brand-border pt-50 pb-40">
            <div class="single-brand">
                <img src="assets/img/gallery/brand1.png" alt="">
            </div>
            <div class="single-brand">
                <img src="assets/img/gallery/brand2.png" alt="">
            </div>
            <div class="single-brand">
                <img src="assets/img/gallery/brand3.png" alt="">
            </div>
            <div class="single-brand">
                <img src="assets/img/gallery/brand4.png" alt="">
            </div>
            <div class="single-brand">
                <img src="assets/img/gallery/brand2.png" alt="">
            </div>
        </div>
    </div>
</div>
<!-- Brand Area End -->

</main>

<?php include 'footer.php' ?>