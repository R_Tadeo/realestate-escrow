<?php include 'header.php' ?>

<main>

<!-- Header Start -->
<div class="slider-area ">
    <div class="slider-height2 d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="hero-cap">
                        <h2>Our Services</h2>
                        <nav aria-label="breadcrumb ">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item"><a>Our Services</a></li> 
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Header End -->
<!-- Categories Area Start -->
<section class="categories-area categories-area2  section-padding30">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6" >
                <div class="single-cat text-center mb-50" style="height: 560px;">
                    <div class="cat-icon">
                    <span class="box-default-icon novi-icon icon-lg mercury-icon-house"></span>
                    </div>
                    <div class="cat-cap">
                        <h5><a href="services.html">Rentals</a></h5>
                        <p>Our management service solves all tenant concerns and channels them effectively. Month by month, balances, receipts and account statements are generated that inform both Owners and Tenants of the situation of their rent.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6" >
                <div class="single-cat text-center mb-50" style="height: 560px;">
                    <div class="cat-icon">
                    <span class="box-default-icon novi-icon icon-lg mercury-icon-partners"></span>
                    </div>
                    <div class="cat-cap">
                        <h5><a href="services.html">Sales</a></h5>
                        <p>We make sure that the buyer of your property is determined to buy and has the resources to carry out the sale. The delivery of the sale opinion is one of the most important points of our service because at that moment is when we advise you in detail after having determined everything in relation to your sale.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6" >
                <div class="single-cat text-center mb-50" style="height: 560px;">
                    <div class="cat-icon">
                    <span class="box-default-icon novi-icon icon-lg mercury-icon-group"></span>
                    </div>
                    <div class="cat-cap">
                        <h5><a href="services.html">Advisory</a></h5>
                        <p>With the best data analysis technology and a team of experts, Escrow conducts candidate research so that you have the peace of mind of having the best client.</p>
                    </div>
                </div>
            </div>
         </div>
    </div>
</section>
<!-- Categories Area End -->
<!-- Want To Work Start -->
<section class="wantToWork-area w-padding2">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-xl-6 col-lg-8 col-md-8">
                <div class="wantToWork-caption wantToWork-caption2">
                    <h2>Dont worry for contact we are available</h2>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-3">
                <a href="contact.php" class="btn btn-black f-right">Contact Us Now</a>
            </div>
        </div>
    </div>
</section>
<!-- Want To Work End -->
<!-- Brand Area Start -->
<div class="brand-area section-padding30">
    <div class="container">
        <div class="brand-active brand-border pt-50 pb-40">
            <div class="single-brand">
                <img src="assets/img/gallery/brand1.png" alt="">
            </div>
            <div class="single-brand">
                <img src="assets/img/gallery/brand2.png" alt="">
            </div>
            <div class="single-brand">
                <img src="assets/img/gallery/brand3.png" alt="">
            </div>
            <div class="single-brand">
                <img src="assets/img/gallery/brand4.png" alt="">
            </div>
            <div class="single-brand">
                <img src="assets/img/gallery/brand2.png" alt="">
            </div>
        </div>
    </div>
</div>
<!-- Brand Area End -->

</main>

<?php include 'footer.php' ?>